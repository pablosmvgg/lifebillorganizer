package com.psm.lifebillorganizer.dao

import android.content.Context
import com.psm.lifebillorganizer.Database
import com.psm.lifebillorganizer.daos.*
import com.squareup.sqldelight.android.AndroidSqliteDriver
import com.squareup.sqldelight.db.SqlDriver
import javax.inject.Inject

class DatabaseDao @Inject constructor() {

    private lateinit var periodQueries: PeriodQueries
    private lateinit var paymentQueries: PaymentQueries
    private lateinit var payerQueries: PayerQueries
    private lateinit var paymentGroupQueries: PaymentGroupQueries
    private lateinit var firstTimeExecQueries: FirstTimeExecQueries

    private var database: Database? = null

    fun initDatabase(context: Context) {
        if (database == null) {
            val driver: SqlDriver = AndroidSqliteDriver(Database.Schema, context, "lifeOrganizer.db")
            database = Database(driver)
            this.periodQueries = database!!.periodQueries
            this.paymentQueries = database!!.paymentQueries
            this.payerQueries = database!!.payerQueries
            this.paymentGroupQueries = database!!.paymentGroupQueries
            this.firstTimeExecQueries = database!!.firstTimeExecQueries
        }

    }

    fun getPeriodQueries(): PeriodQueries {
        return this.periodQueries
    }

    fun getPaymentQueries(): PaymentQueries {
        return this.paymentQueries
    }

    fun getPayerQueries(): PayerQueries {
        return this.payerQueries
    }

    fun getPaymentGroupQueries(): PaymentGroupQueries {
        return this.paymentGroupQueries
    }

    fun getFirstTimeExecQueries(): FirstTimeExecQueries {
        return this.firstTimeExecQueries
    }

}