package com.psm.lifebillorganizer.rests

import com.psm.lifebillorganizer.models.rests.MongoPayerResponse
import com.psm.lifebillorganizer.models.rests.PayerRemoteUpdatedBody
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST

interface PayerRest {
    @POST("aggregate")
    suspend fun getOtherPayersFromGroup(@Body body: PayerRemoteUpdatedBody):
            Response<MongoPayerResponse>
}