package com.psm.lifebillorganizer.rests

import com.psm.lifebillorganizer.models.rests.*
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST

interface GroupRest {

    @POST("findOne")
    suspend fun getPaymentGroupById(@Body body: PaymentGroupBody): Response<MongoPaymentGroup?>

    @POST("insertOne")
    suspend fun insertPaymentGroup(@Body body: InsertPaymentGroupBody): Response<MongoPaymentGroup?>

    @POST("updateOne")
    suspend fun updatePaymentGroup(@Body body: UpdatePaymentGroupBody): Response<MongoPaymentGroup?>

    @POST("deleteOne")
    suspend fun deletePaymentGroup(@Body body: DeletePaymentGroupBody): Response<String>

}