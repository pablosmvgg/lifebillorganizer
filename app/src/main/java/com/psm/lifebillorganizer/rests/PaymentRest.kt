package com.psm.lifebillorganizer.rests

import com.psm.lifebillorganizer.models.rests.*
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST

interface PaymentRest {

    @POST("aggregate")
    suspend fun getPaymentByNotifiedPayers(@Body body: PaymentRemoteUpdatedBody):
            Response<MongoPaymentResponse>

    @POST("aggregate")
    suspend fun getPaymentByExternalId(@Body body: PaymentRemoteUpdatedBody):
            Response<MongoPaymentResponse>

    @POST("updateOne")
    suspend fun insertPaymentInPaymentGroup(@Body body: InsertPaymentInPaymentGroupBody):
            Response<MongoPaymentGroup?>

    @POST("updateOne")
    suspend fun updatePaymentInPaymentGroup(@Body body: UpdatePaymentInPaymentGroupBody):
            Response<MongoPaymentGroup?>

    /*
    Online tests: https://mongoplayground.net/

    Mongo queries: find payments with filter and returns only payments:
    https://www.mongodb.com/community/forums/t/find-inside-an-array-in-a-collection/9956/3
    This returns 1:
    LifeBillOrganizer.GroupPayments.find( {"externalId": "22fVe8B", "payments.notifiedPayers": 1}, { "payments.$": 1, _id: 0 } )
    To get more than 1:
    db.PaymentGroups.aggregate([
      {
        "$unwind": {
          "path": "$payments"
        }
      },
      {
        "$match": {
          "externalId": "22fVe8B",
          "payments.notifiedPayers": {
            $lt: 2
          }
        }
      },
      {
        "$project": {
          _id: 0,
          "name": "$payments.name",
          "description": "$payments.description",
          "value": "$payments.value",
          "payerExternalId": "$payments.payerExternalId",
          "createdDate": "$payments.createdDate",
          "paymentDate": "$payments.paymentDate",
          "paymentType": "$payments.paymentType",
          "paymentRecurrent": "$payments.paymentRecurrent",
          "paymentRecurrentCount": "$payments.paymentRecurrentCount",
          "externalId": "$payments.externalId",
          "notifiedPayers": "$payments.notifiedPayers",
          "deleted": "$payments.deleted"
        }
      }
    ])


    curl aggregate:
    curl --request POST \
  'https://data.mongodb-api.com/app/data-rkubr/endpoint/data/beta/action/aggregate' \
  --header 'Content-Type: application/json' \
  --header 'api-key: UwypHYi5IqdP1rSE5IJyw9W9amJvBJIghNTzysVYtN7jbs2FNC7Oy4XXHrxqcxLv' \
  --data-raw '{
  "collection": "GroupPayments",
  "dataSource": "LifeBillOrganizerCluster",
  "database": "LifeBillOrganizer",
  "pipeline": [
    {
      "$unwind": {
        "path": "$payments"
      }
    },
    {
      "$match": {
        "externalId": "2veZhhT",
        "payments.notifiedPayers": {
          "$lt": "2"
        },
        "payments.payerExternalIdModified": {
          "$ne": "tc9x7anczJ"
        }
      }
    },
    {
      "$project": {
        "createdDate": "$payments.createdDate",
        "deleted": "$payments.deleted",
        "description": "$payments.description",
        "externalId": "$payments.externalId",
        "_id": "0",
        "name": "$payments.name",
        "notifiedPayers": "$payments.notifiedPayers",
        "payerExternalId": "$payments.payerExternalId",
        "paymentDate": "$payments.paymentDate",
        "paymentRecurrent": "$payments.paymentRecurrent",
        "paymentRecurrentCount": "$payments.paymentRecurrentCount",
        "paymentType": "$payments.paymentType",
        "value": "$payments.value"
      }
    }
  ]
}'


     */

}