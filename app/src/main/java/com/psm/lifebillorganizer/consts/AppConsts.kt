package com.psm.lifebillorganizer.consts

object AppConsts {
    //bundle names
    const val BUNDLE_FROM_OPTIONS = "fromOptions"
    const val BUNDLE_ORIGINAL_PAGE = "originalPage"
    const val BUNDLE_PAYMENTS = "payments"
    const val BUNDLE_PAYMENT = "payment"
    const val BUNDLE_PAYMENT_GROUP = "paymentGroup"
    const val BUNDLE_PAYER = "payer"
    const val BUNDLE_PAYMENT_TYPE = "paymentType"
    const val BUNDLE_PAYMENT_GROUP_ID = "paymentGroupId"

    //max values
    const val MAX_PAYERS: Int = 20
    const val MAX_GROUPS: Int = 5
    const val MAX_GROUP_PAYERS: Int = 5

    const val PREFERENCE_THEME = "preferenceTheme"
    const val PREFERENCE_THEME_LIGHT = "preferenceThemeLight"
    const val PREFERENCE_THEME_DARK = "preferenceThemeDark"
    const val PREFERENCE_THEME_SYSTEM = "preferenceThemeSystem"

    const val MONGODB_PAYMENT_GROUP_COLLECTION = "GroupPayments"
    const val MONGODB_DATABASE = "LifeBillOrganizer"
    const val MONGODB_DATASOURCE = "LifeBillOrganizerCluster"

}