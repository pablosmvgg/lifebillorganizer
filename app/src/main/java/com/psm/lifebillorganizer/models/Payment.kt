package com.psm.lifebillorganizer.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable
import java.time.LocalDateTime

data class Payment(
    var id: Long?,
    @Expose var name: String,
    @Expose var description: String?,
    @Expose var value: Double,
    @Expose var payerId: Long,
    @Expose var payerExternalId: String?,
    var paymentGroupId: Long,
    @Expose var createdDate: LocalDateTime,
    @Expose var paymentDate: LocalDateTime,
    @Expose var paymentType: Long,
    @Expose var paymentRecurrent: Boolean,
    @Expose var paymentRecurrentCount: Long,
    @Expose var externalId: String?,
    @Expose var notifiedPayers: Long?,
    @Expose var deleted: Boolean,
    @Expose var payerExternalIdModified: String?,
    var isSubmitted: Boolean
): Serializable {

    lateinit var  payer: Payer

    override fun equals(other: Any?): Boolean {
        if (other !is Payment) {
            return false
        }
        if (other.externalId != null && other.externalId != externalId) {
            return false
        }
        if (other.id != null && other.id != id) {
            return false
        }
        if (other.name != name) {
            return false
        }
        return true
    }

    override fun hashCode(): Int {
        return name.length
    }

    override fun toString(): String {
        val stringReturn = StringBuilder()
        stringReturn.append("DATA: ")
        stringReturn.append(" Id: ").append(id)
        stringReturn.append(" Name: ").append(name)
        stringReturn.append(" Value: ").append(value)
        stringReturn.append(" PayerId: ").append(payerId)
        stringReturn.append(" PayerExternalId: ").append(payerExternalId)
        stringReturn.append(" ExternalId: ").append(externalId)
        stringReturn.append(" NotifiedPayers: ").append(notifiedPayers)
        stringReturn.append(" Deleted: ").append(deleted)
        stringReturn.append(" PayerExternalIdModified: ").append(payerExternalIdModified)
        stringReturn.append(" || ")

        return stringReturn.toString()
    }

}