package com.psm.lifebillorganizer.models.rests.pipelines

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class MatchPipelineParent(@Expose @SerializedName("\$match") var matchPipeline: MatchPipeline)
    : AggregationPipeline()

class MatchPipeline(@Expose @SerializedName("\$and") var andValue: Array<MatchElementPipeline>)  {
}