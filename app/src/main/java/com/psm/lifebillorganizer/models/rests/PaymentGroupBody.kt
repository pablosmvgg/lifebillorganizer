package com.psm.lifebillorganizer.models.rests

import com.google.gson.annotations.Expose

class PaymentGroupBody(@Expose var collection: String,
                       @Expose var database: String,
                       @Expose var dataSource: String,
                       @Expose var filter: GetByExternalIdFilter) {

}