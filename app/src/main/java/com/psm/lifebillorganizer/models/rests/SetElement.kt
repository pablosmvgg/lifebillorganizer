package com.psm.lifebillorganizer.models.rests

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class SetElement(@Expose @SerializedName("\$set") var setElement: SetPayment) {
}