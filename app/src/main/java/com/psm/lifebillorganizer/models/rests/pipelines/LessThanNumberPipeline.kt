package com.psm.lifebillorganizer.models.rests.pipelines

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class LessThanNumberPipeline(@Expose @SerializedName("\$lt") var lessThanValue: Number)
    : MatchElementPipeline() {
}