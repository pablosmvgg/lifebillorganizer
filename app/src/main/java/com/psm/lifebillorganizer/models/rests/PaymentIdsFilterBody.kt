package com.psm.lifebillorganizer.models.rests

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class PaymentIdsFilterBody(@Expose  @SerializedName("externalId") val externalId: String,
                           @Expose  @SerializedName("payments.externalId") val paymentExternalId: String) {

}