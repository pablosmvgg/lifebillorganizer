package com.psm.lifebillorganizer.models.rests.pipelines.specific

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.psm.lifebillorganizer.models.rests.pipelines.LessThanNumberPipeline
import com.psm.lifebillorganizer.models.rests.pipelines.LessThanPipeline
import com.psm.lifebillorganizer.models.rests.pipelines.MatchElementPipeline

class PaymentNotifiedPayerPipeline(
    @Expose @SerializedName("payments.notifiedPayers") var lessThanNumberPipeline: LessThanNumberPipeline
)
    : MatchElementPipeline() {
}