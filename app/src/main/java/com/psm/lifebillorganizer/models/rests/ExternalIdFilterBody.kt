package com.psm.lifebillorganizer.models.rests

import com.google.gson.annotations.Expose

class ExternalIdFilterBody(@Expose val externalId: String) {

}