package com.psm.lifebillorganizer.models.rests

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.psm.lifebillorganizer.models.Payer
import com.psm.lifebillorganizer.models.Payment
import com.psm.lifebillorganizer.models.PaymentGroup

class MongoPayerResponse(@Expose @SerializedName("documents") val documents: ArrayList<Payer>) {

    override fun toString(): String {
        val stringReturn = StringBuilder()
        stringReturn.append("PAYERS RETURNED: ")
        for (document in documents) {
            stringReturn.append(" Name: ").append(document.name)
            stringReturn.append(" Description: ").append(document.description)
            stringReturn.append(" ExternalId: ").append(document.externalId)
            stringReturn.append(" OnlineUsed: ").append(document.onlineUsed)
            stringReturn.append(" || ")
        }

        return stringReturn.toString()
    }
}