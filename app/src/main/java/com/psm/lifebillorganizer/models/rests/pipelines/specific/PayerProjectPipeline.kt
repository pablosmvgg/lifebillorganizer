package com.psm.lifebillorganizer.models.rests.pipelines

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class PayerProjectPipelineParent(
    @Expose @SerializedName("\$project") var payerProjectPipeline: PayerProjectPipeline)
    : AggregationPipeline()

class PayerProjectPipeline() {
    @Expose @SerializedName("_id") var id: String = "0";
    @Expose @SerializedName("name") var name: String = "\$payers.name";
    @Expose @SerializedName("description") var description: String = "\$payers.description";
    @Expose @SerializedName("externalId") var externalId: String = "\$payers.externalId";
    @Expose @SerializedName("onlineUsed") var deleted: String = "\$payers.onlineUsed";
    @Expose @SerializedName("payerGroupExternalId") var payerGroupExternalId: String = "\$payers.payerGroupExternalId";

}