package com.psm.lifebillorganizer.models.rests

import com.google.gson.annotations.Expose
import com.psm.lifebillorganizer.models.PaymentGroup

class MongoPaymentGroup(@Expose val document: PaymentGroup) {

}