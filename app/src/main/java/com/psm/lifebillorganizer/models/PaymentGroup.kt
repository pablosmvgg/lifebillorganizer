package com.psm.lifebillorganizer.models

import com.google.gson.annotations.Expose
import java.io.Serializable
import java.time.LocalDateTime

data class PaymentGroup(var id: Long?,
                        @Expose var name: String,
                        @Expose var description: String?,
                        @Expose var createdDate: LocalDateTime,
                        @Expose var periodId: Long ): Serializable {


    var selected: Boolean = false
    lateinit var period: Period

    @Expose lateinit var payers: ArrayList<Payer>
    @Expose var externalId: String? = null
    @Expose lateinit var payments: ArrayList<Payment>
    @Expose var onlinePayersJoined: Long = 0L
}