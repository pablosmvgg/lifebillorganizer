package com.psm.lifebillorganizer.models.rests.pipelines.specific

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.psm.lifebillorganizer.models.rests.pipelines.MatchElementPipeline

class PaymentExternalIdPipeline(@Expose @SerializedName("payments.externalId") var externalId: String)
    : MatchElementPipeline(){
}