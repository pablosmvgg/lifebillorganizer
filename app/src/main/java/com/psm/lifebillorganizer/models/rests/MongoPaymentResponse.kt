package com.psm.lifebillorganizer.models.rests

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.psm.lifebillorganizer.models.Payment
import com.psm.lifebillorganizer.models.PaymentGroup

class MongoPaymentResponse(@Expose @SerializedName("documents") val documents: ArrayList<Payment>) {

    override fun toString(): String {
        val stringReturn = StringBuilder()
        stringReturn.append("PAYMENTS RETURNED: ")
        if (documents.isEmpty()) {
            stringReturn.append("Nothing returned.")
        }
        for (document in documents) {
            stringReturn.append(" Name: ").append(document.name)
            stringReturn.append(" Value: ").append(document.value)
            stringReturn.append(" PayerExternalId: ").append(document.payerExternalId)
            stringReturn.append(" ExternalId: ").append(document.externalId)
            stringReturn.append(" NotifiedPayers: ").append(document.notifiedPayers)
            stringReturn.append(" Deleted: ").append(document.deleted)
            stringReturn.append(" PayerExternalIdModified: ").append(document.payerExternalIdModified)
            stringReturn.append(" || ")
        }

        return stringReturn.toString()
    }
}