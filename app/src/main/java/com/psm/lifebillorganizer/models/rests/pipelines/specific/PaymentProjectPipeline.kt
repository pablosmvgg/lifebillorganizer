package com.psm.lifebillorganizer.models.rests.pipelines

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class PaymentProjectPipelineParent(
    @Expose @SerializedName("\$project") var paymentProjectPipeline: PaymentProjectPipeline)
    : AggregationPipeline()

class PaymentProjectPipeline() {
    @Expose @SerializedName("_id") var id: String = "0";
    @Expose @SerializedName("name") var name: String = "\$payments.name";
    @Expose @SerializedName("description") var description: String = "\$payments.description";
    @Expose @SerializedName("value") var value: String = "\$payments.value";
    @Expose @SerializedName("payerExternalId") var payerExternalId: String = "\$payments.payerExternalId";
    @Expose @SerializedName("createdDate") var createdDate: String = "\$payments.createdDate";
    @Expose @SerializedName("paymentDate") var paymentDate: String = "\$payments.paymentDate";
    @Expose @SerializedName("paymentType") var paymentType: String = "\$payments.paymentType";
    @Expose @SerializedName("paymentRecurrent") var paymentRecurrent: String = "\$payments.paymentRecurrent";
    @Expose @SerializedName("paymentRecurrentCount") var paymentRecurrentCount: String = "\$payments.paymentRecurrentCount";
    @Expose @SerializedName("externalId") var externalId: String = "\$payments.externalId";
    @Expose @SerializedName("notifiedPayers") var notifiedPayers: String = "\$payments.notifiedPayers";
    @Expose @SerializedName("deleted") var deleted: String = "\$payments.deleted";
    @Expose @SerializedName("payerExternalIdModified") var payerExternalIdModified: String = "\$payments.payerExternalIdModified";
}