package com.psm.lifebillorganizer.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable
import java.time.LocalDateTime

data class Payer(@Expose var id: Long?,
                 @Expose var name: String,
                 @Expose var createdDate: LocalDateTime,
                 @Expose var externalId: String?): Serializable {

    @Expose lateinit var description: String

    var editedDate: LocalDateTime? = null
        //Example custom get and set:
        /*set(value) {
            field = value
        }
        get() {
            return field
        }*/
    var deletedDate: LocalDateTime? = null
    var selected: Boolean = false
    var isMainPayer: Boolean = false
    @Expose var onlineUsed: Boolean = false
    @Expose var payerGroupExternalId: String? = null

    fun toStringComplete(): String {
        val stringReturn = StringBuilder()
        stringReturn.append("PAYER RETURNED: ")
        stringReturn.append(" Name: ").append(name)
        stringReturn.append(" IsMainPayer: ").append(isMainPayer)
        stringReturn.append(" ExternalId: ").append(externalId)
        stringReturn.append(" PayerGroupExternalId: ").append(payerGroupExternalId)
        stringReturn.append(" || ")

        return stringReturn.toString()
    }

    override fun toString(): String {
        return name
    }
}