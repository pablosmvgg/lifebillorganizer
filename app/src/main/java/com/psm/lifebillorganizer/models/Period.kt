package com.psm.lifebillorganizer.models

import java.io.Serializable

data class Period(val id: Number,
             val name: String,
             val description: String): Serializable {

    override fun toString(): String {
        return name
    }

}