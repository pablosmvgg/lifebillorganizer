package com.psm.lifebillorganizer.models.rests

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.time.LocalDateTime

class SetPayment(@Expose @SerializedName("payments.\$.name") var name: String,
                 @Expose @SerializedName("payments.\$.description") var description: String,
                 @Expose @SerializedName("payments.\$.value") var value: Double,
                 @Expose @SerializedName("payments.\$.paymentDate") var paymentDate: LocalDateTime,
                 @Expose @SerializedName("payments.\$.paymentRecurrent") var paymentRecurrent: Boolean,
                 @Expose @SerializedName("payments.\$.paymentRecurrentCount") var paymentRecurrentCount: Long,
                 @Expose @SerializedName("payments.\$.deleted") var deleted: Boolean,
                 @Expose @SerializedName("payments.\$.notifiedPayers") var notifiedPayers: Number,
                 @Expose @SerializedName("payments.\$.payerExternalIdModified") var payerExternalIdModified: String) {
}