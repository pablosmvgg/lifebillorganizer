package com.psm.lifebillorganizer.models

import java.io.Serializable
import java.time.LocalDateTime

data class PaymentPeriod(
    val name: String,
    val startDate: LocalDateTime,
    val endDate: LocalDateTime,
    val paymentGroup: PaymentGroup,
    val payments: ArrayList<Payment>
): Serializable {


}