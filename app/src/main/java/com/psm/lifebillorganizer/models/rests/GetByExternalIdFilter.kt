package com.psm.lifebillorganizer.models.rests

import com.google.gson.annotations.Expose

class GetByExternalIdFilter(@Expose val externalId: String) {
}