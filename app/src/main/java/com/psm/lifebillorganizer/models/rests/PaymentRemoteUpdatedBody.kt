package com.psm.lifebillorganizer.models.rests

import com.google.gson.annotations.Expose
import com.psm.lifebillorganizer.models.rests.pipelines.AggregationPipeline

class PaymentRemoteUpdatedBody(@Expose var collection: String,
                               @Expose var database: String,
                               @Expose var dataSource: String,
                               @Expose var pipeline: Array<AggregationPipeline>
) {

}