package com.psm.lifebillorganizer.models.enums

import java.io.Serializable

enum class PaymentType(val paymentTypeId: Long): Serializable {

    COMMON(1), BALANCE(2);

    companion object {
        fun getPaymentType(id: Long): PaymentType? {
            when(id) {
                1L -> return COMMON
                2L -> return BALANCE
            }
            return null
        }
    }

}