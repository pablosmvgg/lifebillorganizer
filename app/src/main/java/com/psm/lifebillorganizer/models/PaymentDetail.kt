package com.psm.lifebillorganizer.models

class PaymentDetail {

    private var  total: Double = 0.0
    private var  totalEachPayer: Double = 0.0
    private lateinit var paymentPayerDetails: ArrayList<PaymentPayerDetail>
    private lateinit var payerBalances: ArrayList<PaymentPayerDetail>
    private lateinit var payerDebts: ArrayList<PaymentPayerDetail>

    fun getTotal(): Double {
        return total
    }
    fun setTotal(total: Double) {
        this.total = total
    }

    fun getTotalEachPayer(): Double {
        return totalEachPayer
    }
    fun setTotalEachPayer(totalEachPayer: Double) {
        this.totalEachPayer = totalEachPayer
    }

    fun getPaymentPayerDetail(): ArrayList<PaymentPayerDetail> {
        return paymentPayerDetails
    }
    fun setPaymentPayerDetail(paymentPayerDetails: ArrayList<PaymentPayerDetail>) {
        this.paymentPayerDetails = paymentPayerDetails
    }

    fun getPayerBalances(): ArrayList<PaymentPayerDetail> {
        return payerBalances
    }
    fun setPayerBalances(payerBalances: ArrayList<PaymentPayerDetail>) {
        this.payerBalances = payerBalances
    }

    fun getPayerDebts(): ArrayList<PaymentPayerDetail> {
        return payerDebts
    }
    fun setPayerDebts(payerDebts: ArrayList<PaymentPayerDetail>) {
        this.payerDebts = payerDebts
    }

}