package com.psm.lifebillorganizer.models.rests.pipelines

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class AndPipeline(@Expose @SerializedName("\$and") var andValue: Array<MatchElementPipeline>) {
}