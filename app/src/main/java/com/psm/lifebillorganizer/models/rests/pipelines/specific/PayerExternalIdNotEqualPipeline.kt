package com.psm.lifebillorganizer.models.rests.pipelines.specific

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.psm.lifebillorganizer.models.rests.pipelines.MatchElementPipeline
import com.psm.lifebillorganizer.models.rests.pipelines.NotEqualPipeline

class PayerExternalIdNotEqualPipeline(
    @Expose @SerializedName("payers.externalId") var payersExternalId: NotEqualPipeline)
    : MatchElementPipeline() {
}