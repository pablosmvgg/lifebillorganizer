package com.psm.lifebillorganizer.models.rests

import com.google.gson.annotations.Expose
import com.psm.lifebillorganizer.models.PaymentGroup

class InsertPaymentGroupBody(@Expose var collection: String,
                             @Expose var database: String,
                             @Expose var dataSource: String,
                             @Expose var document: PaymentGroup) {
}