package com.psm.lifebillorganizer.models

data class FirstTimeExec(val id: Number, val executed: Boolean) {

}