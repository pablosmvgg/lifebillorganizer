package com.psm.lifebillorganizer.models.rests.pipelines

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/*
   API: https://www.mongodb.com/docs/atlas/api/data-api-resources/
   array of objects
 */

class UnwindPipelineParent(@Expose @SerializedName("\$unwind") var unwindPipeline: UnwindPipeline)
    : AggregationPipeline()

class UnwindPipeline(
    @Expose @SerializedName("path") var path: String
    )  {
}