package com.psm.lifebillorganizer.models.rests.pipelines

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class LessThanPipeline(@Expose @SerializedName("\$lt") var lessThanValue: String)
    : MatchElementPipeline() {
}