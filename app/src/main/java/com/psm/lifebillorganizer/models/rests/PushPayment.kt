package com.psm.lifebillorganizer.models.rests

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.psm.lifebillorganizer.models.Payment

class PushPayment(@Expose @SerializedName("payments") var payment: Payment) {
}