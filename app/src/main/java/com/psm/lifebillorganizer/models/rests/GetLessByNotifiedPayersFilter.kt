package com.psm.lifebillorganizer.models.rests

import com.google.gson.annotations.Expose

class GetLessByNotifiedPayersFilter(@Expose val notifiedPayers: String) {
}