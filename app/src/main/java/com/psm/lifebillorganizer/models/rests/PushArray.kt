package com.psm.lifebillorganizer.models.rests

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class PushArray(@Expose @SerializedName("\$push") var pushPayment: PushPayment) {
}