package com.psm.lifebillorganizer.models.rests.pipelines

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class NotEqualPipeline(@Expose @SerializedName("\$ne") var NotEqualValue: String)
    : MatchElementPipeline() {
}