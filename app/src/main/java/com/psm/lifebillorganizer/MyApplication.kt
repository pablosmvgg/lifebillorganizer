package com.psm.lifebillorganizer


import android.app.Application
import com.google.gson.*
import com.psm.lifebillorganizer.android.adapters.PaymentGroupAdapter
import com.psm.lifebillorganizer.android.services.PaymentRecurrentService
import com.psm.lifebillorganizer.dao.DatabaseDao
import com.psm.lifebillorganizer.factories.*
import com.psm.lifebillorganizer.services.*
import com.psm.lifebillorganizer.android.ui.FirstTimeActivity
import com.psm.lifebillorganizer.android.ui.MainActivity
import com.psm.lifebillorganizer.android.ui.options.OptionsFragment
import com.psm.lifebillorganizer.android.ui.options.OptionsViewModel
import com.psm.lifebillorganizer.android.ui.payers.PayerFragment
import com.psm.lifebillorganizer.android.ui.paymentgroups.JoinPaymentGroupFragment
import com.psm.lifebillorganizer.android.ui.paymentgroups.PaymentGroupFragment
import com.psm.lifebillorganizer.android.ui.paymentgroups.PaymentGroupSelectionFragment
import com.psm.lifebillorganizer.android.ui.paymentperiods.PaymentPeriodDetailFragment
import com.psm.lifebillorganizer.android.ui.paymentperiods.PaymentPeriodsFragment
import com.psm.lifebillorganizer.android.ui.payments.*
import dagger.Component
import dagger.Module
import dagger.Provides
import okhttp3.Interceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton
import okhttp3.OkHttpClient
import okhttp3.Request
import retrofit2.Response
import java.io.IOException

import com.psm.lifebillorganizer.utils.FormatUtil
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter


// https://medium.com/@carloslopez_19744/dagger2-parte-2-%EF%B8%8F-ejemplo-facilito-en-android-con-kotlin-b0c70cf202c3
//https://developer.android.com/training/dependency-injection/dagger-android?hl=es-419

@Singleton
@Component(modules = [MyModule::class])
interface ApplicationComponent {

    fun inject(activity: MainActivity)
    fun inject(activity: FirstTimeActivity)

    fun inject(dashboardFragment: PaymentPeriodsFragment)
    fun inject(optionsFragment: OptionsFragment)
    fun inject(payerFragment: PayerFragment)
    fun inject(paymentFragment: PaymentFragment)
    fun inject(paymentGroupFragment: PaymentGroupFragment)
    fun inject(paymentGroupSelectionFragment: PaymentGroupSelectionFragment)
    fun inject(paymentsFragment: PaymentsFragment)
    fun inject(previousPaymentsFragment: PreviousPaymentsFragment)
    fun inject(paymentPeriodDetailFragment: PaymentPeriodDetailFragment)
    fun inject(balancePaymentsFragment: BalancePaymentsFragment)
    fun inject(joinPaymentGroupFragment: JoinPaymentGroupFragment)

    fun inject(paymentGroupAdapter: PaymentGroupAdapter)

    fun inject(paymentService: PaymentService)

    fun inject(paymentRecurrentService: PaymentRecurrentService)

}

@Module
class MyModule {
    @Singleton
    @Provides
    fun provideDatabaseDao(): DatabaseDao
    {
        return DatabaseDao()
    }

    @Singleton
    @Provides
    fun providePayerFactory(): PayerFactory
    {
        return PayerFactory()
    }

    @Singleton
    @Provides
    fun providePaymentFactory(): PaymentFactory
    {
        return PaymentFactory()
    }

    @Singleton
    @Provides
    fun providePaymentGroupFactory(): PaymentGroupFactory
    {
        return PaymentGroupFactory()
    }

    @Singleton
    @Provides
    fun providePeriodFactory(): PeriodFactory
    {
        return PeriodFactory()
    }

    @Singleton
    @Provides
    fun providePaymentPeriodFactory(): PaymentPeriodFactory
    {
        return PaymentPeriodFactory()
    }

    @Singleton
    @Provides
    fun providePaymentsDetailFactory(): PaymentsDetailFactory
    {
        return PaymentsDetailFactory()
    }

    @Singleton
    @Provides
    fun providePaymentPayerDetailFactory(): PaymentPayerDetailFactory
    {
        return PaymentPayerDetailFactory()
    }
    @Singleton
    @Provides
    fun provideFirstTimeExecFactory(): FirstTimeExecFactory
    {
        return FirstTimeExecFactory()
    }
    @Singleton
    @Provides
    fun providePaymentRemoteUpdatedBodyFactory(): PaymentRemoteUpdatedBodyFactory
    {
        return PaymentRemoteUpdatedBodyFactory()
    }
    @Singleton
    @Provides
    fun providePayerRemoteUpdatedBodyFactory(): PayerRemoteUpdatedBodyFactory
    {
        return PayerRemoteUpdatedBodyFactory()
    }
    @Singleton
    @Provides
    fun provideInsertPaymentInPaymentGroupBodyFactory(): InsertPaymentInPaymentGroupBodyFactory
    {
        return InsertPaymentInPaymentGroupBodyFactory()
    }
    @Singleton
    @Provides
    fun provideUpdatePaymentInPaymentGroupBodyFactory(): UpdatePaymentInPaymentGroupBodyFactory
    {
        return UpdatePaymentInPaymentGroupBodyFactory()
    }

    @Singleton
    @Provides
    fun providePaymentService(paymentFactory: PaymentFactory,
                              payerFactory: PayerFactory,
                              paymentsDetailFactory: PaymentsDetailFactory,
                              paymentPayerDetailFactory: PaymentPayerDetailFactory,
                              databaseDao: DatabaseDao): PaymentService {
        return PaymentServiceImpl(paymentFactory, payerFactory, paymentsDetailFactory,
            paymentPayerDetailFactory, databaseDao)
    }

    @Singleton
    @Provides
    fun providePayerService(payerFactory: PayerFactory,
                              databaseDao: DatabaseDao,
                              paymentGroupService: PaymentGroupService): PayerService {
        return PayerServiceImpl(payerFactory, databaseDao, paymentGroupService)
    }

    @Singleton
    @Provides
    fun providePeriodService(periodFactory: PeriodFactory, databaseDao: DatabaseDao): PeriodService {
        return PeriodServiceImpl(periodFactory, databaseDao)
    }

    @Singleton
    @Provides
    fun providePaymentGroupService(paymentGroupFactory: PaymentGroupFactory,
                                   payerFactory: PayerFactory,
                                   databaseDao: DatabaseDao,
                                   retrofit: Retrofit,
                                   paymentService: PaymentService,
                                   periodService: PeriodService):
            PaymentGroupService {
        return PaymentGroupServiceImpl(paymentGroupFactory, payerFactory,
            databaseDao, retrofit, paymentService, periodService)
    }

    @Singleton
    @Provides
    fun providePaymentPeriodService(paymentGroupService: PaymentGroupService,
                                    paymentService: PaymentService,
                                    paymentPeriodFactory: PaymentPeriodFactory): PaymentPeriodService {
        return PaymentPeriodServiceImpl(paymentGroupService, paymentService, paymentPeriodFactory)
    }

    @Singleton
    @Provides
    fun provideFirstTimeExecService(firstTimeExecFactory: FirstTimeExecFactory,
                                    databaseDao: DatabaseDao): FirstTimeExecService {
        return FirstTimeExecServiceImpl(firstTimeExecFactory, databaseDao)
    }

    @Singleton
    @Provides
    fun providePaymentGroupOnlineService(paymentFactory: PaymentFactory,
                                         paymentRemoteUpdatedBodyFactory: PaymentRemoteUpdatedBodyFactory,
                                         payerRemoteUpdatedBodyFactory: PayerRemoteUpdatedBodyFactory,
                                         insertPaymentInPaymentGroupBodyFactory: InsertPaymentInPaymentGroupBodyFactory,
                                         updatePaymentInPaymentGroupBodyFactory: UpdatePaymentInPaymentGroupBodyFactory,
                                         paymentGroupService: PaymentGroupService,
                                         paymentService: PaymentService,
                                         payerService: PayerService,
                                         databaseDao: DatabaseDao,
                                         retrofit: Retrofit): PaymentGroupOnlineService {
        return PaymentGroupOnlineServiceImpl(paymentFactory, paymentRemoteUpdatedBodyFactory,
            payerRemoteUpdatedBodyFactory, insertPaymentInPaymentGroupBodyFactory,
            updatePaymentInPaymentGroupBodyFactory, paymentGroupService, paymentService,
            payerService, databaseDao, retrofit)
    }


    @Singleton
    @Provides
    fun provideRetrofit(): Retrofit {

        val httpClient = OkHttpClient.Builder()

        httpClient.addInterceptor { chain ->
            val request = chain.request().newBuilder()
                .addHeader("Content-Type", "application/json")
                .addHeader("Access-Control-Request-Headers", "*")
                .addHeader("api-key", "UwypHYi5IqdP1rSE5IJyw9W9amJvBJIghNTzysVYtN7jbs2FNC7Oy4XXHrxqcxLv")
                .build()
            chain.proceed(request)
        }
        val gson = GsonBuilder()
            .excludeFieldsWithoutExposeAnnotation()
            .registerTypeAdapter(LocalDateTime::class.java,
            JsonDeserializer<Any?> { json, _, _ ->
                LocalDateTime.parse(json.asString, FormatUtil.getMongoDateFormatter())
            })
            .registerTypeAdapter(LocalDateTime::class.java,
            JsonSerializer<LocalDateTime?> { element, _, _ ->
                JsonPrimitive(FormatUtil.formatDBDate(element))
            })
            .create()

        return Retrofit.Builder()
            .baseUrl("https://data.mongodb-api.com/app/data-rkubr/endpoint/data/beta/action/")
            .client(httpClient.build())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
    }

    //ViewModels
    @Singleton
    @Provides
    fun provideOptionsViewModel(paymentGroupService: PaymentGroupService,
                                payerService: PayerService): OptionsViewModel {
        return OptionsViewModel(paymentGroupService, payerService)
    }

    @Singleton
    @Provides
    fun providePaymentsViewModel(paymentService: PaymentService,
                                 paymentGroupService: PaymentGroupService): PaymentsViewModel {
        return PaymentsViewModel(paymentService, paymentGroupService)
    }
}

class MyApplication: Application() {
    // Reference to the application graph that is used across the whole app
    val appComponent = DaggerApplicationComponent.create()


}