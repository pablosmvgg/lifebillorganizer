package com.psm.lifebillorganizer.utils

import java.text.NumberFormat
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*


object FormatUtil {
    fun formatPrice(currPrice: Double?): String {
        val format = NumberFormat.getCurrencyInstance()
        return format.format(currPrice)
    }

    fun formatDBDate(date: LocalDateTime?): String {
        if (date != null) {
            return date.format(getDBFormatDate())
        }
        return ""
    }

    fun formatDate(date: LocalDateTime?): String {
        if (date != null) {
            return date.format(getFormatDate())
        }
        return ""
    }

    fun formatDateWithTime0(dateWithoutTime: String): LocalDateTime {
        return LocalDateTime.parse("$dateWithoutTime 00:00:00", getFormatDate())
    }

    fun getDBFormatDate(): DateTimeFormatter {
        return DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss")
    }

    fun getFormatDate(): DateTimeFormatter {
        //TODO: ver como cambiar el formato dependiendo el pais del locale
        return DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss")
    }

    fun getSimpleFormatDate(): DateTimeFormatter {
        //TODO: ver como cambiar el formato dependiendo el pais del locale
        return DateTimeFormatter.ofPattern("dd/MM/yyyy")
    }

    fun getMongoDateFormatter(): DateTimeFormatter {
        return DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss")
    }

    fun formatNumber2Digits(number: Int): String {
        return if (number.toString().length == 1) {
            String.format(Locale.getDefault(), "%02d", number)
        } else number.toString()
    }

    fun formatDouble2Digits(number: Double?): String {
        return String.format(Locale.getDefault(), "%.2f", number)
    }

    fun formatDouble2DigitsMoney(number: Double?): String {
        return "$" + String.format(Locale.getDefault(), "%.2f", number)
    }
}