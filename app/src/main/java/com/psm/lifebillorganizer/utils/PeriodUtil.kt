package com.psm.lifebillorganizer.utils

import java.time.DayOfWeek
import java.time.LocalDateTime
import java.util.*

object PeriodUtil {

    private const val monthlyId: Long = 1
    private const val quarterlyId: Long = 2
    private const val weeklyId: Long = 3

    fun getFirstDatePeriod(periodId: Long, dateToUse: LocalDateTime): LocalDateTime? {
        var dateToReturn: LocalDateTime? = null

        when (periodId) {
            monthlyId -> {
                dateToReturn = LocalDateTime.of(dateToUse.year, dateToUse.monthValue, 1,
                    0, 0, 0)
            }
            quarterlyId -> {
                dateToReturn = if (dateToUse.dayOfMonth <= 15) {
                    LocalDateTime.of(dateToUse.year, dateToUse.monthValue, 1,
                        0, 0, 0)
                } else {
                    LocalDateTime.of(dateToUse.year, dateToUse.monthValue, 15,
                        0, 0, 0)
                }
            }
            weeklyId -> {
                //TODO: sumar logica para recuperar lunes o domingo, por ahora sera lunes siempre
                dateToReturn = dateToUse.with(DayOfWeek.MONDAY)
            }
            //else -> {}
        }

        return dateToReturn
    }

    fun getLastDatePeriod(periodId: Long, dateToUse: LocalDateTime): LocalDateTime? {
        var dateToReturn: LocalDateTime? = null
        when (periodId) {
            monthlyId -> {
                dateToReturn = LocalDateTime.of(dateToUse.year, dateToUse.monthValue,
                    dateToUse.toLocalDate().lengthOfMonth(),
                    23, 59, 59)
            }
            quarterlyId -> {
                dateToReturn = if (dateToUse.dayOfMonth <= 15) {
                    LocalDateTime.of(dateToUse.year, dateToUse.monthValue, 15,
                        23, 59, 59)
                } else {
                    LocalDateTime.of(dateToUse.year, dateToUse.monthValue,
                        dateToUse.toLocalDate().lengthOfMonth(), 23, 59, 59)
                }
            }
            weeklyId -> {
                //TODO: sumar logica para recuperar lunes o domingo, por ahora sera domingo siempre
                dateToReturn = dateToUse.with(DayOfWeek.SUNDAY)
            }
            //else -> {}
        }

        return dateToReturn
    }

    fun getNextTimePeriod(periodId: Long): Calendar {
        val nextExecutionTime = getLastDatePeriod(periodId, LocalDateTime.now())!!.plusDays(1)
        val executionTime = Calendar.getInstance()
        executionTime.set(Calendar.YEAR, nextExecutionTime.year)
        executionTime.set(Calendar.MONTH, nextExecutionTime.monthValue - 1)
        executionTime.set(Calendar.DAY_OF_MONTH, nextExecutionTime.dayOfMonth)
        executionTime.set(Calendar.HOUR, 0)
        executionTime.set(Calendar.MINUTE, 1)
        executionTime.set(Calendar.SECOND, 0)
        executionTime.set(Calendar.AM_PM, Calendar.AM)

        //executionTime.timeInMillis = System.currentTimeMillis()
        //executionTime.add(Calendar.SECOND, 15)

        return executionTime
    }


}