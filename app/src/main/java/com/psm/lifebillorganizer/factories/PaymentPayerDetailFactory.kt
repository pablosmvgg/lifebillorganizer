package com.psm.lifebillorganizer.factories

import com.psm.lifebillorganizer.models.Payer
import com.psm.lifebillorganizer.models.PaymentPayerDetail

class PaymentPayerDetailFactory {

    fun create(
        payer: Payer,
        totalPaid: Double
    ): PaymentPayerDetail {

        val paymentsDetail = PaymentPayerDetail()
        paymentsDetail.payer = payer
        paymentsDetail.totalPaid = totalPaid

        return paymentsDetail
    }
}