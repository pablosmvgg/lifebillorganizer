package com.psm.lifebillorganizer.factories

import com.psm.lifebillorganizer.daos.FirstTimeExec
import com.psm.lifebillorganizer.daos.Period

class FirstTimeExecFactory {

    fun create(firstTimeExec: FirstTimeExec): com.psm.lifebillorganizer.models.FirstTimeExec {
        var executed = false
        if (firstTimeExec.executed > 0) {
            executed = true
        }
        return com.psm.lifebillorganizer.models
            .FirstTimeExec(firstTimeExec.id, executed)
    }
}