package com.psm.lifebillorganizer.factories

import com.psm.lifebillorganizer.consts.AppConsts
import com.psm.lifebillorganizer.models.Payer
import com.psm.lifebillorganizer.models.Payment
import com.psm.lifebillorganizer.models.PaymentGroup
import com.psm.lifebillorganizer.models.rests.PaymentRemoteUpdatedBody
import com.psm.lifebillorganizer.models.rests.pipelines.*
import com.psm.lifebillorganizer.models.rests.pipelines.specific.ExternalIdPipeline
import com.psm.lifebillorganizer.models.rests.pipelines.specific.PaymentExternalIdPipeline
import com.psm.lifebillorganizer.models.rests.pipelines.specific.PaymentNotifiedPayerPipeline
import com.psm.lifebillorganizer.models.rests.pipelines.specific.PaymentPayerExternalIdModifiedPipeline

class PaymentRemoteUpdatedBodyFactory {

    fun createByNotified(group: PaymentGroup, mainPayer: Payer): PaymentRemoteUpdatedBody {

        val matchPipeline = MatchPipeline(arrayOf(
            ExternalIdPipeline(group.externalId!!),
            PaymentNotifiedPayerPipeline(LessThanNumberPipeline(group.payers.size)),
            PaymentPayerExternalIdModifiedPipeline(NotEqualPipeline(mainPayer.payerGroupExternalId!!))
        ))
        val pipeline = arrayOf(
            UnwindPipelineParent(UnwindPipeline("\$payments")),
            MatchPipelineParent(matchPipeline),
            PaymentProjectPipelineParent(PaymentProjectPipeline())
        )
        return PaymentRemoteUpdatedBody(collection = AppConsts.MONGODB_PAYMENT_GROUP_COLLECTION,
            database = AppConsts.MONGODB_DATABASE,
            dataSource = AppConsts.MONGODB_DATASOURCE,
            pipeline = pipeline
        )
    }

    fun createByExternalId(group: PaymentGroup, payment: Payment): PaymentRemoteUpdatedBody {

        val matchPipeline = MatchPipeline(arrayOf(
            ExternalIdPipeline(group.externalId!!),
            PaymentExternalIdPipeline(payment.externalId!!)
        ))
        val pipeline = arrayOf(
            UnwindPipelineParent(UnwindPipeline("\$payments")),
            MatchPipelineParent(matchPipeline),
            PaymentProjectPipelineParent(PaymentProjectPipeline())
        )
        return PaymentRemoteUpdatedBody(collection = AppConsts.MONGODB_PAYMENT_GROUP_COLLECTION,
            database = AppConsts.MONGODB_DATABASE,
            dataSource = AppConsts.MONGODB_DATASOURCE,
            pipeline = pipeline
        )
    }
}