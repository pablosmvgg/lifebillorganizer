package com.psm.lifebillorganizer.factories

import com.psm.lifebillorganizer.daos.Period
import javax.inject.Inject

class PeriodFactory @Inject constructor() {

    fun create(periodDao: Period): com.psm.lifebillorganizer.models.Period {
        return com.psm.lifebillorganizer.models.Period(periodDao.id, periodDao.name,
            periodDao.description)
    }

    fun createList(periodDaos: List<Period>): ArrayList<com.psm.lifebillorganizer.models.Period> {
        val periods = ArrayList<com.psm.lifebillorganizer.models.Period>()
        for (periodDao in periodDaos) {
            periods.add(this.create(periodDao))
        }

        return periods
    }
}