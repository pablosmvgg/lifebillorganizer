package com.psm.lifebillorganizer.factories

import com.psm.lifebillorganizer.consts.AppConsts
import com.psm.lifebillorganizer.models.Payer
import com.psm.lifebillorganizer.models.PaymentGroup
import com.psm.lifebillorganizer.models.rests.PayerRemoteUpdatedBody
import com.psm.lifebillorganizer.models.rests.PaymentRemoteUpdatedBody
import com.psm.lifebillorganizer.models.rests.pipelines.*
import com.psm.lifebillorganizer.models.rests.pipelines.specific.ExternalIdPipeline
import com.psm.lifebillorganizer.models.rests.pipelines.specific.PayerExternalIdNotEqualPipeline
import com.psm.lifebillorganizer.models.rests.pipelines.specific.PaymentNotifiedPayerPipeline
import com.psm.lifebillorganizer.models.rests.pipelines.specific.PaymentPayerExternalIdModifiedPipeline

class PayerRemoteUpdatedBodyFactory {

    fun create(group: PaymentGroup, mainPayer: Payer): PayerRemoteUpdatedBody {

        val matchPipeline = MatchPipeline(arrayOf(
            ExternalIdPipeline(group.externalId!!),
            PayerExternalIdNotEqualPipeline(NotEqualPipeline(mainPayer.externalId!!))
        ))
        val pipeline = arrayOf(
            UnwindPipelineParent(UnwindPipeline("\$payers")),
            MatchPipelineParent(matchPipeline),
            PayerProjectPipelineParent(PayerProjectPipeline())
        )
        return PayerRemoteUpdatedBody(collection = AppConsts.MONGODB_PAYMENT_GROUP_COLLECTION,
            database = AppConsts.MONGODB_DATABASE,
            dataSource = AppConsts.MONGODB_DATASOURCE,
            pipeline = pipeline
        )
    }
}