package com.psm.lifebillorganizer.factories

import com.psm.lifebillorganizer.models.PaymentDetail
import com.psm.lifebillorganizer.models.PaymentPayerDetail

class PaymentsDetailFactory {

    fun create(
        total: Double,
        totalEachPayer: Double,
        paymentPayerDetails: ArrayList<PaymentPayerDetail>,
        payerBalances: ArrayList<PaymentPayerDetail>,
        payerDebts: ArrayList<PaymentPayerDetail>
    ): PaymentDetail {

        val paymentsDetail = PaymentDetail()
        paymentsDetail.setTotal(total)
        paymentsDetail.setTotalEachPayer(totalEachPayer)
        paymentsDetail.setPaymentPayerDetail(paymentPayerDetails)
        paymentsDetail.setPayerBalances(payerBalances)
        paymentsDetail.setPayerDebts(payerDebts)

        return paymentsDetail
    }
}