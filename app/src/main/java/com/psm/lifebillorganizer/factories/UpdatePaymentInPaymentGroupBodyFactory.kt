package com.psm.lifebillorganizer.factories

import com.psm.lifebillorganizer.consts.AppConsts
import com.psm.lifebillorganizer.models.Payment
import com.psm.lifebillorganizer.models.rests.PaymentIdsFilterBody
import com.psm.lifebillorganizer.models.rests.SetElement
import com.psm.lifebillorganizer.models.rests.SetPayment
import com.psm.lifebillorganizer.models.rests.UpdatePaymentInPaymentGroupBody

class UpdatePaymentInPaymentGroupBodyFactory {

    fun create(payment: Payment, payerExternalId: String, paymentGroupExternalId: String):
            UpdatePaymentInPaymentGroupBody {
        val setPayment = SetElement(
            SetPayment(
                name = payment.name,
                description = payment.description!!,
                value = payment.value,
                paymentDate = payment.paymentDate,
                paymentRecurrent = payment.paymentRecurrent,
                paymentRecurrentCount = payment.paymentRecurrentCount,
                deleted = payment.deleted,
                notifiedPayers = 1,
                payerExternalIdModified = payerExternalId
            )
        )
        return UpdatePaymentInPaymentGroupBody(
            collection = AppConsts.MONGODB_PAYMENT_GROUP_COLLECTION,
            database = AppConsts.MONGODB_DATABASE,
            dataSource = AppConsts.MONGODB_DATASOURCE,
            update = setPayment,
            filter = PaymentIdsFilterBody(
                externalId = paymentGroupExternalId,
                paymentExternalId = payment.externalId!!
            )
        )
    }

}