package com.psm.lifebillorganizer.factories

import com.psm.lifebillorganizer.models.Payment
import com.psm.lifebillorganizer.models.rests.MongoPaymentResponse
import com.psm.lifebillorganizer.utils.FormatUtil
import java.time.LocalDateTime
import javax.inject.Inject
import kotlin.collections.ArrayList

class PaymentFactory @Inject constructor() {
    fun create(
        id: Long, name: String, description: String?, value: Double,
        payerId: Long, payerExternalId: String, paymentGroupId: Long, createdDate: LocalDateTime, paymentDate: LocalDateTime,
        paymentType: Long, paymentRecurrent: Boolean, paymentRecurrentCount: Long, externalId: String?,
        notifiedPayers: Long?, deleted: Boolean, payerExternalIdModified: String?, isSubmitted: Boolean
    ): Payment {

        return Payment(id, name, description, value, payerId, payerExternalId, paymentGroupId,
            createdDate, paymentDate, paymentType, paymentRecurrent, paymentRecurrentCount,
            externalId, notifiedPayers, deleted, payerExternalIdModified, isSubmitted)
    }

    fun create(paymentDao: com.psm.lifebillorganizer.daos.Payment): Payment {

        var paymentRecurrent = false
        if (paymentDao.paymentRecurrent > 0) {
            paymentRecurrent = true
        }
        var deleted = false
        if (paymentDao.deleted!! > 0) {
            deleted = true
        }
        var isSubmitted = false
        if (paymentDao.isSubmitted!! > 0) {
            isSubmitted = true
        }
        return Payment(paymentDao.id, paymentDao.name, paymentDao.description,
            paymentDao.value, paymentDao.payerId, paymentDao.payerExternalId,
            paymentDao.paymentGroupId, LocalDateTime.parse(paymentDao.createdDate,
                FormatUtil.getDBFormatDate()),
            LocalDateTime.parse(paymentDao.paymentDate, FormatUtil.getDBFormatDate()),
            paymentDao.paymentType, paymentRecurrent, paymentDao.paymentRecurrentCount,
            paymentDao.externalId, paymentDao.notifiedPayers, deleted,
            paymentDao.payerExternalIdModified, isSubmitted)
    }


    fun createList(payerDaos: List<com.psm.lifebillorganizer.daos.Payment>): ArrayList<Payment> {
        val payments = ArrayList<Payment>()
        for (payerDao in payerDaos) {
            payments.add(this.create(payerDao))
        }

        return payments
    }

}