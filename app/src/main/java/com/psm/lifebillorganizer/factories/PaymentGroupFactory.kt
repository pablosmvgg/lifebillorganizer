package com.psm.lifebillorganizer.factories

import com.psm.lifebillorganizer.models.PaymentGroup
import com.psm.lifebillorganizer.utils.FormatUtil
import java.time.LocalDateTime
import javax.inject.Inject

class PaymentGroupFactory @Inject constructor() {

    fun create(
        id: Long, externalId: String?, name: String, description: String?, createdDate: LocalDateTime,
        periodId: Long, selected: Boolean = false, onlinePayersJoined: Long
    ): PaymentGroup {
        val paymentGroup = PaymentGroup(id, name, description, createdDate, periodId)
        paymentGroup.selected =selected
        paymentGroup.externalId = externalId
        paymentGroup.onlinePayersJoined = onlinePayersJoined
        return paymentGroup
    }

    fun create(paymentGroupDao: com.psm.lifebillorganizer.daos.PaymentGroup): PaymentGroup {
        val paymentGroup = PaymentGroup(paymentGroupDao.id, paymentGroupDao.name, paymentGroupDao.description,
            LocalDateTime.parse(paymentGroupDao.createdDate, FormatUtil.getDBFormatDate()), paymentGroupDao.periodId
        )
        paymentGroup.selected = paymentGroupDao.selected > 0
        paymentGroup.externalId = paymentGroupDao.externalId
        return paymentGroup
    }

    fun createList(paymentGroupDaos: List<com.psm.lifebillorganizer.daos.PaymentGroup>):
            ArrayList<PaymentGroup> {
        val paymentGroups = ArrayList<PaymentGroup>()
        for (paymentGroupDao in paymentGroupDaos) {
            paymentGroups.add(this.create(paymentGroupDao))
        }

        return paymentGroups
    }

}