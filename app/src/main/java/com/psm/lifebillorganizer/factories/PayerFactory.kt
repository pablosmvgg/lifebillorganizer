package com.psm.lifebillorganizer.factories

import com.psm.lifebillorganizer.models.Payer
import com.psm.lifebillorganizer.utils.FormatUtil
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import javax.inject.Inject

class PayerFactory @Inject constructor() {
    fun create(id: Long?, name: String, description: String, createdDate: LocalDateTime,
               editedDate: LocalDateTime?, deletedDate: LocalDateTime?, externalId: String?,
               isMainPayer: Boolean, onlineUsed: Boolean): Payer {
        val payer = Payer(id, name, createdDate, externalId)
        payer.description = description
        payer.editedDate = editedDate
        payer.deletedDate = deletedDate
        payer.isMainPayer = isMainPayer
        payer.onlineUsed = onlineUsed
        return payer
    }

    fun create(payerDao: com.psm.lifebillorganizer.daos.Payer): Payer {
        val formatter = FormatUtil.getDBFormatDate()
        val payer = Payer(payerDao.id, payerDao.name,
            LocalDateTime.parse(payerDao.createdDate, formatter), payerDao.externalId)
        payer.description = payerDao.description.toString()
        if (payerDao.editedDate != null) {
            payer.editedDate = LocalDateTime.parse(payerDao.editedDate,formatter)
        }
        if (payerDao.deletedDate != null) {
            payer.deletedDate = LocalDateTime.parse(payerDao.deletedDate, formatter)
        }
        payer.isMainPayer = false
        if (payerDao.isMainPayer == 1L) {
            payer.isMainPayer = true
        }
        payer.onlineUsed = false
        if (payerDao.onlineUsed == 1L) {
            payer.onlineUsed = true
        }

        return payer
    }


    fun createList(payerDaos: List<com.psm.lifebillorganizer.daos.Payer>): ArrayList<Payer> {
        val payers = ArrayList<Payer>()
        for (payerDao in payerDaos) {
            payers.add(this.create(payerDao))
        }

        return payers
    }

}