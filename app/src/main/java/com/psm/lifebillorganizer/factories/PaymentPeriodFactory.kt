package com.psm.lifebillorganizer.factories

import com.psm.lifebillorganizer.models.Payment
import com.psm.lifebillorganizer.models.PaymentGroup
import com.psm.lifebillorganizer.models.PaymentPeriod
import java.time.LocalDateTime
import javax.inject.Inject

class PaymentPeriodFactory @Inject constructor() {

    fun create(
        name: String, startDate: LocalDateTime, endDate: LocalDateTime,
        paymentGroup: PaymentGroup, payments: ArrayList<Payment>
    ): PaymentPeriod {

        return PaymentPeriod(name, startDate, endDate, paymentGroup, payments)
    }

}