package com.psm.lifebillorganizer.factories

import com.psm.lifebillorganizer.consts.AppConsts
import com.psm.lifebillorganizer.models.Payment
import com.psm.lifebillorganizer.models.rests.ExternalIdFilterBody
import com.psm.lifebillorganizer.models.rests.InsertPaymentInPaymentGroupBody
import com.psm.lifebillorganizer.models.rests.PushArray
import com.psm.lifebillorganizer.models.rests.PushPayment

class InsertPaymentInPaymentGroupBodyFactory {

    fun create(payment: Payment, paymentGroupExternalId: String): InsertPaymentInPaymentGroupBody {

        val pushPayment = PushArray(PushPayment(payment))

        return InsertPaymentInPaymentGroupBody(
            collection = AppConsts.MONGODB_PAYMENT_GROUP_COLLECTION,
            database = AppConsts.MONGODB_DATABASE,
            dataSource = AppConsts.MONGODB_DATASOURCE,
            update = pushPayment,
            filter = ExternalIdFilterBody(externalId = paymentGroupExternalId)
        )
    }
}