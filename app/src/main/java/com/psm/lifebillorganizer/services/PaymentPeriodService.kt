package com.psm.lifebillorganizer.services

import android.content.Context
import com.psm.lifebillorganizer.models.PaymentGroup
import com.psm.lifebillorganizer.models.PaymentPeriod

interface PaymentPeriodService {

    fun getPaymentPeriodsByGroup(context: Context, paymentGroup: PaymentGroup): ArrayList<PaymentPeriod>
}