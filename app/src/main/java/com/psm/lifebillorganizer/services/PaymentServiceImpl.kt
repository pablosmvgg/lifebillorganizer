package com.psm.lifebillorganizer.services

import android.util.Log
import com.psm.lifebillorganizer.consts.AppConsts
import com.psm.lifebillorganizer.dao.DatabaseDao
import com.psm.lifebillorganizer.factories.PayerFactory
import com.psm.lifebillorganizer.factories.PaymentFactory
import com.psm.lifebillorganizer.factories.PaymentPayerDetailFactory
import com.psm.lifebillorganizer.factories.PaymentsDetailFactory
import com.psm.lifebillorganizer.models.Payment
import com.psm.lifebillorganizer.models.PaymentGroup
import com.psm.lifebillorganizer.models.PaymentDetail
import com.psm.lifebillorganizer.models.PaymentPayerDetail
import com.psm.lifebillorganizer.models.enums.PaymentType
import com.psm.lifebillorganizer.models.rests.*
import com.psm.lifebillorganizer.rests.GroupRest
import com.psm.lifebillorganizer.rests.PaymentRest
import com.psm.lifebillorganizer.utils.FormatUtil
import com.psm.lifebillorganizer.utils.PeriodUtil
import com.psm.lifebillorganizer.utils.StringUtil
import retrofit2.Retrofit
import java.time.LocalDateTime
import javax.inject.Inject

class PaymentServiceImpl @Inject constructor(
    var paymentFactory: PaymentFactory,
    var payerFactory: PayerFactory,
    var paymentsDetailFactory: PaymentsDetailFactory,
    var paymentPayerDetailFactory: PaymentPayerDetailFactory,
    var databaseDao: DatabaseDao,
): PaymentService {

    override fun getPaymentsByGroupAndPeriod(group: PaymentGroup, paymentType: Long, dateToUse: LocalDateTime): ArrayList<Payment> {
        val firstDayPeriod = PeriodUtil.getFirstDatePeriod(group.period.id as Long,
            dateToUse)
        val lastDayPeriod = PeriodUtil.getLastDatePeriod(group.period.id as Long,
            dateToUse)
        val payments = databaseDao.getPaymentQueries()
            .getAllByPeriod(group.id!!,
                FormatUtil.formatDBDate(firstDayPeriod), FormatUtil.formatDBDate(lastDayPeriod),
                paymentType)
                .executeAsList()
        val paymentList = paymentFactory.createList(payments)

        for (payment in paymentList) {
            Log.i("PaymentService - getPaymentsByGroupAndPeriod", "Payment found: $payment")
            val payer = databaseDao.getPayerQueries().getById(payment.payerId)
                .executeAsOne()
            payment.payer = payerFactory.create(payer)
        }

        return paymentList
    }

    override fun getPaymentsByGroup(group: PaymentGroup, paymentType: Long): ArrayList<Payment> {
        val payments = databaseDao.getPaymentQueries()
            .getAll(group.id!!, paymentType)
            .executeAsList()
        val paymentList = paymentFactory.createList(payments)
        for (payment in paymentList) {
            val payer = databaseDao.getPayerQueries().getById(payment.payerId)
                .executeAsOne()
            payment.payer = payerFactory.create(payer)
        }

        return paymentList
    }

    override fun getPreviousPayments(group: PaymentGroup, paymentType: Long): ArrayList<Payment> {
        val firstDay = PeriodUtil.getFirstDatePeriod(group.periodId!!, LocalDateTime.now())
        val paymentsDB = databaseDao.getPaymentQueries()
            .getPreviousPayments(group.id!!, FormatUtil.formatDBDate(firstDay),paymentType)
            .executeAsList()

        val payments = paymentFactory.createList(paymentsDB)
        for (payment in payments) {
            val payer = databaseDao.getPayerQueries().getById(payment.payerId!!)
                .executeAsOne()
            payment.payer = payerFactory.create(payer)
        }

        return payments
    }

    override fun getUnsubmittedPayments(paymentGroupId: Long): ArrayList<Payment> {
        val paymentsDB = databaseDao.getPaymentQueries()
            .getUnsubmittedPayments(paymentGroupId = paymentGroupId)
            .executeAsList()
        return paymentFactory.createList(paymentsDB)
    }

    override fun insertPayment(name: String, description: String, value: Double, paymentDate: LocalDateTime,
                               payerId: Long, payerExternalId: String?, paymentGroupId: Long,
                               paymentTypeId: Long, paymentRecurrent: Boolean,
                               paymentRecurrentCount: Long, notifiedPayers: Long,
                               payerExternalIdModified: String?, externalId: String?,
                               isSubmitted: Boolean): Payment {

        var recurrentCountNumber: Long = 0
        if (paymentRecurrent) {
            recurrentCountNumber = 1
        }
        var finalExternalId = externalId
        if (finalExternalId == null) {
            finalExternalId = StringUtil.getRandomString(20)
        }
        var isSubmittedNumber = 0L
        if (isSubmitted) {
            isSubmittedNumber = 1L
        }
        databaseDao.getPaymentQueries().insert(
            name = name,
            description = description,
            value = value,
            createdDate = FormatUtil.formatDBDate(LocalDateTime.now()),
            paymentDate = FormatUtil.formatDBDate(paymentDate),
            payerId = payerId,
            payerExternalId = payerExternalId,
            paymentGroupId = paymentGroupId,
            paymentType = paymentTypeId,
            paymentRecurrent = recurrentCountNumber,
            paymentRecurrentCount = paymentRecurrentCount,
            externalId = finalExternalId,
            notifiedPayers = notifiedPayers,
            payerExternalIdModified = payerExternalIdModified,
            isSubmitted = isSubmittedNumber
        )
        val paymentId = databaseDao.getPaymentQueries().lastInsertRowId().executeAsOne()
        val newPayment = databaseDao.getPaymentQueries().getById(paymentId).executeAsOne()
        return paymentFactory.create(newPayment)
    }

    override fun updatePayment(payment: Payment) {

        var recurrentCountNumber: Long = 0
        if (payment.paymentRecurrent) {
            recurrentCountNumber = 1
        }
        var isSubmittedNumber = 0L
        if (payment.isSubmitted) {
            isSubmittedNumber = 1L
        }

        databaseDao.getPaymentQueries().update(
            name = payment.name,
            description = payment.description,
            value = payment.value,
            paymentDate = FormatUtil.formatDBDate(payment.paymentDate),
            payerId = payment.payerId,
            payerExternalId = payment.payerExternalId,
            paymentRecurrent = recurrentCountNumber,
            paymentRecurrentCount = payment.paymentRecurrentCount,
            notifiedPayers = payment.notifiedPayers,
            payerExternalIdModified = payment.payerExternalIdModified,
            isSubmitted = isSubmittedNumber,
            id = payment.id!!
        )

    }

    override fun deletePaymentLogically(paymentId: Long) {
        databaseDao.getPaymentQueries().deleteLogically(paymentId)
    }

    override fun deletePayment(paymentId: Long) {
        databaseDao.getPaymentQueries().delete(paymentId)
    }

    override fun deleteByPaymentGroup(paymentGroupId: Long) {
        databaseDao.getPaymentQueries().deleteByPaymentGroup(paymentGroupId)
    }

    override fun calculatePaymentsDetail(paymentGroup: PaymentGroup?, payments: ArrayList<Payment>,
                                         paymentsBalances: ArrayList<Payment>
    ):PaymentDetail {

        var total = 0.0
        var totalEachPayer = 0.0

        val paymentPayerDetails = ArrayList<PaymentPayerDetail>()
        val payerBalances = ArrayList<PaymentPayerDetail>()
        val payerDebts = ArrayList<PaymentPayerDetail>()

        //Payers pays
        if (paymentGroup!=null) {
            for (payer in paymentGroup.payers) {
                val paymentPayerDetail = paymentPayerDetailFactory
                    .create(payer, 0.0)
                paymentPayerDetails.add(paymentPayerDetail)
                val paymentPayerBalance = paymentPayerDetailFactory
                    .create(payer, 0.0)
                payerBalances.add(paymentPayerBalance)
            }

            //Payment balances
            if (paymentsBalances.any()) {
                for (paymentBalance in paymentsBalances) {
                    val paymentPayerFound = payerBalances
                        .filter { it.payer.id == paymentBalance.payerId }
                    paymentPayerFound[0]
                        .totalPaid = paymentPayerFound[0].totalPaid + paymentBalance.value
                }
            }

            //Payments
            for (payment in payments) {
                //Total paid
                total += payment.value

                //Total paid for each payer
                val paymentPayerFound = paymentPayerDetails
                    .filter { it.payer.id == payment.payerId }
                paymentPayerFound[0].totalPaid = paymentPayerFound[0].totalPaid + payment.value
            }
            totalEachPayer = total / paymentGroup.payers.size

            //Payer debts
            for (paymentPayerDetail in paymentPayerDetails) {
                val payerBalanceFound = payerBalances
                    .filter { it.payer.id == paymentPayerDetail.payer.id }
                /*val otherPayerBalancesFound = payerBalances
                    .filter { it.payer.id != paymentPayerDetail.payer.id }
                var totalOtherBalance = 0.0
                for (otherPayerBalanceFound in otherPayerBalancesFound) {
                    totalOtherBalance += otherPayerBalanceFound.totalPaid
                }*/
                val payerDebt = paymentPayerDetailFactory
                    .create(paymentPayerDetail.payer, 0.0)
                val difference = totalEachPayer /*+ totalOtherBalance*/ - paymentPayerDetail.totalPaid - payerBalanceFound[0].totalPaid
                if (difference > 0) {
                    payerDebt.totalPaid = difference
                }
                payerDebts.add(payerDebt)
            }

        }

        return paymentsDetailFactory.create(total, totalEachPayer, paymentPayerDetails,
            payerBalances, payerDebts)
    }

    override fun savePreviousRecurrentPayments(group: PaymentGroup, paymentType: PaymentType) {
        val today = LocalDateTime.now()
        val firstDayMinusOne = PeriodUtil.getFirstDatePeriod(group.periodId, today)!!
            .minusDays(1)
        val firstPreviousPeriodDay = PeriodUtil.getFirstDatePeriod(group.periodId, firstDayMinusOne)
        val lastPreviousPeriodDay = PeriodUtil.getLastDatePeriod(group.periodId, firstDayMinusOne)
        val previousRecurrentPaymentsDB = databaseDao.getPaymentQueries()
            .getPreviousRecurrentPayments(group.id!!, paymentType.paymentTypeId,
                FormatUtil.formatDBDate(firstPreviousPeriodDay),
                FormatUtil.formatDBDate(lastPreviousPeriodDay)).executeAsList()
        val previousRecurrentPayments = paymentFactory.createList(previousRecurrentPaymentsDB)
        for(previousRecurrentPayment in previousRecurrentPayments) {
            if (previousRecurrentPayment.paymentRecurrentCount == 0L ||
                previousRecurrentPayment.paymentRecurrentCount >= 2L) {
                if (previousRecurrentPayment.paymentRecurrentCount >= 2L) {
                    previousRecurrentPayment.paymentRecurrentCount--
                }
                insertPayment(previousRecurrentPayment.name,
                    previousRecurrentPayment.description!!, previousRecurrentPayment.value,
                    today, previousRecurrentPayment.payerId, previousRecurrentPayment.payerExternalId,
                    previousRecurrentPayment.paymentGroupId, previousRecurrentPayment.paymentType,
                    previousRecurrentPayment.paymentRecurrent,
                    previousRecurrentPayment.paymentRecurrentCount,
                    previousRecurrentPayment.notifiedPayers!!,
                    previousRecurrentPayment.payerExternalIdModified,
                    previousRecurrentPayment.externalId,
                    previousRecurrentPayment.isSubmitted)
            }
        }
    }
}