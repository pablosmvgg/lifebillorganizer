package com.psm.lifebillorganizer.services

import com.psm.lifebillorganizer.models.Period

interface PeriodService {
    fun getPeriodById(periodId: Long): Period
    fun getPeriods(): ArrayList<Period>
}