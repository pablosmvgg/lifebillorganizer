package com.psm.lifebillorganizer.services

import android.content.Context
import com.psm.lifebillorganizer.R
import com.psm.lifebillorganizer.factories.PaymentPeriodFactory
import com.psm.lifebillorganizer.models.Payment
import com.psm.lifebillorganizer.models.PaymentGroup
import com.psm.lifebillorganizer.models.PaymentPeriod
import com.psm.lifebillorganizer.models.enums.PaymentType
import com.psm.lifebillorganizer.utils.FormatUtil
import com.psm.lifebillorganizer.utils.PeriodUtil
import javax.inject.Inject

class PaymentPeriodServiceImpl @Inject constructor(var paymentGroupService: PaymentGroupService,
                                                   var paymentService: PaymentService,
                                                   var paymentPeriodFactory: PaymentPeriodFactory
)
    : PaymentPeriodService {



    override fun getPaymentPeriodsByGroup(context: Context, paymentGroup: PaymentGroup): ArrayList<PaymentPeriod> {

        val paymentPeriods = ArrayList<PaymentPeriod>()
        val payments = paymentService.getPreviousPayments(paymentGroup,
            PaymentType.COMMON.paymentTypeId)

        for (payment in payments) {

            val periodFound = paymentPeriods
                .filter { (it.startDate == payment.paymentDate
                            || it.startDate.isBefore(payment.paymentDate))
                        && (it.endDate == payment.paymentDate
                            || it.endDate.isAfter(payment.paymentDate))}

            if (periodFound.isNotEmpty()) {
                periodFound[0].payments.add(payment)
            }
            else {
                val newPayments = ArrayList<Payment>()
                newPayments.add(payment)
                val newStartDate = PeriodUtil.getFirstDatePeriod(paymentGroup.periodId,
                    payment.paymentDate)
                val newEndDate = PeriodUtil.getLastDatePeriod(paymentGroup.periodId,
                    payment.paymentDate)
                val name = StringBuilder()
                name.append(context.getString(R.string.label_period)).append(" ")
                name.append(FormatUtil.formatDate(newStartDate)).append(" ")
                name.append(context.getString(R.string.label_period_payment_to)).append(" ")
                name.append(FormatUtil.formatDate(newEndDate))
                val newPaymentPeriod = paymentPeriodFactory.create(
                    name = name.toString(),
                    startDate = newStartDate!!,
                    endDate = newEndDate!!,
                    paymentGroup = paymentGroup,
                    payments = newPayments
                )
                paymentPeriods.add(newPaymentPeriod)
            }
        }

        return paymentPeriods
    }

}