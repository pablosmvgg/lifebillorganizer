package com.psm.lifebillorganizer.services

import com.psm.lifebillorganizer.models.Payer
import com.psm.lifebillorganizer.models.Payment
import com.psm.lifebillorganizer.models.PaymentGroup

interface PaymentGroupService {
    fun getGroup(groupId: Long): PaymentGroup?
    fun getGroups(): ArrayList<PaymentGroup>
    fun getGroupsByPayer(payerId: Long): ArrayList<PaymentGroup>
    fun getSelectedGroup(): PaymentGroup?
    fun getGroup(externalId: String): PaymentGroup?
    fun selectPaymentGroup(paymentGroupId: Long)
    fun unselectPaymentGroups()
    fun insertPaymentGroup(externalId: String?, name: String, description: String, periodId: Long,
                           payers: ArrayList<Payer>, onlinePayersJoined: Long): PaymentGroup
    fun deleteGroup(paymentGroupId: Long): PaymentGroup?
    fun deleteGroupsByPayer(payerId: Long): PaymentGroup?
    fun updatePaymentGroup(paymentGroup: PaymentGroup)

    fun setPayerGroupExternalId(payerGroupExternalId: String, payerId: Long, groupId: Long)
    fun getPayerGroupExternalId(payerId: Long, groupId: Long): String?
}