package com.psm.lifebillorganizer.services

import com.psm.lifebillorganizer.models.Payment
import com.psm.lifebillorganizer.models.PaymentGroup
import com.psm.lifebillorganizer.models.PaymentDetail
import com.psm.lifebillorganizer.models.enums.PaymentType
import java.time.LocalDateTime

interface PaymentService {
    fun getPaymentsByGroupAndPeriod(group: PaymentGroup, paymentType: Long, dateToUse: LocalDateTime): ArrayList<Payment>
    fun getPaymentsByGroup(group: PaymentGroup, paymentType: Long): ArrayList<Payment>
    fun getPreviousPayments(group: PaymentGroup, paymentType: Long): ArrayList<Payment>
    fun getUnsubmittedPayments(paymentGroupId: Long): ArrayList<Payment>
    fun insertPayment(name: String, description: String, value: Double, paymentDate: LocalDateTime,
                      payerId: Long, payerExternalId: String?, paymentGroupId: Long, paymentTypeId: Long,
                      paymentRecurrent: Boolean, paymentRecurrentCount: Long, notifiedPayers: Long,
                      payerExternalIdModified: String?, externalId: String?, isSubmitted: Boolean): Payment
    fun updatePayment(payment: Payment)
    fun deletePaymentLogically(paymentId: Long)
    fun deletePayment(paymentId: Long)
    fun deleteByPaymentGroup(paymentGroupId: Long)

    fun calculatePaymentsDetail(paymentGroup: PaymentGroup?, payments: ArrayList<Payment>,
                                paymentsBalances: ArrayList<Payment> = arrayListOf()): PaymentDetail

    fun savePreviousRecurrentPayments(group: PaymentGroup, paymentType: PaymentType)
}