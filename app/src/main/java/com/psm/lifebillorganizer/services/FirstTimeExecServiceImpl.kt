package com.psm.lifebillorganizer.services

import com.psm.lifebillorganizer.dao.DatabaseDao
import com.psm.lifebillorganizer.factories.FirstTimeExecFactory
import com.psm.lifebillorganizer.factories.PeriodFactory
import com.psm.lifebillorganizer.models.FirstTimeExec
import javax.inject.Inject

class FirstTimeExecServiceImpl @Inject constructor(var firstTimeExecFactory: FirstTimeExecFactory,
                                                   var databaseDao: DatabaseDao
): FirstTimeExecService {

    override fun getFirstTimeExec(): FirstTimeExec {
        val element = databaseDao.getFirstTimeExecQueries().getAll().executeAsOne()
        return firstTimeExecFactory.create(element)
    }

    override fun updateFirstTimeExec(executed: Boolean) {
        var executedNumber:Long = 0
        if (executed) {
            executedNumber = 1
        }
        databaseDao.getFirstTimeExecQueries().update(executedNumber)
    }
}