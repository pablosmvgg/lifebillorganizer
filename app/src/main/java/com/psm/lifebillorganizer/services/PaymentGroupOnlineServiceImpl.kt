package com.psm.lifebillorganizer.services

import android.util.Log
import com.google.gson.Gson
import com.psm.lifebillorganizer.consts.AppConsts
import com.psm.lifebillorganizer.dao.DatabaseDao
import com.psm.lifebillorganizer.factories.*
import com.psm.lifebillorganizer.models.Payment
import com.psm.lifebillorganizer.models.PaymentGroup
import com.psm.lifebillorganizer.models.rests.*
import com.psm.lifebillorganizer.rests.GroupRest
import com.psm.lifebillorganizer.rests.PayerRest
import com.psm.lifebillorganizer.rests.PaymentRest
import com.psm.lifebillorganizer.utils.FormatUtil
import com.psm.lifebillorganizer.utils.StringUtil
import retrofit2.Retrofit
import java.time.LocalDateTime
import javax.inject.Inject


class PaymentGroupOnlineServiceImpl @Inject constructor(var paymentFactory: PaymentFactory,
                                                        var paymentRemoteUpdatedBodyFactory: PaymentRemoteUpdatedBodyFactory,
                                                        var payerRemoteUpdatedBodyFactory: PayerRemoteUpdatedBodyFactory,
                                                        var insertPaymentInPaymentGroupBodyFactory: InsertPaymentInPaymentGroupBodyFactory,
                                                        var updatePaymentInPaymentGroupBodyFactory: UpdatePaymentInPaymentGroupBodyFactory,
                                                        var paymentGroupService: PaymentGroupService,
                                                        var paymentService: PaymentService,
                                                        var payerService: PayerService,
                                                        var databaseDao: DatabaseDao,
                                                        var retrofit: Retrofit):
    PaymentGroupOnlineService {

    override suspend fun getOnlineGroup(groupExtId: String): PaymentGroup? {
        val body = PaymentGroupBody(collection = AppConsts.MONGODB_PAYMENT_GROUP_COLLECTION,
            database = AppConsts.MONGODB_DATABASE,
            dataSource = AppConsts.MONGODB_DATASOURCE,
            filter = GetByExternalIdFilter(groupExtId)
        )
        val call = retrofit.create(GroupRest::class.java).getPaymentGroupById(body)
        if(call.isSuccessful){
            return call.body()?.document
        }
        //show error
        return null
    }

    override suspend fun insertOnlineGroup(paymentGroup: PaymentGroup): PaymentGroup? {

        Log.i("PaymentGroupOnlineService - insertOnlineGroup", "INIT")
        //Save data to the payers to make them online
        paymentGroup.payers.forEach { payer ->
            if (payer.isMainPayer) {
                payer.onlineUsed = true
                payerService.setOnlinePayer(payer)
            }
            val payerGroupExternalId = generatePayerGroupExternalId()
            paymentGroupService.setPayerGroupExternalId(
                payerGroupExternalId = payerGroupExternalId,
                payerId = payer.id!!,
                groupId = paymentGroup.id!!
            )
            payer.payerGroupExternalId = payerGroupExternalId
        }

        //Get the payments of the group if it has
        val mainPayer = payerService.getMainPayer(paymentGroup.id)
        paymentGroup.payments = paymentService.getPaymentsByGroup(paymentGroup, paymentGroup.periodId)
        paymentGroup.payments.forEach {payment ->
            paymentGroup.payers.filter { payer -> payer.id == payment.payerId }
                .forEach { payer ->
                    payment.payerExternalId = payer.payerGroupExternalId
                    payment.payerExternalIdModified = mainPayer.payerGroupExternalId
                }
            Log.i("PaymentGroupOnlineService - insertOnlineGroup", "Payment Updated: $payment")
        }

        paymentGroup.externalId = generateExternalId(paymentGroup)
        paymentGroup.onlinePayersJoined = 1

        val body = InsertPaymentGroupBody(collection = AppConsts.MONGODB_PAYMENT_GROUP_COLLECTION,
            database = AppConsts.MONGODB_DATABASE,
            dataSource = AppConsts.MONGODB_DATASOURCE,
            document = paymentGroup)
        val gson = Gson()
        val bodyString = gson.toJson(body)
        Log.i("PaymentGroupOnlineService - insertOnlineGroup", "Body Generated: $bodyString")
        val call = retrofit.create(GroupRest::class.java).insertPaymentGroup(body)
        Log.i("PaymentGroupOnlineService - insertOnlineGroup",
            "Group inserted?: ${call.isSuccessful}")
        if(call.isSuccessful){
            paymentGroupService.updatePaymentGroup(paymentGroup)
            val mainPayer = payerService.getMainPayer(paymentGroup.id)
            payerService.setOnlinePayer(mainPayer)
            Log.i("PaymentGroupOnlineService - insertOnlineGroup", "END")
            paymentGroup.payments.forEach{ paymentToSave ->
                paymentService.updatePayment(paymentToSave)
            }
            return paymentGroup
        }
        else {
            Log.i("PaymentGroupOnlineService - insertOnlineGroup",
                "Error: " + call.raw().toString())
        }
        Log.i("PaymentGroupOnlineService - insertOnlineGroup", "END with error.")
        //show error
        return null
    }

    override suspend fun insertOnlineGroupLocally(paymentGroup: PaymentGroup): PaymentGroup? {
        //TODO BUG: Si me joineo a un grupo, lo borro y creo otro con los mismos integrantes y lo hago online
        //Al joinearte con el segundo aparece online el integrante y no deja seleccionarlo.
        // Fix, sacarle el online a/los integrantes que formaban el grupo borrado.

        Log.i("PaymentGroupOnlineService - insertOnlineGroupLocally", "INIT")

        //put the data from main payer to the selected one
        val mainPayer = payerService.getMainPayer(paymentGroup.id)
        paymentGroup.payers.stream()
            .filter { payer -> payer.selected && !payer.onlineUsed }
            .forEach { payer ->
                run {
                    //set the local values
                    payer.externalId = mainPayer.externalId
                }
            }
        //increase the amount of payers joined
        paymentGroup.onlinePayersJoined += 1
        
        //Associate or add the other payers to the local table.
        //TODO: al enviar los cambios se modifica la created date del payer que no es main
        val nonMainPayers = paymentGroup.payers.filter {!it.isMainPayer}
        nonMainPayers.forEach { payer ->
            val payerFound = databaseDao.getPayerQueries().getByName(payer.name).executeAsOneOrNull()
            if (payerFound==null) {
                databaseDao.getPayerQueries().insert(
                    name = payer.name,
                    description = payer.description,
                    createdDate = FormatUtil.formatDBDate(LocalDateTime.now()),
                    externalId = payer.externalId
                )
                val payerId = databaseDao.getPaymentGroupQueries().lastInsertRowId()
                    .executeAsOne()
                databaseDao.getPayerQueries().setPayerOnlineUsed(onlineUsed = 1, id = payerId)
                val payerCreated = databaseDao.getPayerQueries()
                    .getById(payerId).executeAsOne()
                paymentGroup.payers.stream()
                    .filter { it.name == payerCreated.name }
                    .forEach {
                        run {
                            it.id = payerCreated.id
                        }
                    }
            }
            else {
                databaseDao.getPayerQueries().update(
                    name = payer.name,
                    description = payer.description,
                    editedDate = FormatUtil.formatDBDate(LocalDateTime.now()),
                    deletedDate = null,
                    externalId = payer.externalId,
                    id = payerFound.id
                )
                databaseDao.getPayerQueries().setPayerOnlineUsed(onlineUsed = 1, id = payerFound.id)
                paymentGroup.payers.stream()
                    .filter { it.name == payerFound.name }
                    .forEach {
                        run {
                            it.id = payerFound.id
                        }
                    }

            }
        }

        val paymentGroupCreated = paymentGroupService.insertPaymentGroup(paymentGroup.externalId,
            paymentGroup.name, paymentGroup.description!!, paymentGroup.periodId,
            paymentGroup.payers, paymentGroup.onlinePayersJoined)
        paymentGroupService.selectPaymentGroup(paymentGroupCreated.id!!)

        paymentGroup.payers.forEach { onlinePayer ->
            Log.i("PaymentGroupOnlineServiceImpl - insertOnlineGroupLocally",
                "payerGroupExternalId: ${onlinePayer.payerGroupExternalId} payerId: " +
                        "${onlinePayer.id} paymentGroupId: ${paymentGroupCreated.id}")
            paymentGroupService.setPayerGroupExternalId(onlinePayer.payerGroupExternalId!!,
                onlinePayer.id!!, paymentGroupCreated.id!!)
            paymentGroupCreated.payers.filter { payerGC ->payerGC.externalId == onlinePayer.externalId }
                .forEach { payerGC ->
                    payerGC.payerGroupExternalId = onlinePayer.payerGroupExternalId
                }
        }

        //Insert Payments
        paymentGroup.payments.forEach{ payment ->
            paymentGroupCreated.payers.filter {
                it.payerGroupExternalId == payment.payerExternalId
            }
            .forEach{ payerIt ->
                val payerToUse = payerService
                    .getPayerByPayerGroupExternalId(payerIt.payerGroupExternalId!!)
                payment.payerId = payerToUse.id!!
            }

            payment.notifiedPayers = payment.notifiedPayers?.plus(1)
            payment.paymentGroupId = paymentGroupCreated.id!!

            Log.i("PaymentGroupOnlineService - insertOnlineGroupLocally",
                "PaymentGroupOnlineService Payment To SAVE: $payment")

            paymentService.insertPayment(payment.name, payment.description!!, payment.value,
                payment.paymentDate, payment.payerId, payment.payerExternalId,
                payment.paymentGroupId, payment.paymentType, payment.paymentRecurrent,
                payment.paymentRecurrentCount, payment.notifiedPayers!!,
                payment.payerExternalIdModified, payment.externalId, payment.isSubmitted)
        }

        paymentGroupCreated.payments = paymentGroup.payments
        paymentGroupCreated.createdDate = paymentGroup.createdDate

        if (updateOnlineGroup(paymentGroupCreated)) {
            Log.i("PaymentGroupOnlineService - insertOnlineGroupLocally", "END")
            return paymentGroupCreated
        }
        Log.i("PaymentGroupOnlineService - insertOnlineGroupLocally", "null END")
        return null
    }

    override suspend fun deleteOnlinePaymentGroup(paymentGroup: PaymentGroup) {

        if (paymentGroup.onlinePayersJoined == 1L) {
            //If it's the last member of the group online then delete it in Mongo
            val filter = IdFilterBody(paymentGroup.externalId!!)
            val body = DeletePaymentGroupBody(collection = AppConsts.MONGODB_PAYMENT_GROUP_COLLECTION,
                database = AppConsts.MONGODB_DATABASE,
                dataSource = AppConsts.MONGODB_DATASOURCE,
                filter = filter)
            val call = retrofit.create(GroupRest::class.java).deletePaymentGroup(body)
            if(!call.isSuccessful){
                //show error
                //TODO: Ver como manejar el error
            }
        }
        else if (paymentGroup.onlinePayersJoined > 1L) {
            paymentGroup.onlinePayersJoined--
            databaseDao.getPaymentGroupQueries()
                .setOnlinePayersJoined(onlinePayersJoined = paymentGroup.onlinePayersJoined,
                    externalId = paymentGroup.externalId)
        }

    }

    override suspend fun updateOnlineGroup(paymentGroup: PaymentGroup): Boolean {
        paymentGroup.payers.forEach { payer ->
            val payerGroupExternalId = paymentGroupService.getPayerGroupExternalId(payer.id!!,
                paymentGroup.id!!)
            paymentGroupService.setPayerGroupExternalId(payerGroupExternalId!!,
                payer.id!!, paymentGroup.id!!)
        }
        Log.i("PaymentGroupOnlineService - updateOnlineGroup", "INIT")
        val body = UpdatePaymentGroupBody(collection = AppConsts.MONGODB_PAYMENT_GROUP_COLLECTION,
            database = AppConsts.MONGODB_DATABASE,
            dataSource = AppConsts.MONGODB_DATASOURCE,
            update = paymentGroup,
            filter = ExternalIdFilterBody(externalId = paymentGroup.externalId!!)
        )
        val gson = Gson()
        val bodyString = gson.toJson(body)
        Log.i("PaymentGroupOnlineService - updateOnlineGroup", "Body to send: $bodyString")
        val call = retrofit.create(GroupRest::class.java).updatePaymentGroup(body)
        Log.i("PaymentGroupOnlineService - updateOnlineGroup", "Group updated?: ${call.isSuccessful}")
        Log.i("PaymentGroupOnlineService - updateOnlineGroup", "Group updated?: ${call.raw()}")
        if (!call.isSuccessful) {
            Log.i("PaymentGroupOnlineService - updateOnlineGroup",
                "MongoDB - REST API UPDATE ERROR: ${call.errorBody()?.string()!!}")
        }

        Log.i("PaymentGroupOnlineService - updateOnlineGroup", "END")
        return call.isSuccessful
    }

    //Payments
    override suspend fun getOnlinePaymentsByGroupAndPeriod(group: PaymentGroup, paymentType: Long,
                                                           dateToUse: LocalDateTime):
            ArrayList<Payment> {

        var paymentsFound: ArrayList<Payment> = ArrayList()
        var amountPaymentsChanged = 0
        try {
            val mainPayer = payerService.getMainPayer(group.id)
            Log.i("PaymentGroupOnlineService - getOnlinePaymentsByGroupAndPeriod", "INIT")
            val paymentsChanged: ArrayList<Payment>
            val body = paymentRemoteUpdatedBodyFactory.createByNotified(group, mainPayer)
            val gson = Gson()
            val bodyString = gson.toJson(body)
            Log.i("PaymentGroupOnlineService - getOnlinePaymentsByGroupAndPeriod",
                "Body generated: $bodyString")
            val call = retrofit.create(PaymentRest::class.java).getPaymentByNotifiedPayers(body)
            if(call.isSuccessful){
                Log.i("PaymentGroupOnlineService - getOnlinePaymentsByGroupAndPeriod",
                    "Data Returned: ${call.body().toString()}")
                val response = call.body()!!
                paymentsChanged = response.documents
                if (paymentsChanged.isNotEmpty()) {
                    //Save the changes from online database

                    for(paymentChanged in paymentsChanged) {
                        val localPayment =  databaseDao.getPaymentQueries().getByExternalId(paymentChanged.externalId!!)
                            .executeAsOneOrNull()
                        //New payment
                        if (localPayment == null && !paymentChanged.deleted) {
                            val notified = paymentChanged.notifiedPayers!! + 1
                            val payerUsed = payerService.getPayerByExternalId(paymentChanged.payerExternalId!!, group.id)
                            paymentService.insertPayment(name = paymentChanged.name,
                                description = paymentChanged.description!!,
                                value = paymentChanged.value, paymentDate = paymentChanged.paymentDate,
                                payerId = payerUsed.id!!, payerExternalId = paymentChanged.payerExternalId,
                                paymentGroupId = group.id!!, paymentTypeId = paymentChanged.paymentType,
                                paymentRecurrent = paymentChanged.paymentRecurrent,
                                paymentRecurrentCount = paymentChanged.paymentRecurrentCount,
                                notifiedPayers = notified,
                                payerExternalIdModified = paymentChanged.payerExternalIdModified,
                                externalId = paymentChanged.externalId,
                                isSubmitted = paymentChanged.isSubmitted)

                            amountPaymentsChanged +=1
                        }
                        else {
                            //Update existing payment
                            if (!paymentChanged.deleted) {
                                if (paymentChanged.payerExternalIdModified!! != mainPayer.externalId) {
                                    paymentChanged.notifiedPayers = paymentChanged.notifiedPayers!! + 1
                                    paymentChanged.id = localPayment!!.id
                                    paymentChanged.payerId = localPayment.payerId
                                    paymentChanged.paymentGroupId = localPayment.paymentGroupId

                                    paymentService.updatePayment(paymentChanged)

                                    amountPaymentsChanged +=1
                                }
                            }
                            //Delete existing payment
                            else {
                                paymentService.deletePayment(localPayment!!.id)
                                amountPaymentsChanged +=1
                            }
                        }
                    }
                }
            }
            else {
                Log.e("PaymentGroupOnlineService - getOnlinePaymentsByGroupAndPeriod",
                    "getOnlinePaymentsByGroupAndPeriod error: ${call.raw()}")
                //TODO: mandar un mensaje de error indicando que no se pudo actualizar con la nube
            }
            paymentsFound = paymentService.getPaymentsByGroupAndPeriod(group, paymentType, dateToUse)
            if (amountPaymentsChanged > 0) {
                group.payments = paymentsFound
                updateOnlineGroup(group)
            }
        }
        catch (ex: Exception) {
            Log.e("PaymentGroupOnlineService - getOnlinePaymentsByGroupAndPeriod",
                "Error: " + ex.stackTraceToString())
        }

        Log.i("PaymentGroupOnlineService - getOnlinePaymentsByGroupAndPeriod", "END")
        return paymentsFound
    }

    override suspend fun getUpdateOnlinePayersByGroup(group: PaymentGroup): Boolean {
        var response = true
        var amountPaymentsChanged = 0
        try {
            Log.i("PaymentGroupOnlineService", "getUpdateOnlinePayersByGroup init")
            val mainPayer = payerService.getMainPayer(group.id)
            val body = payerRemoteUpdatedBodyFactory.create(group, mainPayer)
            val gson = Gson()
            val bodyString = gson.toJson(body)
            Log.i("PaymentGroupOnlineService - getUpdateOnlinePayersByGroup",
                "Body generated: $bodyString")
            val call = retrofit.create(PayerRest::class.java).getOtherPayersFromGroup(body)
            if(call.isSuccessful) {
                val payersFound = call.body()!!.documents
                Log.i("PaymentGroupOnlineService - getUpdateOnlinePayersByGroup",
                    "Payers returned: $payersFound")
                group.payers.forEach { payer ->
                    payersFound
                        .filter { payerFound -> payerFound.externalId == payer.externalId }
                        .forEach { payerIt ->
                            if (payerIt.onlineUsed && !payer.onlineUsed) {
                                payerService.setOnlinePayer(payer)
                                group.onlinePayersJoined += 1
                                amountPaymentsChanged += 1
                            }
                        }
                }
                if (amountPaymentsChanged > 0) {
                    updateOnlineGroup(group)
                }
            }
            else {
                Log.e("PaymentGroupOnlineService", "getUpdateOnlinePayersByGroup error: "
                        + call.raw().toString())
                //TODO: mandar un mensaje de error indicando que no se pudo traer de la nube
            }

        }
        catch (ex: Exception) {
            Log.e("PaymentGroupOnlineService getUpdateOnlinePayersByGroup",
                "Error: " + ex.stackTraceToString())
            response = false
        }
        Log.i("PaymentGroupOnlineService", "getUpdateOnlinePayersByGroup end")
        return response
    }

    //https://www.mongodb.com/docs/atlas/app-services/functions/mongodb/write/
    override suspend fun insertPaymentOnline(payment: Payment, paymentGroupExternalId: String,
                                             paymentGroupId: Long): Boolean {

        Log.i("PaymentGroupOnlineService - insertPaymentOnline", "INIT")

        val mainPayer = payerService.getMainPayer(paymentGroupId)
        payment.notifiedPayers = 1
        payment.payerExternalIdModified = mainPayer.payerGroupExternalId
        val body = insertPaymentInPaymentGroupBodyFactory.create(payment, paymentGroupExternalId)
        val gson = Gson()
        val bodyString = gson.toJson(body)
        Log.i("PaymentGroupOnlineService - insertPaymentOnline", "Body to send: $bodyString")
        val call = retrofit.create(PaymentRest::class.java).insertPaymentInPaymentGroup(body)
        Log.i("PaymentGroupOnlineService - insertPaymentOnline", "Payment added?: ${call.isSuccessful}")
        Log.i("PaymentGroupOnlineService - insertPaymentOnline", "Payment added?: ${call.raw()}")
        if (!call.isSuccessful) {
            Log.i("PaymentGroupOnlineService - insertPaymentOnline",
                "MongoDB - REST API UPDATE ERROR: ${call.errorBody()?.string()!!}")
        }

        Log.i("PaymentGroupOnlineService - insertPaymentOnline", "END")
        return call.isSuccessful
    }

    override suspend fun updatePaymentOnline(payment: Payment, paymentGroupExternalId: String,
                                             paymentGroupId: Long): Boolean {

        Log.i("PaymentGroupOnlineService - updatePaymentOnline", "INIT")
        val mainPayer = payerService.getMainPayer(paymentGroupId)
        val body = updatePaymentInPaymentGroupBodyFactory.create(payment, mainPayer.externalId!!,
            paymentGroupExternalId)
        val gson = Gson()
        val bodyString = gson.toJson(body)
        Log.i("PaymentGroupOnlineService - updatePaymentOnline", "Body to send: $bodyString")
        val call = retrofit.create(PaymentRest::class.java).updatePaymentInPaymentGroup(body)
        Log.i("PaymentGroupOnlineService - updatePaymentOnline", "Payment updated?: ${call.isSuccessful}")
        Log.i("PaymentGroupOnlineService - updatePaymentOnline", "Payment updated?: ${call.raw()}")
        if (!call.isSuccessful) {
            Log.i("PaymentGroupOnlineService - updatePaymentOnline",
                "MongoDB - REST API UPDATE ERROR: ${call.errorBody()?.string()!!}")
        }

        Log.i("PaymentGroupOnlineService - updatePaymentOnline", "END")
        return call.isSuccessful
    }

    override suspend fun processUnSubmittedPayments(paymentGroup: PaymentGroup) {
        val unsubmittedPayments = paymentService.getUnsubmittedPayments(paymentGroup.id!!)
        unsubmittedPayments.forEach { usPayment ->
            //get the payment online to see if I need to insert or update.
            val body = paymentRemoteUpdatedBodyFactory.createByExternalId(paymentGroup, usPayment)
            val gson = Gson()
            val bodyString = gson.toJson(body)
            Log.i("PaymentGroupOnlineService - processUnSubmittedPayments",
                "Body generated: $bodyString")
            val call = retrofit.create(PaymentRest::class.java).getPaymentByExternalId(body)
            if(call.isSuccessful) {
                //Insert or update online the payment.
                Log.i("PaymentGroupOnlineService - processUnSubmittedPayments",
                    "Data Returned: ${call.body().toString()}")
                val response = call.body()!!
                var responseCall = false
                if (response.documents.isEmpty()) {
                    if (!usPayment.deleted) {
                        //Insert online the payment
                        responseCall = insertPaymentOnline(usPayment, paymentGroup.externalId!!,
                            paymentGroup.id!!)
                    }
                }
                else {
                    //update online the payment
                    responseCall = updatePaymentOnline(usPayment, paymentGroup.externalId!!,
                        paymentGroup.id!!)
                }
                if (responseCall) {
                    //Update or delete locally the payment to confirm that it's uploaded
                    if (usPayment.deleted) {
                        paymentService.deletePayment(usPayment.id!!)
                    }
                    else {
                        usPayment.isSubmitted = true
                        paymentService.updatePayment(usPayment)
                    }
                }
            }
        }
    }


    private fun generateExternalId(paymentGroup: PaymentGroup): String {
        return "${paymentGroup.payers.size}${StringUtil.getRandomString(6)}"
    }

    private fun generatePayerGroupExternalId(): String {
        return StringUtil.getRandomString(8)
    }

}