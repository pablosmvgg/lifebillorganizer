package com.psm.lifebillorganizer.services

import com.psm.lifebillorganizer.dao.DatabaseDao
import com.psm.lifebillorganizer.factories.PayerFactory
import com.psm.lifebillorganizer.factories.PeriodFactory
import com.psm.lifebillorganizer.models.Period
import javax.inject.Inject

class PeriodServiceImpl @Inject constructor(var periodFactory: PeriodFactory,
                                           var databaseDao: DatabaseDao): PeriodService {

    override fun getPeriodById(periodId: Long): Period {
        val periodDB = databaseDao.getPeriodQueries()
            .getById(periodId).executeAsOne()

        return periodFactory.create(periodDB)
    }

    override fun getPeriods(): ArrayList<Period> {
        var periodsDB = databaseDao.getPeriodQueries().getAll().executeAsList()
        return periodFactory.createList(periodsDB)
    }

}