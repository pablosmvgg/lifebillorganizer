package com.psm.lifebillorganizer.services

import com.psm.lifebillorganizer.models.Payment
import com.psm.lifebillorganizer.models.PaymentGroup
import java.time.LocalDateTime

interface PaymentGroupOnlineService {
    suspend fun getOnlineGroup(groupExtId: String): PaymentGroup?
    suspend fun insertOnlineGroup(paymentGroup: PaymentGroup): PaymentGroup?
    suspend fun insertOnlineGroupLocally(paymentGroup: PaymentGroup): PaymentGroup?
    suspend fun deleteOnlinePaymentGroup(paymentGroup: PaymentGroup)
    suspend fun updateOnlineGroup(paymentGroup: PaymentGroup): Boolean
    suspend fun getOnlinePaymentsByGroupAndPeriod(group: PaymentGroup, paymentType: Long,
                                                  dateToUse: LocalDateTime): ArrayList<Payment>
    suspend fun getUpdateOnlinePayersByGroup(group: PaymentGroup): Boolean
    suspend fun insertPaymentOnline(payment: Payment, paymentGroupExternalId: String,
                                    paymentGroupId: Long): Boolean
    suspend fun updatePaymentOnline(payment: Payment, paymentGroupExternalId: String,
                                    paymentGroupId: Long): Boolean
    suspend fun processUnSubmittedPayments(paymentGroup: PaymentGroup)
}