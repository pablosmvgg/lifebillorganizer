package com.psm.lifebillorganizer.services

import com.psm.lifebillorganizer.dao.DatabaseDao
import com.psm.lifebillorganizer.factories.PayerFactory
import com.psm.lifebillorganizer.models.Payer
import com.psm.lifebillorganizer.models.PaymentGroup
import com.psm.lifebillorganizer.utils.FormatUtil
import com.psm.lifebillorganizer.utils.StringUtil
import java.time.LocalDateTime
import javax.inject.Inject

class PayerServiceImpl @Inject constructor(var payerFactory: PayerFactory,
                                           var databaseDao: DatabaseDao,
                                           var paymentGroupService: PaymentGroupService)
    : PayerService {


    override fun getGroupPayers(groupId: Long): ArrayList<Payer> {
        val payersDB = databaseDao.getPaymentGroupQueries()
            .getGroupPayers(groupId).executeAsList()
        val payers = payerFactory.createList(payersDB)
        payers.forEach { currPayer ->
            setPayerGroupExternalId(currPayer, groupId)
        }
        return payers
    }

    override fun getPayers(groupId: Long?): ArrayList<Payer> {
        val payersDB = databaseDao.getPayerQueries().getAll().executeAsList()
        val payers = payerFactory.createList(payersDB)
        if (groupId != null) {
            payers.forEach { currPayer ->
                setPayerGroupExternalId(currPayer, groupId)
            }
        }
        return payers
    }

    override fun getPayerByName(name: String, groupId: Long?): Payer? {
        val payerDB = databaseDao.getPayerQueries().getByName(name).executeAsOneOrNull()
        if (payerDB!=null) {
            val payer = payerFactory.create(payerDB)
            if (groupId != null) {
                setPayerGroupExternalId(payer, groupId)
            }
            return payer
        }
        return null
    }

    override fun getPayerByExternalId(externalId: String, groupId: Long?): Payer {
        val payerDB = databaseDao.getPayerQueries().getByExternalId(externalId)
            .executeAsOne()
        return payerFactory.create(payerDB)
    }

    override fun getPayerByPayerGroupExternalId(payerGroupExternalId: String): Payer {
        val payerDB = databaseDao.getPayerQueries().getByPayerGroupExternalId(payerGroupExternalId)
            .executeAsOne()
        return payerFactory.create(payerDB)
    }

    override fun insertPayer(name: String, description: String):Payer {
        databaseDao.getPayerQueries().insert(
            name = name,
            description = description,
            createdDate = FormatUtil.formatDBDate(LocalDateTime.now()),
            externalId = StringUtil.getRandomString(10)
        )
        val payerId = databaseDao.getPaymentGroupQueries().lastInsertRowId()
            .executeAsOne()
        val payerCreated = databaseDao.getPayerQueries().getById(payerId).executeAsOne()
        return payerFactory.create(payerCreated)
    }

    override fun updatePayer(payer: Payer) {
        var deletedDate: String? = null
        if (payer.deletedDate!=null) {
            deletedDate = FormatUtil.formatDBDate(payer.deletedDate)
        }

        databaseDao.getPayerQueries().update(
            name = payer.name,
            description = payer.description,
            editedDate = FormatUtil.formatDBDate(LocalDateTime.now()),
            deletedDate = deletedDate,
            id = payer.id!!,
            externalId = payer.externalId
        )
    }

    override fun getMainPayer(groupId: Long?): Payer {
        val payerDB = databaseDao.getPayerQueries().getMainPayer().executeAsOne()
        val payer = payerFactory.create(payerDB)
        if (groupId != null) {
            setPayerGroupExternalId(payer, groupId)
        }
        return payer
    }

    override fun setMainPayer(payer: Payer): Payer {

        payer.isMainPayer = true

        databaseDao.getPayerQueries().setMainPayer(
            externalId = payer.externalId,
            isMainPayer = if (payer.isMainPayer) 1L else 0L,
            id = payer.id!!)

        return payer

    }

    override fun setOnlinePayer(payer: Payer): Payer {
        payer.onlineUsed = true
        databaseDao.getPayerQueries().setPayerOnlineUsed(
            onlineUsed = if (payer.onlineUsed) 1L else 0L,
            id = payer.id!!)

        return payer
    }


    override fun deletePayer(payerId: Long): PaymentGroup? {
        databaseDao.getPayerQueries().delete(payerId)

        return paymentGroupService.deleteGroupsByPayer(payerId)
    }

    private fun setPayerGroupExternalId(payer: Payer, groupId: Long) {
        val payerGroupExternalId = paymentGroupService.getPayerGroupExternalId(payer.id!!, groupId)
        if (payerGroupExternalId!=null) {
            payer.payerGroupExternalId = payerGroupExternalId
        }
    }
}