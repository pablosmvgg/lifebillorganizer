package com.psm.lifebillorganizer.services

import com.psm.lifebillorganizer.models.Payer
import com.psm.lifebillorganizer.models.PaymentGroup

interface PayerService {
    fun getGroupPayers(groupId: Long): ArrayList<Payer>
    fun getPayers(groupId: Long?): ArrayList<Payer>
    fun getPayerByName(name: String, groupId: Long?): Payer?
    fun getPayerByExternalId(externalId: String, groupId: Long?): Payer
    fun getPayerByPayerGroupExternalId(payerGroupExternalId: String): Payer
    fun insertPayer(name: String, description: String): Payer
    fun updatePayer(payer: Payer)
    fun getMainPayer(groupId: Long?): Payer
    fun setMainPayer(payer: Payer): Payer
    fun setOnlinePayer(payer: Payer): Payer
    fun deletePayer(payerId: Long): PaymentGroup?
}