package com.psm.lifebillorganizer.services

import com.psm.lifebillorganizer.dao.DatabaseDao
import com.psm.lifebillorganizer.factories.PayerFactory
import com.psm.lifebillorganizer.factories.PaymentGroupFactory
import com.psm.lifebillorganizer.models.Payer
import com.psm.lifebillorganizer.models.PaymentGroup
import com.psm.lifebillorganizer.utils.FormatUtil
import retrofit2.Retrofit
import java.time.LocalDateTime
import javax.inject.Inject

class PaymentGroupServiceImpl @Inject constructor(var paymentGroupFactory: PaymentGroupFactory,
                                                 var payerFactory: PayerFactory,
                                                 var databaseDao: DatabaseDao,
                                                 var retrofit: Retrofit,
                                                 var paymentService: PaymentService,
                                                 var periodService: PeriodService)
    : PaymentGroupService {

    override fun getGroup(groupId: Long): PaymentGroup? {
        val groupDB = databaseDao.getPaymentGroupQueries().getById(groupId).executeAsOneOrNull()
        if (groupDB!=null) {
            return generatePaymentGroup(groupDB)
        }
        return null
    }

    override fun getGroup(externalId: String): PaymentGroup? {
        val groupDB = databaseDao.getPaymentGroupQueries().getByExternalId(externalId)
            .executeAsOneOrNull()
        if (groupDB!=null) {
            return generatePaymentGroup(groupDB)
        }
        return null
    }

    override fun getGroups(): ArrayList<PaymentGroup> {
        val paymentGroupsDB = databaseDao.getPaymentGroupQueries().getAll().executeAsList()
        val paymentGroups = paymentGroupFactory.createList(paymentGroupsDB)
        for (paymentGroup in paymentGroups) {
            val payersDB = databaseDao.getPaymentGroupQueries()
                .getGroupPayers(paymentGroup.id!!).executeAsList()
            paymentGroup.payers = payerFactory.createList(payersDB)
            paymentGroup.payers.forEach{ payer ->
                val payerGroupExternalId = databaseDao.getPaymentGroupQueries()
                    .selectPayerGroupExternalId(payer.id!!, paymentGroup.id!!).executeAsOneOrNull()
                if (payerGroupExternalId!=null) {
                    payer.payerGroupExternalId = payerGroupExternalId.payerGroupExternalId
                }

            }
            paymentGroup.period = periodService.getPeriodById(paymentGroup.periodId)
        }

        return paymentGroups
    }

    override fun getGroupsByPayer(payerId: Long): ArrayList<PaymentGroup> {
        val groupsBD = databaseDao.getPaymentGroupQueries().getGroupsByPayer(payerId)
            .executeAsList()
        return paymentGroupFactory.createList(groupsBD)
    }

    override fun getSelectedGroup(): PaymentGroup? {

        val selectedPaymentGroupDB = databaseDao.getPaymentGroupQueries().getSelectedGroup()
            .executeAsOneOrNull()
        if (selectedPaymentGroupDB != null) {
            return generatePaymentGroup(selectedPaymentGroupDB)
        }
        return null
    }

    private fun generatePaymentGroup(paymentGroupDB: com.psm.lifebillorganizer.daos.PaymentGroup): PaymentGroup {
        val selectedPaymentGroup = paymentGroupFactory.create(paymentGroupDB)
        val period = periodService.getPeriodById(selectedPaymentGroup.periodId)
        selectedPaymentGroup.period = period
        val payersDB = databaseDao.getPaymentGroupQueries()
            .getGroupPayers(selectedPaymentGroup.id!!).executeAsList()
        selectedPaymentGroup.payers = payerFactory.createList(payersDB)
        return selectedPaymentGroup
    }

    override fun selectPaymentGroup(paymentGroupId: Long) {
        databaseDao.getPaymentGroupQueries().selectPaymentGroup(paymentGroupId)
    }

    override fun unselectPaymentGroups() {
        databaseDao.getPaymentGroupQueries().unselectPaymentGroups()
    }

    override fun insertPaymentGroup(
        externalId: String?,
        name: String,
        description: String,
        periodId: Long,
        payers: ArrayList<Payer>,
        onlinePayersJoined: Long
    ): PaymentGroup {

        val today = LocalDateTime.now()
        databaseDao.getPaymentGroupQueries().insert(
            name = name,
            description = description,
            createdDate = FormatUtil.formatDBDate(today),
            periodId = periodId,
            selected = 0,
            externalId = externalId,
            onlinePayersJoined = onlinePayersJoined
        )
        val groupCreatedId = databaseDao.getPaymentGroupQueries().lastInsertRowId()
            .executeAsOne()
        val paymentGroup = paymentGroupFactory.create(groupCreatedId, externalId, name, description,
            today, periodId, false, onlinePayersJoined)

        for (payer in payers) {
            databaseDao.getPaymentGroupQueries().insertGroupPayer(
                payerId = payer.id!!,
                groupId = paymentGroup.id!!
            )
        }
        paymentGroup.selected = false
        paymentGroup.period = periodService.getPeriodById(paymentGroup.periodId)
        val payersDB = databaseDao.getPaymentGroupQueries()
            .getGroupPayers(paymentGroup.id!!).executeAsList()
        paymentGroup.payers = payerFactory.createList(payersDB)
        paymentGroup.payers.forEach { payer ->
            val payerGroupExternalId = databaseDao.getPaymentGroupQueries()
                .selectPayerGroupExternalId(payerId = payer.id!!, groupId = paymentGroup.id!!)
                .executeAsOneOrNull()
            if (payerGroupExternalId!=null) {
                payer.payerGroupExternalId = payerGroupExternalId.payerGroupExternalId
            }
        }

        return paymentGroup
    }

    override fun deleteGroup(paymentGroupId: Long): PaymentGroup? {
        //TODO: arreglar un bug que al agregar un grupo luego de haber borrado todos los seleccione
        val groupToDelete = databaseDao.getPaymentGroupQueries().getById(paymentGroupId).executeAsOne()
        val selected = groupToDelete.selected
        databaseDao.getPaymentGroupQueries().delete(paymentGroupId)
        databaseDao.getPaymentGroupQueries().deleteGroupPayers(paymentGroupId)
        paymentService.deleteByPaymentGroup(paymentGroupId)

        if (selected == 1L) {
            //Select by default the first group created
            val groups = databaseDao.getPaymentGroupQueries().getAll().executeAsList()
            if (groups.any()) {
                databaseDao.getPaymentGroupQueries().selectPaymentGroup(groups[0].id)
                return generatePaymentGroup(groups[0])
            }
        }
        return null
    }

    override fun deleteGroupsByPayer(payerId: Long): PaymentGroup? {
        val paymentGroups = getGroupsByPayer(payerId)
        var finalGroupSelected: PaymentGroup? = null
        for (group in paymentGroups) {
            val groupSelected = deleteGroup(group.id!!)
            if (groupSelected != null) {
                finalGroupSelected = groupSelected
            }
        }

        return finalGroupSelected
    }

    override fun updatePaymentGroup(paymentGroup: PaymentGroup) {
        var selected:Long = 0L
        if (paymentGroup.selected) {
            selected = 1L
        }
        databaseDao.getPaymentGroupQueries().update(
            name = paymentGroup.name,
            description = paymentGroup.description,
            periodId = paymentGroup.periodId,
            selected = selected,
            externalId = paymentGroup.externalId,
            id = paymentGroup.id!!
        )
    }

    override fun setPayerGroupExternalId(payerGroupExternalId: String, payerId: Long, groupId: Long) {
        databaseDao.getPaymentGroupQueries().setPayerGroupExternalId(
            payerGroupExternalId = payerGroupExternalId,
            payerId = payerId,
            groupId = groupId
        )
    }

    override fun getPayerGroupExternalId(payerId: Long, groupId: Long): String? {
        val response = databaseDao.getPaymentGroupQueries()
            .selectPayerGroupExternalId(payerId, groupId).executeAsOneOrNull()
        if (response!=null) {
            return response.payerGroupExternalId
        }
        return null
    }

}