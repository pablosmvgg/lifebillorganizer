package com.psm.lifebillorganizer.services

import com.psm.lifebillorganizer.models.FirstTimeExec
import com.psm.lifebillorganizer.models.Period

interface FirstTimeExecService {

    fun getFirstTimeExec(): FirstTimeExec
    fun updateFirstTimeExec(executed: Boolean)
}