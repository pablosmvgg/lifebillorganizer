package com.psm.lifebillorganizer.android.ui.utils

import android.app.Activity
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.view.View
import android.widget.EditText
import com.psm.lifebillorganizer.utils.FormatUtil
import java.util.*

class DatePickerUtil(private val elementWithDate: EditText, private val activity: Activity) :
    View.OnClickListener {
    private var mYear = 0
    private var mMonth = 0
    private var mDay = 0
    private var mHour = 0
    private var mMinute = 0
    override fun onClick(v: View) {
        UIUtil.hideKeyboard(activity)
        // Get Current Date
        val c = Calendar.getInstance()
        mYear = c[Calendar.YEAR]
        mMonth = c[Calendar.MONTH]
        mDay = c[Calendar.DAY_OF_MONTH]
        val datePickerDialog = DatePickerDialog(
            activity,
            { view, year, monthOfYear, dayOfMonth ->
                elementWithDate.setText(
                    FormatUtil.formatNumber2Digits(dayOfMonth)
                        .toString() + "/" + FormatUtil.formatNumber2Digits(monthOfYear + 1) + "/"
                            + year
                )

                //Get Current Time too
                val c = Calendar.getInstance()
                mHour = c[Calendar.HOUR_OF_DAY]
                mMinute = c[Calendar.MINUTE]

                // Launch Time Picker Dialog
                val timePickerDialog = TimePickerDialog(
                    activity,
                    { view, hourOfDay, minute ->
                        elementWithDate.setText(
                            (elementWithDate.text.toString()
                                    + ' ' + FormatUtil.formatNumber2Digits(hourOfDay)
                                .toString() + ":" + FormatUtil.formatNumber2Digits(minute))
                        )
                    }, mHour, mMinute, false
                )
                //timePickerDialog.show()
            }, mYear, mMonth, mDay
        )
        datePickerDialog.show()
    }
}