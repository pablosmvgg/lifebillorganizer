package com.psm.lifebillorganizer.android.ui.paymentgroups

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.psm.lifebillorganizer.MyApplication
import com.psm.lifebillorganizer.R
import com.psm.lifebillorganizer.android.adapters.PaymentGroupAdapter
import com.psm.lifebillorganizer.consts.AppConsts
import com.psm.lifebillorganizer.databinding.FragmentPaymentGroupSelectionBinding
import com.psm.lifebillorganizer.models.PaymentGroup
import com.psm.lifebillorganizer.services.PaymentGroupService
import com.psm.lifebillorganizer.android.ui.MainActivity
import javax.inject.Inject

class PaymentGroupSelectionFragment: Fragment() {

    private lateinit var mainActivity: MainActivity
    private lateinit var paymentGroupsAdapter: PaymentGroupAdapter
    private lateinit var paymentGroups: ArrayList<PaymentGroup>

    private var destinationPage: Int = 0

    @Inject
    lateinit var paymentGroupService: PaymentGroupService

    private var _binding: FragmentPaymentGroupSelectionBinding? = null
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        this.mainActivity = activity as MainActivity
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentPaymentGroupSelectionBinding.inflate(inflater, container, false)

        mainActivity.supportActionBar?.title = getString(R.string.title_payment_group_selection)
        (mainActivity.applicationContext as MyApplication).appComponent.inject(this)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setHasOptionsMenu(true)
        val currentBundle = arguments
        if (currentBundle != null) {
            destinationPage = currentBundle.getInt(AppConsts.BUNDLE_ORIGINAL_PAGE)
        }

        loadPaymentGroups()
        paymentGroupsAdapter = PaymentGroupAdapter(mainActivity, paymentGroups, destinationPage)

        val layoutManagerPG = LinearLayoutManager(mainActivity)
        binding.rvSelectionPaymentGroups.layoutManager = layoutManagerPG
        binding.rvSelectionPaymentGroups.itemAnimator = DefaultItemAnimator()
        binding.rvSelectionPaymentGroups.adapter = paymentGroupsAdapter
    }

    private fun loadPaymentGroups() {
        paymentGroups = paymentGroupService.getGroups()
    }
}