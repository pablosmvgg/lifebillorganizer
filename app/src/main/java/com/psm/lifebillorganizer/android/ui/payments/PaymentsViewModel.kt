package com.psm.lifebillorganizer.android.ui.payments

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.psm.lifebillorganizer.models.Payer
import com.psm.lifebillorganizer.models.Payment
import com.psm.lifebillorganizer.models.PaymentGroup
import com.psm.lifebillorganizer.models.enums.PaymentType
import com.psm.lifebillorganizer.services.PayerService
import com.psm.lifebillorganizer.services.PaymentGroupService
import com.psm.lifebillorganizer.services.PaymentService
import java.time.LocalDateTime
import javax.inject.Inject

class PaymentsViewModel @Inject constructor(val paymentService: PaymentService,
                                            val paymentGroupService: PaymentGroupService)
    : ViewModel() {

    private val _text = MutableLiveData<String>().apply {
        value = "This is home Fragment"
    }
    val text: LiveData<String> = _text

    var paymentGroup: PaymentGroup? = null
    private val payments: MutableLiveData<ArrayList<Payment>> by lazy {
        MutableLiveData<ArrayList<Payment>>().also {
            it.value = loadPayments()
        }
    }
    fun getPayments(): LiveData<ArrayList<Payment>> {
        return payments
    }
    private fun loadPayments(): ArrayList<Payment> {
        paymentGroup = paymentGroupService.getSelectedGroup()
        return paymentService.getPaymentsByGroupAndPeriod(paymentGroup!!,
            PaymentType.COMMON.paymentTypeId, LocalDateTime.now())
    }
}