package com.psm.lifebillorganizer.android.ui

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.content.Intent
import android.content.res.Configuration
import androidx.appcompat.app.AppCompatDelegate
import com.psm.lifebillorganizer.R
import com.psm.lifebillorganizer.consts.AppConsts


class SplashActivity : ParentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {

        setCurrentTheme(this)

        super.onCreate(savedInstanceState)
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finish()
    }
}