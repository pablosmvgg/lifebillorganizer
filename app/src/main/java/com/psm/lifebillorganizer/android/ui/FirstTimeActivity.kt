package com.psm.lifebillorganizer.android.ui

import android.content.Intent
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import android.widget.ArrayAdapter
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import com.psm.lifebillorganizer.MyApplication
import com.psm.lifebillorganizer.R
import com.psm.lifebillorganizer.android.receivers.PaymentRecurrentAlarm
import com.psm.lifebillorganizer.android.ui.paymentgroups.JoinPaymentGroupFragment
import com.psm.lifebillorganizer.android.ui.utils.UIUtil
import com.psm.lifebillorganizer.consts.AppConsts
import com.psm.lifebillorganizer.dao.DatabaseDao
import com.psm.lifebillorganizer.databinding.ActivityFirstTimeBinding
import com.psm.lifebillorganizer.models.Payer
import com.psm.lifebillorganizer.models.Period
import com.psm.lifebillorganizer.services.FirstTimeExecService
import com.psm.lifebillorganizer.services.PayerService
import com.psm.lifebillorganizer.services.PaymentGroupService
import com.psm.lifebillorganizer.services.PeriodService
import com.psm.lifebillorganizer.utils.PeriodUtil
import javax.inject.Inject

class FirstTimeActivity : ParentActivity() {

    @Inject
    lateinit var paymentGroupService: PaymentGroupService
    @Inject
    lateinit var payerService: PayerService
    @Inject
    lateinit var periodService: PeriodService
    @Inject
    lateinit var firstTimeExecService: FirstTimeExecService
    @Inject
    lateinit var databaseDao: DatabaseDao

    private lateinit var binding: ActivityFirstTimeBinding

    var payersShown: Int = 1
    private lateinit var periods: ArrayList<Period>
    private var localPaymentGroupHidden: Boolean = true
    private var onlinePaymentGroupHidden: Boolean = true
    private lateinit var joinFragment: JoinPaymentGroupFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        setCurrentTheme(this)
        super.onCreate(savedInstanceState)

        binding = ActivityFirstTimeBinding.inflate(layoutInflater)
        setContentView(binding.root)

        //Inject
        (applicationContext as MyApplication).appComponent.inject(this)

        databaseDao.initDatabase(applicationContext)

        joinFragment = supportFragmentManager.findFragmentByTag("fg_join_online_group")
                as JoinPaymentGroupFragment
        joinFragment.isEmbedded = true

        getPeriods()
        val adapter: ArrayAdapter<Period> = ArrayAdapter<Period>(
            this,
            android.R.layout.simple_spinner_dropdown_item,
            periods
        )
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.spFirstTimePeriod.adapter = adapter

        binding.btNewLocalPaymentGroup.setOnClickListener(View.OnClickListener {
            localPaymentGroupHidden = !localPaymentGroupHidden
            var img: Drawable? = null
            img = if (localPaymentGroupHidden) {
                binding.clLocalGroupContainer.setVisibility(View.GONE)
                binding.root.resources.getDrawable(R.drawable.outline_expand_more_white_24, this.theme)
            } else {
                binding.clLocalGroupContainer.setVisibility(View.VISIBLE)
                binding.root.resources.getDrawable(R.drawable.outline_expand_less_white_24, this.theme)
            }
            binding.btNewLocalPaymentGroup.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null)

            if (!localPaymentGroupHidden && !onlinePaymentGroupHidden) {
                binding.clOnlineGroupContainer.setVisibility(View.GONE)
                img = binding.root.resources.getDrawable(R.drawable.outline_expand_more_white_24, this.theme)
                binding.btNewOnlinePaymentGroup.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null)
                onlinePaymentGroupHidden = true
            }
        })

        binding.btNewOnlinePaymentGroup.setOnClickListener(View.OnClickListener {
            onlinePaymentGroupHidden = !onlinePaymentGroupHidden
            var img: Drawable? = null
            img = if (onlinePaymentGroupHidden) {
                binding.clOnlineGroupContainer.setVisibility(View.GONE)
                binding.root.resources.getDrawable(R.drawable.outline_expand_more_white_24, this.theme)
            } else {
                binding.clOnlineGroupContainer.setVisibility(View.VISIBLE)
                binding.root.resources.getDrawable(R.drawable.outline_expand_less_white_24, this.theme)
            }
            binding.btNewOnlinePaymentGroup.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null)

            if (!localPaymentGroupHidden && !onlinePaymentGroupHidden) {
                binding.clLocalGroupContainer.setVisibility(View.GONE)
                img = binding.root.resources.getDrawable(R.drawable.outline_expand_more_white_24, this.theme)
                binding.btNewLocalPaymentGroup.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null)
                localPaymentGroupHidden = true
            }
        })

        binding.ibFirstTimeAddPayer.setOnClickListener {
            payersShown++
            when (payersShown) {
                2 -> binding.tilFirstTimePayerName2.visibility = View.VISIBLE
                3 -> binding.tilFirstTimePayerName3.visibility = View.VISIBLE
                4 -> binding.tilFirstTimePayerName4.visibility = View.VISIBLE
            }
            if (payersShown + 1 == AppConsts.MAX_GROUP_PAYERS) {
                binding.ibFirstTimeAddPayer.visibility = View.GONE
            }
        }

        binding.btSaveFirstTime.setOnClickListener(View.OnClickListener {
            UIUtil.hideKeyboard(this)
            if (validateData()) {

                //Save payers first
                val mainPayer = payerService.insertPayer(binding.etFirstTimeMainPayerName.text.toString(), "")
                payerService.setMainPayer(mainPayer)

                if (!localPaymentGroupHidden) {
                    val payersGroup = ArrayList<Payer>()
                    payersGroup.add(mainPayer)
                    if (binding.etFirstTimePayerName1.text.toString().trim().isNotEmpty()) {
                        payersGroup.add(payerService.insertPayer(binding.etFirstTimePayerName1.text.toString(), ""))
                    }
                    //TODO: ver si con algo como reflection se puede reducir esto
                    if (binding.etFirstTimePayerName2.text.toString().trim().isNotEmpty()) {
                        payersGroup.add(payerService.insertPayer(binding.etFirstTimePayerName2.text.toString(), ""))
                    }
                    if (binding.etFirstTimePayerName3.text.toString().trim().isNotEmpty()) {
                        payersGroup.add(payerService.insertPayer(binding.etFirstTimePayerName3.text.toString(), ""))
                    }
                    if (binding.etFirstTimePayerName4.text.toString().trim().isNotEmpty()) {
                        payersGroup.add(payerService.insertPayer(binding.etFirstTimePayerName4.text.toString(), ""))
                    }

                    //Save the group
                    val periodId: Long = (binding.spFirstTimePeriod.selectedItem as Period).id as Long
                    val paymentGroup = paymentGroupService.insertPaymentGroup(null, binding.etPaymentGroupName.text.toString(),
                        "", periodId, payersGroup, 0)
                    paymentGroupService.selectPaymentGroup(paymentGroup.id!!)

                    //Schedule an Alarm to generate recurrent payments for the new period
                    val executionTime = PeriodUtil.getNextTimePeriod(paymentGroup!!.periodId)
                    //TODO: ver si conviene usar injection con esta clase y si conviene mantener juntos el setAlarm y el reciever
                    val alarm = PaymentRecurrentAlarm()
                    alarm.setAlarm(this, paymentGroup.id!!, executionTime)

                }
                else if (!onlinePaymentGroupHidden) {
                    joinFragment.saveOnlineGroup()
                }

                firstTimeExecService.updateFirstTimeExec(true)
                val intent = Intent(this, MainActivity::class.java).apply {
                }
                startActivity(intent)
            }
        })
    }

    private fun hasPayersNameRepeated(): Boolean {
        val payersName = ArrayList<String>()
        if (binding.etFirstTimePayerName1.text.toString().trim().isNotEmpty()) {
            payersName.add(binding.etFirstTimePayerName1.text.toString())
        }
        if (binding.etFirstTimePayerName2.text.toString().trim().isNotEmpty()) {
            if (payersName.contains(binding.etFirstTimePayerName2.text.toString().trim())) {
                return true
            }
            payersName.add(binding.etFirstTimePayerName2.text.toString().trim())
        }
        if (binding.etFirstTimePayerName3.text.toString().trim().isNotEmpty()) {
            if (payersName.contains(binding.etFirstTimePayerName3.text.toString().trim())) {
                return true
            }
            payersName.add(binding.etFirstTimePayerName3.text.toString().trim())
        }
        if (binding.etFirstTimePayerName4.text.toString().trim().isNotEmpty()) {
            if (payersName.contains(binding.etFirstTimePayerName4.text.toString().trim())) {
                return true
            }
            payersName.add(binding.etFirstTimePayerName4.text.toString().trim())
        }

        return false
    }

    private fun validateData(): Boolean {


        if (binding.etFirstTimeMainPayerName.text.isNullOrEmpty()) {
            Snackbar.make(
                binding.etFirstTimePayerName1, getString(R.string.message_error_payer_name_empty),
                Snackbar.LENGTH_LONG
            ).show()
            return false;
        }

        if (hasPayersNameRepeated()) {
            Snackbar.make(
                binding.etFirstTimePayerName1, getString(R.string.message_error_payers_name_repeated),
                Snackbar.LENGTH_LONG
            ).show()
            return false
        }

        if (localPaymentGroupHidden && onlinePaymentGroupHidden) {
            Snackbar.make(
                binding.etPaymentGroupName, getString(R.string.message_error_group_type_not_chosen),
                Snackbar.LENGTH_LONG
            ).show()
            return false;
        }

        if (!localPaymentGroupHidden) {
            if (binding.etPaymentGroupName.text.isNullOrEmpty()) {
                Snackbar.make(
                    binding.etPaymentGroupName, getString(R.string.message_error_group_name_empty),
                    Snackbar.LENGTH_LONG
                ).show()
                return false;
            }
        }

        if (!onlinePaymentGroupHidden) {
            val onlineGroupValidation = joinFragment.validateOnlineGroup()
            if (!onlineGroupValidation) {
                return onlineGroupValidation
            }
        }
        return true
    }

    private fun getPeriods() {
        //Get Periods
        periods = periodService.getPeriods()
    }



}