package com.psm.lifebillorganizer.android.receivers

import android.app.AlarmManager
import android.app.PendingIntent
import android.app.PendingIntent.FLAG_IMMUTABLE
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.psm.lifebillorganizer.android.services.PaymentRecurrentService
import com.psm.lifebillorganizer.consts.AppConsts
import java.util.*

class PaymentRecurrentAlarm: BroadcastReceiver() {


    override fun onReceive(context: Context?, p1: Intent?) {
        val intent = Intent(context, PaymentRecurrentService::class.java)
        intent.putExtra(AppConsts.BUNDLE_PAYMENT_GROUP_ID,
            p1!!.getLongExtra(AppConsts.BUNDLE_PAYMENT_GROUP_ID, 0L))
        context?.startService(intent)
    }

    fun setAlarm(context: Context?, paymentGroupId: Long, executionTime: Calendar) {

        val alarmManager = context!!.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        //Creating an intent to the MyAlarm Class
        val intent = Intent(context, PaymentRecurrentAlarm::class.java)
        intent.putExtra(AppConsts.BUNDLE_PAYMENT_GROUP_ID, paymentGroupId)

        //setting up a pending intent
        val alarmPendingIntent = PendingIntent.getBroadcast(context,
            paymentGroupId.toInt(), intent, FLAG_IMMUTABLE)
        alarmManager.setExactAndAllowWhileIdle(
            AlarmManager.RTC_WAKEUP,
            executionTime.timeInMillis,
            alarmPendingIntent
        )
    }

    fun cancelAlarm(context: Context?, paymentGroupId: Long) {
        val alarmManager = context!!.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        val intent = Intent(context, PaymentRecurrentAlarm::class.java)
        val alarmPendingIntent = PendingIntent.getBroadcast(context,
            paymentGroupId.toInt(), intent, FLAG_IMMUTABLE)
        alarmPendingIntent.cancel()
        alarmManager.cancel(alarmPendingIntent)
    }
}