package com.psm.lifebillorganizer.android.ui.paymentgroups

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.psm.lifebillorganizer.MyApplication
import com.psm.lifebillorganizer.R
import com.psm.lifebillorganizer.android.adapters.PayersAdapter
import com.psm.lifebillorganizer.android.receivers.PaymentRecurrentAlarm
import com.psm.lifebillorganizer.android.ui.FirstTimeActivity
import com.psm.lifebillorganizer.android.ui.MainActivity
import com.psm.lifebillorganizer.android.ui.ParentActivity
import com.psm.lifebillorganizer.android.ui.utils.UIUtil
import com.psm.lifebillorganizer.consts.AppConsts
import com.psm.lifebillorganizer.databinding.FragmentJoinPaymentGroupBinding
import com.psm.lifebillorganizer.models.Payer
import com.psm.lifebillorganizer.models.PaymentGroup
import com.psm.lifebillorganizer.services.PaymentGroupOnlineService
import com.psm.lifebillorganizer.services.PaymentGroupService
import com.psm.lifebillorganizer.utils.PeriodUtil
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

class JoinPaymentGroupFragment: Fragment() {

    private lateinit var mainActivity: ParentActivity

    @Inject
    lateinit var paymentGroupService: PaymentGroupService
    @Inject
    lateinit var paymentGroupOnlineService: PaymentGroupOnlineService


    private var _binding: FragmentJoinPaymentGroupBinding? = null
    private val binding get() = _binding!!

    private var onlineGroup: PaymentGroup? = null

    var isEmbedded: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        this.mainActivity = activity as ParentActivity
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentJoinPaymentGroupBinding.inflate(inflater, container, false)
        if (mainActivity is MainActivity) {
            val currActivity = activity as MainActivity
            currActivity.supportActionBar?.title = getString(R.string.title_join_payment_group)
        }

        //Inject
        (mainActivity.applicationContext as MyApplication).appComponent.inject(this)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setHasOptionsMenu(false)

        binding.ibJoinPaymentGroupSearch.setOnClickListener {
            UIUtil.hideKeyboard(mainActivity)
            if (validateSearchData()) {
                if (isEmbedded)
                    (mainActivity as FirstTimeActivity).blockScreen(binding.clJoinPaymentGroupProgress)
                else
                    (mainActivity as MainActivity).blockScreen(binding.clJoinPaymentGroupProgress)

                CoroutineScope(Dispatchers.IO).launch {
                    onlineGroup = paymentGroupOnlineService
                        .getOnlineGroup(binding.etJoinPaymentGroupExternalId.text.toString())
                    showHideGroupDetail(onlineGroup)
                    if (isEmbedded)
                        (mainActivity as FirstTimeActivity).unblockScreen(binding.clJoinPaymentGroupProgress)
                    else
                        (mainActivity as MainActivity).unblockScreen(binding.clJoinPaymentGroupProgress)
                }
            }
        }
        if (isEmbedded) {
            binding.btJoinPaymentGroupSave.visibility = View.GONE
        }
        binding.btJoinPaymentGroupSave.setOnClickListener {
            if (validateOnlineGroup()) {
                saveOnlineGroup()
            }
        }

    }

    private fun showHideGroupDetail(onlineGroup: PaymentGroup?) {
        mainActivity.runOnUiThread {

            if (onlineGroup == null) {
                binding.clJoinPaymentGroupDetail.visibility = View.GONE
                Snackbar.make(
                    binding.etJoinPaymentGroupExternalId,
                    getString(R.string.message_error_payment_group_doesnt_exist),
                    Snackbar.LENGTH_LONG
                ).show()
                return@runOnUiThread
            }

            val alreadyJoinedGroup = paymentGroupService.getGroup(onlineGroup.externalId!!)
            if (alreadyJoinedGroup!=null) {
                Snackbar.make(
                    binding.etJoinPaymentGroupExternalId,
                    getString(R.string.message_error_payment_group_already_joined),
                    Snackbar.LENGTH_LONG
                ).show()
                return@runOnUiThread
            }

            if (onlineGroup.onlinePayersJoined.toInt() == onlineGroup.payers.size) {
                Snackbar.make(
                    binding.etJoinPaymentGroupExternalId,
                    getString(R.string.message_error_payment_group_all_joined),
                    Snackbar.LENGTH_LONG
                ).show()
                return@runOnUiThread
            }

            //TEST: 22fVe8B
            binding.tvJoinPaymentGroupFoundName.text = onlineGroup.name
            if (onlineGroup.description.isNullOrEmpty()) {
                binding.tvJoinPaymentGroupFoundDesc.visibility = View.GONE
                binding.tvJoinPaymentGroupFoundDescLabel.visibility = View.GONE
            } else {
                binding.tvJoinPaymentGroupFoundDesc.text = onlineGroup.description
            }
            val payersAdapter = PayersAdapter(
                mainActivity, onlineGroup.payers,
                itemSelectable = true, oneSelection = true
            )
            val layoutManager = LinearLayoutManager(activity)
            binding.rvJoinPaymentGroupPayers.layoutManager = layoutManager
            binding.rvJoinPaymentGroupPayers.itemAnimator = DefaultItemAnimator()
            binding.rvJoinPaymentGroupPayers.adapter = payersAdapter

            binding.clJoinPaymentGroupDetail.visibility = View.VISIBLE
        }
    }

    private fun validateSearchData():Boolean {
        if (binding.etJoinPaymentGroupExternalId.text.isNullOrEmpty()) {
            Snackbar.make(
                binding.etJoinPaymentGroupExternalId, getString(R.string.message_error_payment_group_external_id_empty),
                Snackbar.LENGTH_LONG
            ).show()
            return false;
        }
        val maxSizeString = getString(R.string.max_size_7)
        if (binding.etJoinPaymentGroupExternalId.text?.length!= maxSizeString.toInt()) {
            Snackbar.make(
                binding.etJoinPaymentGroupExternalId, getString(R.string.message_error_payment_group_external_id_invalid),
                Snackbar.LENGTH_LONG
            ).show()
            return false;
        }
        return true
    }

    private fun navigateBack() {

        if (mainActivity is MainActivity) {
            val currActivity = activity as MainActivity
            mainActivity.runOnUiThread {
                val bundle = Bundle()
                bundle.putBoolean(AppConsts.BUNDLE_FROM_OPTIONS, true)
                currActivity.navigate(R.id.navigation_join_payment_group, R.id.navigation_options,
                    bundle, true)
            }
        }

    }

    fun validateOnlineGroup(): Boolean {
        val payerSelected = onlineGroup?.payers?.filter { payer: Payer ->
            !payer.onlineUsed && payer.selected }
        if (payerSelected?.size != 1) {
            Snackbar.make(
                binding.etJoinPaymentGroupExternalId,
                getString(R.string.message_error_payment_group_payer_not_selected),
                Snackbar.LENGTH_LONG
            ).show()
            return false
        }

        return true
    }

    fun saveOnlineGroup(): PaymentGroup? {
        CoroutineScope(Dispatchers.IO).launch {
            onlineGroup = paymentGroupOnlineService.insertOnlineGroupLocally(onlineGroup!!)
            if (onlineGroup!=null) {

                mainActivity.runOnUiThread {
                    Snackbar.make(
                        binding.etJoinPaymentGroupExternalId,
                        getString(R.string.message_success_join_payment_group),
                        Snackbar.LENGTH_LONG
                    ).show()
                    //Schedule an Alarm to generate recurrent payments for the new period
                    val executionTime = PeriodUtil.getNextTimePeriod(onlineGroup!!.periodId)
                    //TODO: ver si conviene usar injection con esta clase y si conviene mantener juntos el setAlarm y el reciever
                    val alarm = PaymentRecurrentAlarm()
                    alarm.setAlarm(mainActivity, onlineGroup!!.id!!, executionTime)
                    if (!isEmbedded) {
                        navigateBack()
                    }
                    else {
                        paymentGroupService.selectPaymentGroup(onlineGroup!!.id!!)
                    }
                }
            }
            else {
                mainActivity.runOnUiThread {
                    Snackbar.make(
                        binding.etJoinPaymentGroupExternalId,
                        getString(R.string.message_error_payment_group_payer_not_selected),
                        Snackbar.LENGTH_LONG
                    ).show()
                }
            }
        }
        return onlineGroup
    }
}