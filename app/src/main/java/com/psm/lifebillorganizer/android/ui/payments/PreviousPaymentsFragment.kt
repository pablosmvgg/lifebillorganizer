package com.psm.lifebillorganizer.android.ui.payments

import android.app.AlertDialog
import android.os.Bundle
import android.view.*
import androidx.appcompat.content.res.AppCompatResources
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout.OnRefreshListener
import com.google.android.material.snackbar.Snackbar
import com.psm.lifebillorganizer.MyApplication
import com.psm.lifebillorganizer.R
import com.psm.lifebillorganizer.android.adapters.PaymentsAdapter
import com.psm.lifebillorganizer.android.adapters.SwipeGesture
import com.psm.lifebillorganizer.consts.AppConsts
import com.psm.lifebillorganizer.databinding.FragmentPreviousPaymentsBinding
import com.psm.lifebillorganizer.models.Payment
import com.psm.lifebillorganizer.services.PaymentService
import com.psm.lifebillorganizer.android.ui.MainActivity
import com.psm.lifebillorganizer.android.ui.paymentperiods.PaymentPeriodDetailFragment
import com.psm.lifebillorganizer.utils.FormatUtil
import com.psm.lifebillorganizer.utils.PeriodUtil
import java.time.LocalDateTime
import javax.inject.Inject

class PreviousPaymentsFragment : Fragment() {

    private var paymentList = ArrayList<Payment>()
    private lateinit var periodStartDate: LocalDateTime
    private lateinit var periodEndDate: LocalDateTime
    private lateinit var paymentsAdapter: PaymentsAdapter
    private lateinit var mainActivity: MainActivity
    private var paymentsDetailsShown: Boolean = false

    @Inject
    lateinit var paymentService: PaymentService

    private var _binding: FragmentPreviousPaymentsBinding? = null
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        this.mainActivity = activity as MainActivity
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = FragmentPreviousPaymentsBinding.inflate(inflater, container, false)

        //Inject
        (mainActivity.applicationContext as MyApplication).appComponent
            .inject(this)


        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setHasOptionsMenu(true)

        loadData()
        setPaymentPeriodDetailFragment()

        val title = StringBuilder()

        if (paymentList.size > 0) {
            title.append(getString(R.string.label_period)).append(" ")
            title.append(periodStartDate.format(FormatUtil.getFormatDate())).append("-")
            title.append(periodEndDate.format(FormatUtil.getFormatDate()))
        }

        mainActivity.supportActionBar?.title = title.toString()

        paymentsAdapter = PaymentsAdapter(mainActivity, paymentList, false)

        val swipeGesture = object : SwipeGesture(mainActivity) {
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {

                when(direction) {
                    ItemTouchHelper.LEFT -> {
                        deletePayment(viewHolder, direction)
                    }
                    ItemTouchHelper.RIGHT -> {
                        deletePayment(viewHolder, direction)
                    }
                }
            }
        }
        val touchHelper = ItemTouchHelper(swipeGesture)
        touchHelper.attachToRecyclerView(binding.rvPreviousPayments)

        val layoutManager = LinearLayoutManager(activity)
        binding.rvPreviousPayments.layoutManager = layoutManager
        binding.rvPreviousPayments.itemAnimator = DefaultItemAnimator()
        binding.rvPreviousPayments.adapter = paymentsAdapter
        binding.srlPreviousPayments.setOnRefreshListener(OnRefreshListener {
            binding.srlPreviousPayments.isRefreshing = false
            loadData()
            paymentsAdapter.notifyDataSetChanged()
        })

    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.payments_header_menu, menu)

        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        if (item.itemId == R.id.select_group) {
            if (mainActivity.getSelectedPaymentGroup() != null) {
                val bundle = Bundle()
                bundle.putInt(AppConsts.BUNDLE_ORIGINAL_PAGE, R.id.navigation_previous_payments)
                mainActivity.navigate(R.id.navigation_previous_payments, R.id.navigation_payment_group_selection, bundle, false)
            }
            else {
                Snackbar.make(
                    binding.rvPreviousPayments, getString(R.string.message_error_no_group_created),
                    Snackbar.LENGTH_LONG
                ).show()
            }

            return true
        }
        if (item.itemId == R.id.show_detail) {
            if (paymentsDetailsShown) {
                binding.fmPaymentPeriodDetail.visibility = View.GONE
                binding.srlPreviousPayments.visibility = View.VISIBLE
                item.icon = AppCompatResources.getDrawable(mainActivity,
                    R.drawable.outline_receipt_white_24)
            }
            else {
                binding.fmPaymentPeriodDetail.visibility = View.VISIBLE
                binding.srlPreviousPayments.visibility = View.GONE
                item.icon = AppCompatResources.getDrawable(mainActivity,
                    R.drawable.outline_payments_white_24)
            }
            paymentsDetailsShown = !paymentsDetailsShown
        }
        return false
    }

    private fun showEmptyListMessage(paymentList: ArrayList<Payment>) {
        if (paymentList.isEmpty()) {
            binding.tvPreviousPaymentListEmpty.visibility = View.VISIBLE
            binding.srlPreviousPayments.visibility = View.GONE
        }
        else {
            binding.tvPreviousPaymentListEmpty.visibility = View.GONE
            binding.srlPreviousPayments.visibility = View.VISIBLE
        }
    }

    private fun loadData() {
        mainActivity.blockScreen(binding.clPreviousPaymentsProgress)
        val currentBundle = arguments
        if (currentBundle?.getSerializable(AppConsts.BUNDLE_PAYMENTS) != null) {
            paymentList = currentBundle.getSerializable(AppConsts.BUNDLE_PAYMENTS) as ArrayList<Payment>
            if (paymentList.size > 0) {
                periodStartDate =
                    PeriodUtil.getFirstDatePeriod(mainActivity.getSelectedPaymentGroup()?.periodId!!,
                        paymentList[0].paymentDate)!!
                periodEndDate =
                    PeriodUtil.getLastDatePeriod(mainActivity.getSelectedPaymentGroup()?.periodId!!,
                        paymentList[0].paymentDate)!!
            }
        }
        else {
            paymentList = ArrayList()
        }
        mainActivity.unblockScreen(binding.clPreviousPaymentsProgress)
        showEmptyListMessage(paymentList)
    }

    private fun setPaymentPeriodDetailFragment() {

        val childFragment: Fragment = PaymentPeriodDetailFragment(paymentList, true)
        val transaction: FragmentTransaction = childFragmentManager.beginTransaction()
        transaction.replace(R.id.fm_payment_period_detail, childFragment).commit()
    }

    private fun deletePayment(viewHolder: RecyclerView.ViewHolder, direction: Int) {
        val builder = AlertDialog.Builder(mainActivity)
        builder.setMessage(getString(R.string.message_sure_delete))
            .setCancelable(false)
            .setPositiveButton(getString(R.string.label_yes)) { dialog, id ->
                val itemToDelete = paymentList[viewHolder.adapterPosition]
                paymentService.deletePayment(itemToDelete.id!!)
                paymentsAdapter.deleteItem(viewHolder.adapterPosition)
            }
            .setNegativeButton(getString(R.string.label_no)) { dialog, id ->
                paymentsAdapter.refreshAdapter()
                // Dismiss the dialog
                dialog.dismiss()
            }
        val alert = builder.create()
        alert.show()
    }

}