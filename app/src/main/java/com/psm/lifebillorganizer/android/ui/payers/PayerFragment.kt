package com.psm.lifebillorganizer.android.ui.payers

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ProgressBar
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.textfield.TextInputEditText
import com.psm.lifebillorganizer.MyApplication
import com.psm.lifebillorganizer.R
import com.psm.lifebillorganizer.consts.AppConsts
import com.psm.lifebillorganizer.models.Payer
import com.psm.lifebillorganizer.services.PayerService
import com.psm.lifebillorganizer.android.ui.MainActivity
import com.psm.lifebillorganizer.android.ui.options.OptionsViewModel
import com.psm.lifebillorganizer.android.ui.utils.UIUtil
import com.psm.lifebillorganizer.databinding.FragmentPayerBinding
import com.psm.lifebillorganizer.databinding.FragmentPaymentBinding
import javax.inject.Inject


class PayerFragment : Fragment() {

    private lateinit var mainActivity: MainActivity
    private var _binding: FragmentPayerBinding? = null
    private val binding get() = _binding!!

    private var isNew: Boolean = true
    private lateinit var payer: Payer

    @Inject
    lateinit var payerService: PayerService

    override fun onCreate(savedInstanceState: Bundle?) {
        this.mainActivity = activity as MainActivity
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        setHasOptionsMenu(true)
        mainActivity.supportActionBar?.title = getString(R.string.title_payer)

        _binding = FragmentPayerBinding.inflate(inflater, container, false)

        //Inject
        (mainActivity.applicationContext as MyApplication).appComponent.inject(this)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        this.binding.btSavePayer.setOnClickListener(View.OnClickListener {
            UIUtil.hideKeyboard(mainActivity)
            if (validateData()) {
                mainActivity.blockScreen(binding.clPayerProgress)
                if (isNew) {
                    payerService.insertPayer(this.binding.etPayerName.text.toString(),
                        this.binding.etPayerDesc.text.toString())
                }
                else {
                    payer.name = this.binding.etPayerName.text.toString()
                    payer.description = this.binding.etPayerDesc.text.toString()
                    payerService.updatePayer(payer)
                }
                val bundle = Bundle()
                mainActivity.navigate(R.id.navigation_payer, R.id.navigation_options, bundle, true)
                mainActivity.unblockScreen(binding.clPayerProgress)
            }
        })
        this.getPayer();
    }

    private fun validateData(): Boolean {
        if (this.binding.etPayerName.text.isNullOrEmpty()) {
            Snackbar.make(
                this.binding.etPayerName, getString(R.string.message_error_name_empty),
                Snackbar.LENGTH_LONG
            ).show()
            return false;
        }
        val payerFound = payerService.getPayerByName(this.binding.etPayerName.text.toString(), null)
        if (payerFound != null) {
            Snackbar.make(
                this.binding.etPayerName, getString(R.string.message_error_payer_name_repeated),
                Snackbar.LENGTH_LONG
            ).show()
            return false;
        }

        return true;
    }

    private fun getPayer() {
        mainActivity.blockScreen(binding.clPayerProgress)
        val currentBundle = arguments
        if (currentBundle?.getSerializable(AppConsts.BUNDLE_PAYER) != null) {
            isNew = false
            payer = currentBundle.getSerializable(AppConsts.BUNDLE_PAYER) as Payer
            this.binding.etPayerName.setText(payer.name)
            this.binding.etPayerDesc.setText(payer.description)
        }
        mainActivity.unblockScreen(binding.clPayerProgress)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment PayerFragment.
         */
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            PayerFragment().apply {

            }
    }
}