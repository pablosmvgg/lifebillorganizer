package com.psm.lifebillorganizer.android.services

import android.app.Service
import android.content.Intent
import android.os.IBinder
import android.os.HandlerThread
import android.os.Process.THREAD_PRIORITY_BACKGROUND
import android.util.Log
import android.widget.Toast
import com.psm.lifebillorganizer.MyApplication
import com.psm.lifebillorganizer.android.receivers.PaymentRecurrentAlarm
import com.psm.lifebillorganizer.consts.AppConsts
import com.psm.lifebillorganizer.dao.DatabaseDao
import com.psm.lifebillorganizer.services.PaymentGroupService
import com.psm.lifebillorganizer.services.PaymentService
import javax.inject.Inject
import android.os.Bundle
import com.psm.lifebillorganizer.models.enums.PaymentType
import com.psm.lifebillorganizer.utils.PeriodUtil
import java.time.LocalDateTime


class PaymentRecurrentService: Service() {

    @Inject
    lateinit var databaseDao: DatabaseDao
    @Inject
    lateinit var paymentGroupService: PaymentGroupService
    @Inject
    lateinit var paymentService: PaymentService

    override fun onCreate() {
        super.onCreate()
        //Inject
        (applicationContext as MyApplication).appComponent.inject(this)
        databaseDao.initDatabase(applicationContext)
    }

    override fun onBind(intent: Intent): IBinder? = null

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        super.onStartCommand(intent, flags, startId)

        val paymentGroupId = intent!!.getLongExtra(AppConsts.BUNDLE_PAYMENT_GROUP_ID, 0L)
        val paymentGroup = paymentGroupService.getGroup(paymentGroupId)
        if (paymentGroup !=null) {
            Toast.makeText(this, "Llamando al Service con grupo ${paymentGroup.name}",
                Toast.LENGTH_SHORT).show();

            val today = LocalDateTime.now()
            val firstDatePeriod = PeriodUtil.getFirstDatePeriod(paymentGroup.periodId, today)
            if (today.dayOfMonth == firstDatePeriod!!.dayOfMonth) {
                paymentService.savePreviousRecurrentPayments(paymentGroup,
                    PaymentType.COMMON)
            }

            val alarm = PaymentRecurrentAlarm()
            val executionTime = PeriodUtil.getNextTimePeriod(paymentGroup.periodId)
            alarm.setAlarm(this, paymentGroupId, executionTime)

        }

        return START_STICKY
    }

}