package com.psm.lifebillorganizer.android.adapters

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.psm.lifebillorganizer.R
import com.psm.lifebillorganizer.consts.AppConsts
import com.psm.lifebillorganizer.databinding.ItemPaymentBinding
import com.psm.lifebillorganizer.models.Payment
import com.psm.lifebillorganizer.models.enums.PaymentType
import com.psm.lifebillorganizer.android.ui.MainActivity
import com.psm.lifebillorganizer.utils.FormatUtil
import java.text.DecimalFormat

// https://www.youtube.com/watch?v=xE8z8wiXz18
class PaymentsAdapter(private val mainActivity: MainActivity,
                      private val dataSet: ArrayList<Payment>,
                      private val isActualPeriod: Boolean) :
    RecyclerView.Adapter<PaymentsAdapter.ViewHolder>() {

    private val dec = DecimalFormat("#.##")

    /**
     * Provide a reference to the type of views that you are using
     * (custom ViewHolder).
     */
    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val binding = ItemPaymentBinding.bind(view)

    }

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        // Create a new view, which defines the UI of the list item
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.item_payment, viewGroup, false)

        return ViewHolder(view)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {

        //Edit payment
        viewHolder.itemView.setOnClickListener(View.OnClickListener {

            val bundle = Bundle()
            bundle.putSerializable(AppConsts.BUNDLE_PAYMENT, dataSet[position])
            bundle.putSerializable(AppConsts.BUNDLE_PAYMENT_TYPE,
                PaymentType.getPaymentType(dataSet[position].paymentType))
            if (isActualPeriod) {
                mainActivity.navigate(R.id.navigation_payments, R.id.navigation_payment, bundle,
                    false)
            }
            else {
                bundle.putSerializable(AppConsts.BUNDLE_PAYMENTS, dataSet)
                mainActivity.navigate(R.id.navigation_previous_payments, R.id.navigation_payment, bundle,
                    false)
            }
        })

        viewHolder.binding.tvPaymentTitle.text = dataSet[position].name
        viewHolder.binding.tvPaymentValue.text = FormatUtil.formatDouble2DigitsMoney(dataSet[position].value)
        viewHolder.binding.tvPaymentPayer.text = mainActivity.getString(R.string.label_payed_by) + " " + dataSet[position].payer.name
        viewHolder.binding.tvPaymentDate.text = dataSet[position].paymentDate
            .format(FormatUtil.getSimpleFormatDate())

        if (dataSet[position].paymentRecurrent) {
            viewHolder.binding.tvPaymentRecurrency.visibility = View.VISIBLE
            var recurrency = mainActivity.getString(R.string.label_payment_recurrent_count) + ": " + mainActivity.getString(R.string.label_payment_recurrent_count_status)
            if (dataSet[position].paymentRecurrentCount > 0L) {
                recurrency = mainActivity.getString(R.string.label_payment_recurrent_count) + ": " + (dataSet[position].paymentRecurrentCount - 1L) + " " + mainActivity.getString(R.string.label_remaining)
            }
            viewHolder.binding.tvPaymentRecurrency.text = recurrency
        }
        else {
            viewHolder.binding.tvPaymentRecurrency.visibility = View.GONE
        }

    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount() = dataSet.size

    fun deleteItem(i: Int) {
        dataSet.removeAt(i)
        refreshAdapter()
    }

    fun addItem(i: Int, payment: Payment) {
        dataSet.add(i, payment)
        refreshAdapter()
    }

    fun refreshAdapter() {
        notifyDataSetChanged()
    }

}
