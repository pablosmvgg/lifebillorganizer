package com.psm.lifebillorganizer.android.ui.paymentgroups

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.psm.lifebillorganizer.MyApplication
import com.psm.lifebillorganizer.R
import com.psm.lifebillorganizer.android.adapters.PayersAdapter
import com.psm.lifebillorganizer.android.receivers.PaymentRecurrentAlarm
import com.psm.lifebillorganizer.consts.AppConsts
import com.psm.lifebillorganizer.databinding.FragmentPaymentGroupBinding
import com.psm.lifebillorganizer.models.Payer
import com.psm.lifebillorganizer.models.PaymentGroup
import com.psm.lifebillorganizer.models.Period
import com.psm.lifebillorganizer.services.PayerService
import com.psm.lifebillorganizer.services.PaymentGroupService
import com.psm.lifebillorganizer.services.PeriodService
import com.psm.lifebillorganizer.android.ui.MainActivity
import com.psm.lifebillorganizer.android.ui.options.OptionsViewModel
import com.psm.lifebillorganizer.android.ui.utils.UIUtil
import com.psm.lifebillorganizer.services.PaymentGroupOnlineService
import com.psm.lifebillorganizer.utils.PeriodUtil
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.lang.StringBuilder
import java.time.LocalDateTime
import javax.inject.Inject

class PaymentGroupFragment: Fragment() {

    private lateinit var mainActivity: MainActivity
    private lateinit var payersAdapter: PayersAdapter

    private var isNew: Boolean = true;
    private lateinit var paymentGroup: PaymentGroup;
    private lateinit var periods: ArrayList<Period>
    private lateinit var payers: ArrayList<Payer>

    @Inject
    lateinit var paymentGroupService: PaymentGroupService
    @Inject
    lateinit var paymentGroupOnlineService: PaymentGroupOnlineService
    @Inject
    lateinit var periodService: PeriodService
    @Inject
    lateinit var payerService: PayerService

    private var _binding: FragmentPaymentGroupBinding? = null
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        this.mainActivity = activity as MainActivity
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentPaymentGroupBinding.inflate(inflater, container, false)

        mainActivity.supportActionBar?.title = getString(R.string.title_payment_group)

        //Inject
        (mainActivity.applicationContext as MyApplication).appComponent.inject(this)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setHasOptionsMenu(true)
        //Load Data
        //getPaymentGroup()

        binding.smPaymentGroupOnline.setOnClickListener {
            if (binding.smPaymentGroupOnline.isChecked) {
                binding.tvGroupOnlineDescription.visibility = View.VISIBLE
            }
            else {
                binding.tvGroupOnlineDescription.visibility = View.GONE
            }
        }

        binding.btSavePaymentGroup.setOnClickListener(View.OnClickListener {
            UIUtil.hideKeyboard(mainActivity)
            if (validateData()) {
                if (isNew) {
                    paymentGroup = PaymentGroup(0,  binding.etPaymentGroupName.text.toString(),
                        binding.etPaymentGroupDesc.text.toString(), LocalDateTime.now(),
                        (binding.spPeriod.selectedItem as Period).id as Long
                    )

                    val selectedPayers = payers.filter { it.selected  }
                    var paymentGroup = paymentGroupService.insertPaymentGroup(null,
                        paymentGroup.name, paymentGroup.description.toString(),
                        paymentGroup.periodId, selectedPayers as ArrayList<Payer>, 0)

                    //Schedule an Alarm to generate recurrent payments for the new period
                    val executionTime = PeriodUtil.getNextTimePeriod(paymentGroup.periodId)
                    //TODO: ver si conviene usar injection con esta clase y si conviene mantener juntos el setAlarm y el reciever
                    val alarm = PaymentRecurrentAlarm()
                    alarm.setAlarm(mainActivity, paymentGroup.id!!, executionTime)

                    if (mainActivity.getSelectedPaymentGroup() == null) {
                        mainActivity.setSelectedPaymentGroup(paymentGroup)
                    }

                    if (binding.smPaymentGroupOnline.isChecked) {
                        CoroutineScope(Dispatchers.IO).launch {
                            paymentGroup = paymentGroupOnlineService.insertOnlineGroup(paymentGroup)!!
                            Snackbar.make(
                                binding.btSavePaymentGroup,
                                getString(R.string.message_success_group_saved_online),
                                Snackbar.LENGTH_LONG
                            ).show()
                        }
                    }
                    else {
                        Snackbar.make(
                            binding.btSavePaymentGroup,
                            getString(R.string.message_success_group_saved),
                            Snackbar.LENGTH_LONG
                        ).show()
                    }
                }
                else {
                    paymentGroup.name = binding.etPaymentGroupName.text.toString()
                    paymentGroup.description = binding.etPaymentGroupDesc.text.toString()
                    /*mainActivity.getPaymentGroupQueries().update(
                        name = paymentGroup.getName().toString(),
                        description = paymentGroup.getDescription().toString(),
                        create
                        id = payer.getId()
                    )*/
                    //completar el edit por si se usa alguna vez
                }
                val bundle = Bundle()
                mainActivity.navigate(R.id.navigation_payer, R.id.navigation_options, bundle, true)
            }
        })

        //Periods
        getPeriods()
        val adapter: ArrayAdapter<Period> = ArrayAdapter<Period>(
            mainActivity,
            android.R.layout.simple_spinner_dropdown_item,
            periods
        )
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.spPeriod.adapter = adapter

        //Payers
        getPayers()
        payersAdapter = PayersAdapter(mainActivity, payers, itemSelectable = true,
            oneSelection = false)
        val layoutManager = LinearLayoutManager(activity)
        binding.rvPayers.layoutManager = layoutManager
        binding.rvPayers.itemAnimator = DefaultItemAnimator()
        binding.rvPayers.adapter = payersAdapter

    }

    private fun validateData(): Boolean {
        if (binding.etPaymentGroupName.text.isNullOrEmpty()) {
            Snackbar.make(
                binding.etPaymentGroupName, getString(R.string.message_error_name_empty),
                Snackbar.LENGTH_LONG
            ).show()
            return false;
        }

        val selectedPayers = payers.filter { it.selected  }
        if (selectedPayers.isEmpty()) {
            Snackbar.make(
                binding.etPaymentGroupName, getString(R.string.message_error_payers_empty),
                Snackbar.LENGTH_LONG
            ).show()
            return false;
        }

        if (selectedPayers.size > AppConsts.MAX_GROUP_PAYERS) {
            val message = StringBuilder()
            message.append(getString(R.string.message_error_max_payers_groups_exceeded_part1))
            message.append(AppConsts.MAX_GROUP_PAYERS)
            message.append(getString(R.string.message_error_max_payers_groups_exceeded_part2))
            Snackbar.make(
                binding.etPaymentGroupName, message.toString(),
                Snackbar.LENGTH_LONG
            ).show()
            return false;
        }

        return true;
    }

    //queda por su aparece el Editar
    private fun getPaymentGroup() {

        val currentBundle = arguments
        if (currentBundle?.getSerializable(AppConsts.BUNDLE_PAYMENT_GROUP) != null) {
            isNew = false
            paymentGroup = currentBundle.getSerializable(AppConsts.BUNDLE_PAYMENT_GROUP) as PaymentGroup

            binding.etPaymentGroupName.setText(paymentGroup.name)
            binding.etPaymentGroupDesc.setText(paymentGroup.description)
            //sumar seteo de period y lista de payers
        }
    }

    private fun getPeriods() {
        //Get Periods
        periods = periodService.getPeriods()
    }

    private fun getPayers() {
        payers = payerService.getPayers(null)
    }
}