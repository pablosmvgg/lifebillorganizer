package com.psm.lifebillorganizer.android.adapters

import android.app.Activity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.psm.lifebillorganizer.R
import com.psm.lifebillorganizer.databinding.ItemPayerBinding
import com.psm.lifebillorganizer.models.Payer
import com.psm.lifebillorganizer.android.ui.MainActivity
import java.text.DecimalFormat

class PayersAdapter(private val mainActivity: Activity,
                    private val dataSet: ArrayList<Payer>,
                    private val itemSelectable: Boolean,
                    private val oneSelection: Boolean) :
    RecyclerView.Adapter<PayersAdapter.ViewHolder>() {

    private val dec = DecimalFormat("#.##")

    /**
     * Provide a reference to the type of views that you are using
     * (custom ViewHolder).
     */
    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val binding = ItemPayerBinding.bind(view)
    }

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        // Create a new view, which defines the UI of the list item
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.item_payer, viewGroup, false)
        if (itemSelectable) {
            view.setBackgroundColor(ContextCompat.getColor(mainActivity,
                R.color.lt_unselected_item))
        }

        return ViewHolder(view)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {

        if (itemSelectable && !oneSelection) {
            if (itemSelectable && dataSet[position].isMainPayer) {
                dataSet[position].selected = true
                viewHolder.itemView.setBackgroundColor(ContextCompat.getColor(mainActivity,
                    R.color.lt_selected_item))
                viewHolder.itemView.isEnabled = false
            }
        }
        if (itemSelectable && oneSelection) {
            if (dataSet[position].onlineUsed) {
                dataSet[position].selected = true
                viewHolder.itemView.setBackgroundColor(ContextCompat.getColor(mainActivity,
                    R.color.lt_selected_item))
                viewHolder.itemView.isEnabled = false
            }
            else {
                dataSet[position].selected = false
                viewHolder.itemView.setBackgroundColor(ContextCompat.getColor(mainActivity,
                    R.color.lt_unselected_item))
                viewHolder.itemView.isEnabled = true
            }


        }

        viewHolder.itemView.setOnClickListener(View.OnClickListener {

            if (itemSelectable) {
                if (dataSet[position].selected) {
                    dataSet[position].selected = false
                    viewHolder.itemView.setBackgroundColor(ContextCompat.getColor(mainActivity,
                        R.color.lt_unselected_item))
                }
                else {
                    dataSet[position].selected = true
                    viewHolder.itemView.setBackgroundColor(ContextCompat.getColor(mainActivity,
                        R.color.lt_selected_item))
                }
            }
            else {
                if (mainActivity is MainActivity) {
                    val currActivity = mainActivity as MainActivity
                    val bundle = Bundle()
                    bundle.putSerializable("payer", dataSet[position])
                    currActivity.navigate(R.id.navigation_options, R.id.navigation_payer, bundle, false)
                }

            }
        })

        viewHolder.binding.tvItemPayerName.text = dataSet[position].name
    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount() = dataSet.size

    fun deleteItem(i: Int) {
        dataSet.removeAt(i)
        refreshAdapter()
    }

    fun addItem(i: Int, payer: Payer) {
        dataSet.add(i, payer)
        refreshAdapter()
    }

    fun refreshAdapter() {
        notifyDataSetChanged()
    }
}