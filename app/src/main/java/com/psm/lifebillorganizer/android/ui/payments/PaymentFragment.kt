package com.psm.lifebillorganizer.android.ui.payments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar
import com.psm.lifebillorganizer.MyApplication
import com.psm.lifebillorganizer.android.ui.MainActivity
import com.psm.lifebillorganizer.R
import com.psm.lifebillorganizer.consts.AppConsts
import com.psm.lifebillorganizer.databinding.FragmentPaymentBinding
import com.psm.lifebillorganizer.models.Payer
import com.psm.lifebillorganizer.models.Payment
import com.psm.lifebillorganizer.models.enums.PaymentType
import com.psm.lifebillorganizer.services.PayerService
import com.psm.lifebillorganizer.services.PaymentService
import com.psm.lifebillorganizer.android.ui.utils.DatePickerUtil
import com.psm.lifebillorganizer.android.ui.utils.UIUtil
import com.psm.lifebillorganizer.services.PaymentGroupOnlineService
import com.psm.lifebillorganizer.utils.FormatUtil
import com.psm.lifebillorganizer.utils.InternetUtil
import com.psm.lifebillorganizer.utils.PeriodUtil
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.time.LocalDateTime
import javax.inject.Inject

/**
 * A simple [Fragment] subclass.
 * Use the [PaymentFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class PaymentFragment : Fragment() {

    private lateinit var mainActivity: MainActivity

    private lateinit var currentPayment: Payment
    private lateinit var payers: ArrayList<Payer>
    private var isNew: Boolean = true
    private lateinit var paymentType: PaymentType

    private lateinit var previousPayments: ArrayList<Payment>

    @Inject
    lateinit var paymentService: PaymentService
    @Inject
    lateinit var payerService: PayerService
    @Inject
    lateinit var paymentGroupOnlineService: PaymentGroupOnlineService

    private var _binding: FragmentPaymentBinding? = null
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        this.mainActivity = activity as MainActivity
        super.onCreate(savedInstanceState)
        arguments?.let {

        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentPaymentBinding.inflate(inflater, container, false)

        //Inject
        (mainActivity.applicationContext as MyApplication).appComponent.inject(this)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setHasOptionsMenu(true)

        val currentBundle = arguments
        paymentType = currentBundle?.getSerializable(AppConsts.BUNDLE_PAYMENT_TYPE) as PaymentType

        var title = getString(R.string.title_payment_common)
        if (paymentType == PaymentType.BALANCE) {
            title = getString(R.string.title_payment_balance)
        }
        mainActivity.supportActionBar?.title = title

        binding.ibPaymentPaymentdate.setOnClickListener(DatePickerUtil(binding.etPaymentPaymentdate, mainActivity))

        getPayers()
        val adapter: ArrayAdapter<Payer> = ArrayAdapter<Payer>(
            mainActivity,
            android.R.layout.simple_spinner_dropdown_item,
            payers
        )
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.spPayer.adapter = adapter

        binding.btSavePayment.setOnClickListener(View.OnClickListener {
            UIUtil.hideKeyboard(mainActivity)
            binding.pgPayment.visibility = View.VISIBLE
            if (validateData()) {
                mainActivity.blockScreen(binding.clPaymentProgress)
                val paymentDate = FormatUtil.formatDateWithTime0(binding.etPaymentPaymentdate
                    .text.toString())
                var recurrentCount: Long = 0L
                if (binding.etPaymentRecurrentCount.text.toString().any()) {
                    recurrentCount = binding.etPaymentRecurrentCount.text.toString().toLong()
                }
                val mainPayer = payerService.getMainPayer(mainActivity.getSelectedPaymentGroup()?.id!!)
                val paymentGroup = mainActivity.getSelectedPaymentGroup()!!
                if (isNew) {
                    val payment = paymentService.insertPayment(binding.etPaymentName.text.toString(),
                        binding.etPaymentDesc.text.toString(), binding.etPaymentValue.text.toString().toDouble(),
                        paymentDate, (binding.spPayer.selectedItem as Payer).id!!,
                        (binding.spPayer.selectedItem as Payer).payerGroupExternalId,
                        mainActivity.getSelectedPaymentGroup()?.id!!,
                        paymentType.paymentTypeId,
                        binding.cbPaymentRecurrent.isChecked,
                        recurrentCount, 1,
                        mainPayer.payerGroupExternalId, null,
                        false)

                    //Save online the payment if the group is online

                    if (paymentGroup.externalId != null) {
                        //Check internet
                        if (InternetUtil.isOnline(mainActivity)) {
                            CoroutineScope(Dispatchers.IO).launch {
                                val isSuccessInsert = paymentGroupOnlineService.insertPaymentOnline(
                                    payment, mainActivity.getSelectedPaymentGroup()!!.externalId!!,
                                    mainActivity.getSelectedPaymentGroup()!!.id!!)
                                if (isSuccessInsert) {
                                    payment.isSubmitted = true
                                    paymentService.updatePayment(payment)
                                }
                                mainActivity.runOnUiThread {

                                }
                            }
                        }

                    }
                }
                else {

                    currentPayment.id = currentPayment.id!!
                    currentPayment.name = binding.etPaymentName.text.toString()
                    currentPayment.description = binding.etPaymentDesc.text.toString()
                    currentPayment.value = binding.etPaymentValue.text.toString().toDouble()
                    currentPayment.paymentDate = paymentDate
                    currentPayment.payerId = (binding.spPayer.selectedItem as Payer).id!!
                    currentPayment.paymentRecurrent = binding.cbPaymentRecurrent.isChecked
                    currentPayment.paymentRecurrentCount = recurrentCount
                    currentPayment.payerExternalIdModified = mainPayer.payerGroupExternalId

                    paymentService.updatePayment(currentPayment)
                    //Save online the payment if the group is online
                    if (paymentGroup.externalId != null) {
                        //Check internet
                        if (InternetUtil.isOnline(mainActivity)) {
                            CoroutineScope(Dispatchers.IO).launch {
                                val isSuccessUpdate = paymentGroupOnlineService
                                    .updatePaymentOnline(currentPayment,
                                    mainActivity.getSelectedPaymentGroup()!!.externalId!!,
                                        mainActivity.getSelectedPaymentGroup()!!.id!!)
                                if (isSuccessUpdate) {
                                    currentPayment.isSubmitted = true
                                    paymentService.updatePayment(currentPayment)
                                }
                                mainActivity.runOnUiThread {

                                }
                            }
                        }

                    }
                }

                val bundle = Bundle()
                val currentBundle = arguments

                if (currentBundle?.getSerializable(AppConsts.BUNDLE_PAYMENTS) != null) {
                    bundle.putSerializable(AppConsts.BUNDLE_PAYMENTS, previousPayments)
                    mainActivity.navigate(R.id.navigation_payment, R.id.navigation_previous_payments, bundle, true)
                }
                else {
                    if (paymentType == PaymentType.COMMON) {
                        mainActivity.navigate(R.id.navigation_payment, R.id.navigation_payments, bundle, true)
                    }
                    else {
                        mainActivity.navigate(R.id.navigation_payment, R.id.navigation_balance_payments, bundle, true)
                    }
                }
                mainActivity.unblockScreen(binding.clPaymentProgress)
            }
            else {
                binding.pgPayment.visibility = View.GONE;
            }
        })

        getPayment()
        binding.cbPaymentRecurrent.setOnCheckedChangeListener(
            CompoundButton.OnCheckedChangeListener { compoundButton, b ->
                if (b) {
                    binding.cbPaymentRecurrentCount.visibility = View.VISIBLE
                    binding.cbPaymentRecurrentCount.isChecked = true
                }
                else {
                    binding.cbPaymentRecurrentCount.visibility = View.GONE
                    binding.etPaymentRecurrentCount.setText("")
                    binding.tilPaymentRecurrentCount.visibility = View.GONE
                }
            })

        binding.cbPaymentRecurrentCount.setOnCheckedChangeListener { compoundButton, b ->
            if (!b) {
                binding.tilPaymentRecurrentCount.visibility = View.VISIBLE
            } else {
                binding.etPaymentRecurrentCount.setText("")
                binding.tilPaymentRecurrentCount.visibility = View.GONE
            }
        }

        if (paymentType == PaymentType.BALANCE) {
            binding.cbPaymentRecurrent.visibility = View.GONE
        }
    }

    private fun getPayment() {
        mainActivity.blockScreen(binding.clPaymentProgress)
        val currentBundle = arguments
        if (currentBundle?.getSerializable("payment") != null) {
            isNew = false
            currentPayment = currentBundle.getSerializable("payment") as Payment
            binding.tvPaymentTitle.setText(R.string.title_edit_payment)
            binding.etPaymentName.setText(currentPayment.name)
            binding.etPaymentDesc.setText(currentPayment.description)
            binding.etPaymentValue.setText(currentPayment.value.toString())
            binding.etPaymentPaymentdate.setText(currentPayment.paymentDate
                .format(FormatUtil.getSimpleFormatDate()))

            val adapter = binding.spPayer.adapter as ArrayAdapter<Payer>
            var pos = 0
            for(payer in payers) {
                if (currentPayment.payerId == payer.id) {
                    pos = adapter.getPosition(payer)
                }
            }
            binding.spPayer.setSelection(pos)
            binding.cbPaymentRecurrent.isChecked = currentPayment.paymentRecurrent
            if (binding.cbPaymentRecurrent.isChecked) {
                binding.cbPaymentRecurrentCount.visibility = View.VISIBLE
                if (currentPayment.paymentRecurrentCount == 0L) {
                    binding.cbPaymentRecurrentCount.isChecked = true
                }
                else {
                    binding.cbPaymentRecurrentCount.isChecked = false
                    binding.tilPaymentRecurrentCount.visibility = View.VISIBLE
                    binding.etPaymentRecurrentCount.setText(currentPayment.paymentRecurrentCount.toString())
                }
            }
        }
        else {
            isNew = true
            binding.etPaymentPaymentdate.setText(LocalDateTime.now().format(FormatUtil.getSimpleFormatDate()))
        }

        if (currentBundle?.getSerializable(AppConsts.BUNDLE_PAYMENTS) != null) {
            previousPayments = currentBundle.getSerializable(AppConsts.BUNDLE_PAYMENTS) as ArrayList<Payment>
        }
        mainActivity.unblockScreen(binding.clPaymentProgress)
    }

    private fun getPayers() {
        payers = payerService.getGroupPayers(mainActivity.getSelectedPaymentGroup()?.id!!)
    }

    private fun validateData(): Boolean {
        if (mainActivity.getSelectedPaymentGroup()==null) {
            Snackbar.make(
                binding.etPaymentName, getString(R.string.message_error_no_group_selected),
                Snackbar.LENGTH_LONG
            ).show()
            return false;
        }
        if (binding.etPaymentName.text.isNullOrEmpty()) {
            Snackbar.make(
                binding.etPaymentName, getString(R.string.message_error_name_empty),
                Snackbar.LENGTH_LONG
            ).show()
            return false;
        }
        if (binding.etPaymentPaymentdate.text.isNullOrEmpty()) {
            Snackbar.make(
                binding.etPaymentPaymentdate, getString(R.string.message_error_creation_date_empty),
                Snackbar.LENGTH_LONG
            ).show()
            return false;
        }
        //Por ahora se guarda con horario 00:00:00 si se necesita cambiar habilitar la hora
        // en el dialog de fechas
        val paymentDate = LocalDateTime.parse(
            "${binding.etPaymentPaymentdate.text.toString()} 00:00:00",
            FormatUtil.getFormatDate())

        val endPeriodDate = PeriodUtil.getLastDatePeriod(
            mainActivity.getSelectedPaymentGroup()!!.periodId, LocalDateTime.now())
        if (paymentDate.isAfter(endPeriodDate)) {
            Snackbar.make(
                binding.etPaymentPaymentdate, getString(R.string.message_error_payment_date_after_end_period),
                Snackbar.LENGTH_LONG
            ).show()
            return false;
        }

        if (binding.etPaymentValue.text.isNullOrEmpty()) {
            Snackbar.make(
                binding.etPaymentValue, getString(R.string.message_error_value_empty),
                Snackbar.LENGTH_LONG
            ).show()
            return false;
        }
        if (binding.etPaymentValue.text!!.get(binding.etPaymentValue.text!!.length -1) == '.' ||
            binding.etPaymentValue.text!!.get(binding.etPaymentValue.text!!.length -1) == ',') {
            Snackbar.make(
                binding.etPaymentValue, getString(R.string.message_error_value_invalid),
                Snackbar.LENGTH_LONG
            ).show()
            return false;
        }

        if (binding.cbPaymentRecurrent.isChecked) {

            /*if (paymentDate!!.isBefore(startPeriodDate)) {
                Snackbar.make(
                    binding.etPaymentRecurrentCount, getString(R.string.message_error_payment_recurrent_previous_period),
                    Snackbar.LENGTH_LONG
                ).show()
                return false;
            }*/
            if (!binding.cbPaymentRecurrentCount.isChecked) {
                if (binding.etPaymentRecurrentCount.text.isNullOrEmpty()
                    || binding.etPaymentRecurrentCount.text.toString().toLong() == 0L) {
                    Snackbar.make(
                        binding.etPaymentRecurrentCount, getString(R.string.message_error_payment_recurrent_count_invalid),
                        Snackbar.LENGTH_LONG
                    ).show()
                    return false;
                }
            }
        }

        return true
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment PaymentFragment.
         */
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            PaymentFragment().apply {
                arguments = Bundle().apply {

                }
            }
    }
}