package com.psm.lifebillorganizer.android.ui.payments

import android.app.AlertDialog
import android.os.Bundle
import android.view.*
import androidx.appcompat.content.res.AppCompatResources
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout.OnRefreshListener
import com.google.android.material.snackbar.Snackbar
import com.psm.lifebillorganizer.MyApplication
import com.psm.lifebillorganizer.R
import com.psm.lifebillorganizer.android.adapters.PaymentsAdapter
import com.psm.lifebillorganizer.android.adapters.SwipeGesture
import com.psm.lifebillorganizer.consts.AppConsts
import com.psm.lifebillorganizer.databinding.FragmentPaymentsBinding
import com.psm.lifebillorganizer.models.Payment
import com.psm.lifebillorganizer.models.enums.PaymentType
import com.psm.lifebillorganizer.services.PaymentService
import com.psm.lifebillorganizer.android.ui.MainActivity
import com.psm.lifebillorganizer.android.ui.paymentperiods.PaymentPeriodDetailFragment
import com.psm.lifebillorganizer.services.PaymentGroupOnlineService
import com.psm.lifebillorganizer.utils.InternetUtil
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.time.LocalDateTime
import javax.inject.Inject


class PaymentsFragment : Fragment() {

    private var paymentList = ArrayList<Payment>()
    private lateinit var paymentsAdapter: PaymentsAdapter
    private lateinit var mainActivity: MainActivity

    @Inject
    lateinit var paymentService: PaymentService
    @Inject
    lateinit var paymentGroupOnlineService: PaymentGroupOnlineService
    @Inject
    lateinit var paymentsViewModel: PaymentsViewModel

    private var paymentsDetailsShown: Boolean = false

    private var _binding: FragmentPaymentsBinding? = null
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        this.mainActivity = activity as MainActivity
        super.onCreate(null)
        //super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View {
        _binding = FragmentPaymentsBinding.inflate(inflater, container, false)

        //Inject
        (mainActivity.applicationContext as MyApplication).appComponent.inject(this)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setHasOptionsMenu(true)

        val title = StringBuilder()
        if (mainActivity.getSelectedPaymentGroup() != null) {
            title.append(mainActivity.getSelectedPaymentGroup()?.name)
        }
        else {
            title.append(getString(R.string.title_no_group_created))
        }

        mainActivity.supportActionBar?.title = title.toString()


        binding.fabAddPayment.setOnClickListener(View.OnClickListener {
            if (mainActivity.getSelectedPaymentGroup()==null) {
                Snackbar.make(
                    binding.fabAddPayment, getString(R.string.message_error_no_group_selected),
                    Snackbar.LENGTH_LONG
                ).show()
            }
            else {
                val bundle = Bundle()
                bundle.putSerializable(AppConsts.BUNDLE_PAYMENT_TYPE, PaymentType.COMMON)
                mainActivity.navigate(R.id.navigation_payments, R.id.navigation_payment, bundle, false)
            }
        })

        loadData()

        //https://material.io/components/app-bars-top/android
        //https://medium.com/swlh/how-to-create-custom-appbar-actionbar-toolbar-in-android-studio-java-61907fa1e44
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.payments_header_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        if (item.itemId == R.id.select_group) {
            val bundle = Bundle()
            bundle.putInt(AppConsts.BUNDLE_ORIGINAL_PAGE, R.id.navigation_payments)
            mainActivity.navigate(R.id.navigation_payments, R.id.navigation_payment_group_selection, bundle, false)

            return true
        }
        if (item.itemId == R.id.show_detail) {
            if (paymentsDetailsShown) {
                binding.fmPaymentPeriodDetail.visibility = View.GONE
                binding.srlPayments.visibility = View.VISIBLE
                binding.fabAddPayment.visibility = View.VISIBLE
                item.icon = AppCompatResources.getDrawable(mainActivity,
                    R.drawable.outline_receipt_white_24)
            }
            else {
                binding.fmPaymentPeriodDetail.visibility = View.VISIBLE
                binding.srlPayments.visibility = View.GONE
                binding.fabAddPayment.visibility = View.GONE
                item.icon = AppCompatResources.getDrawable(mainActivity,
                    R.drawable.outline_payments_white_24)
            }
            paymentsDetailsShown = !paymentsDetailsShown
        }
        return false
    }

    private fun showEmptyListMessage(paymentList: ArrayList<Payment>) {
        if (paymentList.isEmpty()) {
            binding.tvPaymentListEmpty.visibility = View.VISIBLE
            binding.srlPayments.visibility = View.GONE
        }
        else {
            binding.tvPaymentListEmpty.visibility = View.GONE
            binding.srlPayments.visibility = View.VISIBLE
        }
    }

    private fun loadData() {
        //TODO: implementar esto del block unblock en otras acciones como guardar.
        mainActivity.blockScreen(binding.clPaymentsProgress)
        paymentList = ArrayList()
        val currentBundle = arguments
        if (currentBundle?.getSerializable(AppConsts.BUNDLE_PAYMENTS) != null) {
            paymentList = currentBundle.getSerializable(AppConsts.BUNDLE_PAYMENTS) as ArrayList<Payment>
            setPaymentsAdapter(true)
            setPaymentPeriodDetailFragment()
            showEmptyListMessage(paymentList)
            mainActivity.unblockScreen(binding.clPaymentsProgress)
        }
        else {
            val paymentGroup = mainActivity.getSelectedPaymentGroup()

            if (paymentGroup != null) {
                paymentGroup.payments = ArrayList()
                if (paymentGroup.externalId != null && InternetUtil.isOnline(mainActivity)) {
                    //Call external DB to get the latest changes and synchronize
                    CoroutineScope(Dispatchers.IO).launch {
                        if (paymentGroup.onlinePayersJoined < paymentGroup.payers.size) {
                            paymentGroupOnlineService.getUpdateOnlinePayersByGroup(paymentGroup)
                        }
                        paymentGroupOnlineService.processUnSubmittedPayments(paymentGroup)
                        paymentList = paymentGroupOnlineService
                            .getOnlinePaymentsByGroupAndPeriod(paymentGroup,
                                PaymentType.COMMON.paymentTypeId, LocalDateTime.now())
                        mainActivity.runOnUiThread {
                            setPaymentsAdapter(true)
                            setPaymentPeriodDetailFragment()
                            showEmptyListMessage(paymentList)
                            mainActivity.unblockScreen(binding.clPaymentsProgress)
                        }
                    }
                }
                else {
                    paymentList = paymentService.getPaymentsByGroupAndPeriod(paymentGroup!!,
                        PaymentType.COMMON.paymentTypeId, LocalDateTime.now())
                    setPaymentsAdapter(true)
                    setPaymentPeriodDetailFragment()
                    showEmptyListMessage(paymentList)
                    mainActivity.unblockScreen(binding.clPaymentsProgress)
                }
            }
            else {
                showEmptyListMessage(paymentList)
                mainActivity.unblockScreen(binding.clPaymentsProgress)
            }
        }

    }

    private fun setPaymentsAdapter(isFirstTime: Boolean) {
        if (isFirstTime) {
            paymentsAdapter = PaymentsAdapter(mainActivity, paymentList, true)
            val swipeGesture = object : SwipeGesture(mainActivity) {
                override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                    when(direction) {
                        ItemTouchHelper.LEFT -> {
                            deletePayment(viewHolder, direction)
                        }
                        ItemTouchHelper.RIGHT -> {
                            deletePayment(viewHolder, direction)
                        }
                    }
                }
            }
            val touchHelper = ItemTouchHelper(swipeGesture)
            touchHelper.attachToRecyclerView(binding.rvPayments)

            val layoutManager = LinearLayoutManager(activity)
            binding.rvPayments.layoutManager = layoutManager
            binding.rvPayments.itemAnimator = DefaultItemAnimator()
            binding.rvPayments.adapter = paymentsAdapter

            binding.srlPayments.setOnRefreshListener(OnRefreshListener {
                binding.srlPayments.isRefreshing = false
                loadData()
                setPaymentsAdapter(false)
            })
        }
        else {
            paymentsAdapter.notifyDataSetChanged()
        }
    }

    private fun setPaymentPeriodDetailFragment() {
        val childFragment: Fragment = PaymentPeriodDetailFragment(paymentList, true)
        val transaction: FragmentTransaction = childFragmentManager.beginTransaction()
        transaction.replace(R.id.fm_payment_period_detail, childFragment).commit()
    }

    private fun deletePayment(viewHolder: RecyclerView.ViewHolder, direction: Int) {
        val builder = AlertDialog.Builder(mainActivity)
        builder.setMessage(getString(R.string.message_sure_delete))
            .setCancelable(false)
            .setPositiveButton(getString(R.string.label_yes)) { dialog, id ->
                val itemToDelete = paymentList[viewHolder.adapterPosition]
                val paymentGroup = mainActivity.getSelectedPaymentGroup()!!
                if (paymentGroup.externalId != null) {
                    if (InternetUtil.isOnline(mainActivity)) {
                        itemToDelete.deleted = true
                        CoroutineScope(Dispatchers.IO).launch {
                            val isSuccessDelete = paymentGroupOnlineService.updatePaymentOnline(
                                itemToDelete, mainActivity.getSelectedPaymentGroup()!!.externalId!!,
                                mainActivity.getSelectedPaymentGroup()!!.id!!)

                            mainActivity.runOnUiThread {
                                if (isSuccessDelete) {
                                    paymentService.deletePayment(itemToDelete.id!!)
                                    paymentsAdapter.deleteItem(viewHolder.adapterPosition)
                                    paymentList.remove(itemToDelete)
                                    setPaymentPeriodDetailFragment()
                                }
                            }
                        }
                    }
                    else {
                        paymentService.deletePaymentLogically(itemToDelete.id!!)
                        paymentsAdapter.deleteItem(viewHolder.adapterPosition)
                        paymentList.remove(itemToDelete)
                        setPaymentPeriodDetailFragment()
                    }
                }
                else {
                    paymentService.deletePayment(itemToDelete.id!!)
                    paymentsAdapter.deleteItem(viewHolder.adapterPosition)
                    paymentList.remove(itemToDelete)
                    setPaymentPeriodDetailFragment()
                }
            }
            .setNegativeButton(getString(R.string.label_no)) { dialog, id ->
                paymentsAdapter.refreshAdapter()
                // Dismiss the dialog
                dialog.dismiss()
            }
        val alert = builder.create()
        alert.show()
    }

}