package com.psm.lifebillorganizer.android.adapters

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.psm.lifebillorganizer.R
import com.psm.lifebillorganizer.consts.AppConsts
import com.psm.lifebillorganizer.databinding.ItemPaymentPeriodBinding
import com.psm.lifebillorganizer.models.PaymentPeriod
import com.psm.lifebillorganizer.android.ui.MainActivity

class PaymentPeriodAdapter(val mainActivity: MainActivity,
                           private val dataSet: ArrayList<PaymentPeriod>):
    RecyclerView.Adapter<PaymentPeriodAdapter.ViewHolder>(){

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val binding = ItemPaymentPeriodBinding.bind(view)
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        // Create a new view, which defines the UI of the list item
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.item_payment_period, viewGroup, false)

        //(mainActivity.applicationContext as MyApplication).appComponent.inject(this)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        viewHolder.binding.tvItemPaymentPeriodName.text = dataSet[position].name
        viewHolder.binding.tvItemPaymentPeriodCount.text =
            mainActivity.getString(R.string.label_payments_count) + " " + dataSet[position].payments.size.toString()


        viewHolder.itemView.setOnClickListener(View.OnClickListener {
            val bundle = Bundle()
            bundle.putSerializable(AppConsts.BUNDLE_PAYMENTS, dataSet[position].payments)
            mainActivity.navigate(R.id.navigation_payment_periods,
                R.id.navigation_previous_payments, bundle, false)
        })
    }

    override fun getItemCount() = dataSet.size

    fun deleteItem(i: Int) {
        dataSet.removeAt(i)
        refreshAdapter()
    }

    fun addItem(i: Int, paymentPeriod: PaymentPeriod) {
        dataSet.add(i, paymentPeriod)
        refreshAdapter()
    }

    fun refreshAdapter() {
        notifyDataSetChanged()
    }
}