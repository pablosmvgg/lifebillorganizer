package com.psm.lifebillorganizer.android.ui.paymentperiods

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.psm.lifebillorganizer.MyApplication
import com.psm.lifebillorganizer.android.adapters.PayerPaymentAdapter
import com.psm.lifebillorganizer.databinding.FragmentPaymentPeriodDetailBinding
import com.psm.lifebillorganizer.models.Payment
import com.psm.lifebillorganizer.models.PaymentDetail
import com.psm.lifebillorganizer.models.enums.PaymentType
import com.psm.lifebillorganizer.services.PaymentService
import com.psm.lifebillorganizer.android.ui.MainActivity
import com.psm.lifebillorganizer.utils.FormatUtil
import javax.inject.Inject

class PaymentPeriodDetailFragment(paymentList: ArrayList<Payment>, showPeriodDetail: Boolean):
    Fragment() {

    private var paymentList = ArrayList<Payment>()
    private var showPeriodDetail: Boolean
    private var allPayments = ArrayList<Payment>()
    private var paymentBalances  = ArrayList<Payment>()
    private lateinit var paymentDetail: PaymentDetail
    private lateinit var allPaymentDetail: PaymentDetail
    private lateinit var mainActivity: MainActivity

    @Inject
    lateinit var paymentService: PaymentService

    private var _binding: FragmentPaymentPeriodDetailBinding? = null
    private val binding get() = _binding!!

    init {
        this.paymentList = paymentList
        this.showPeriodDetail = showPeriodDetail
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        this.mainActivity = activity as MainActivity
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = FragmentPaymentPeriodDetailBinding.inflate(inflater, container, false)

        setHasOptionsMenu(false)

        //Inject
        (mainActivity.applicationContext as MyApplication).appComponent.inject(this)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (showPeriodDetail) {
            setCurrentPeriodPaymentDetail()
        }
        else {
            binding.clPayerPeriod.visibility = View.GONE
        }

        setAllPayments()

    }

    private fun setCurrentPeriodPaymentDetail() {
        if (paymentList.any()) {
            val currentGroup = mainActivity.getSelectedPaymentGroup()!!
            paymentBalances = paymentService.getPaymentsByGroupAndPeriod(currentGroup,
                PaymentType.BALANCE.paymentTypeId, paymentList[0].paymentDate)
            paymentDetail = paymentService
                .calculatePaymentsDetail(mainActivity.getSelectedPaymentGroup()!!, paymentList,
                    paymentBalances)

            binding.tvPaymentsTotalPaid.text = FormatUtil.formatDouble2DigitsMoney(paymentDetail.getTotal())
            binding.tvPaymentsTotalEachPayer.text = FormatUtil.formatDouble2DigitsMoney(paymentDetail.getTotalEachPayer())

            //Payer payment
            val payersPaymentsAdapter = PayerPaymentAdapter(mainActivity,
                paymentDetail.getPaymentPayerDetail())
            val layoutManager = LinearLayoutManager(activity)
            binding.rvPayersPayments.layoutManager = layoutManager
            binding.rvPayersPayments.itemAnimator = DefaultItemAnimator()
            binding.rvPayersPayments.adapter = payersPaymentsAdapter

            //Payer balances
            val payersBalancesAdapter = PayerPaymentAdapter(mainActivity,
                paymentDetail.getPayerBalances())
            val layoutManagerBalances = LinearLayoutManager(activity)
            binding.rvPayersBalances.layoutManager = layoutManagerBalances
            binding.rvPayersBalances.itemAnimator = DefaultItemAnimator()
            binding.rvPayersBalances.adapter = payersBalancesAdapter

        }
    }

    private fun setAllPayments() {
        if (paymentList.any()) {
            val currentGroup = mainActivity.getSelectedPaymentGroup()!!
            allPayments = paymentService.getPaymentsByGroup(currentGroup,
                PaymentType.COMMON.paymentTypeId)
            paymentBalances = paymentService.getPaymentsByGroup(currentGroup, PaymentType.BALANCE.paymentTypeId)
            allPaymentDetail = paymentService.calculatePaymentsDetail(paymentGroup= currentGroup,
                payments= allPayments, paymentsBalances = paymentBalances)

            //Payer debts
            val payersDebtsAdapter = PayerPaymentAdapter(mainActivity,
                allPaymentDetail.getPayerDebts())
            val layoutManagerDebts = LinearLayoutManager(activity)
            binding.rvAllPayersDebts.layoutManager = layoutManagerDebts
            binding.rvAllPayersDebts.itemAnimator = DefaultItemAnimator()
            binding.rvAllPayersDebts.adapter = payersDebtsAdapter
        }

    }

}