package com.psm.lifebillorganizer.android.ui.payments

import android.app.AlertDialog
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout.OnRefreshListener
import com.google.android.material.snackbar.Snackbar
import com.psm.lifebillorganizer.MyApplication
import com.psm.lifebillorganizer.R
import com.psm.lifebillorganizer.android.adapters.PaymentsAdapter
import com.psm.lifebillorganizer.android.adapters.SwipeGesture
import com.psm.lifebillorganizer.consts.AppConsts
import com.psm.lifebillorganizer.databinding.FragmentBalancePaymentsBinding
import com.psm.lifebillorganizer.models.Payment
import com.psm.lifebillorganizer.models.enums.PaymentType
import com.psm.lifebillorganizer.services.PaymentService
import com.psm.lifebillorganizer.android.ui.MainActivity
import javax.inject.Inject

class BalancePaymentsFragment: Fragment() {

    private var paymentList = ArrayList<Payment>()
    private lateinit var balancePaymentsAdapter: PaymentsAdapter
    private lateinit var mainActivity: MainActivity

    @Inject
    lateinit var paymentService: PaymentService

    private var _binding: FragmentBalancePaymentsBinding? = null
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        this.mainActivity = activity as MainActivity
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentBalancePaymentsBinding.inflate(inflater, container, false)

        //Inject
        (mainActivity.applicationContext as MyApplication).appComponent
            .inject(this)
        setHasOptionsMenu(true)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val title = StringBuilder()
        if (mainActivity.getSelectedPaymentGroup() != null) {
            title.append(mainActivity.getSelectedPaymentGroup()?.name)
        }
        else {
            title.append(getString(R.string.title_no_group_created))
        }

        mainActivity.supportActionBar?.title = title.toString()

        //TODO Bug: Si tenes mas de 2 integrantes al balancear calcula mal. Habria que sumar a quien se paga el balance y recalcular bien
        binding.fabAddBalancePayment.setOnClickListener(View.OnClickListener {
            if (mainActivity.getSelectedPaymentGroup()==null) {
                Snackbar.make(
                    binding.fabAddBalancePayment, getString(R.string.message_error_no_group_selected),
                    Snackbar.LENGTH_LONG
                ).show()
            }
            else {
                val bundle = Bundle()
                bundle.putSerializable(AppConsts.BUNDLE_PAYMENT_TYPE, PaymentType.BALANCE)
                mainActivity.navigate(R.id.navigation_balance_payments, R.id.navigation_payment, bundle, false)
            }
        })

        loadData()

        balancePaymentsAdapter = PaymentsAdapter(mainActivity, paymentList, true)
        val swipeGesture = object : SwipeGesture(mainActivity) {
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                when(direction) {
                    ItemTouchHelper.LEFT -> {
                        deleteBalance(viewHolder, direction)
                    }
                    ItemTouchHelper.RIGHT -> {
                        deleteBalance(viewHolder, direction)
                    }
                }
            }
        }
        val touchHelper = ItemTouchHelper(swipeGesture)
        touchHelper.attachToRecyclerView(binding.rvBalancePayments)

        val layoutManager = LinearLayoutManager(activity)
        binding.rvBalancePayments.layoutManager = layoutManager
        binding.rvBalancePayments.itemAnimator = DefaultItemAnimator()
        binding.rvBalancePayments.adapter = balancePaymentsAdapter
        binding.srlBalancePayments.setOnRefreshListener(OnRefreshListener {
            binding.srlBalancePayments.isRefreshing = false
            loadData()
            balancePaymentsAdapter.notifyDataSetChanged()
        })
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.payments_header_only_groups_menu, menu)

        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        if (item.itemId == R.id.select_group) {
            val bundle = Bundle()
            bundle.putInt(AppConsts.BUNDLE_ORIGINAL_PAGE, R.id.navigation_balance_payments)
            mainActivity.navigate(R.id.navigation_payments, R.id.navigation_payment_group_selection, bundle, false)

            return true
        }

        return false
    }

    private fun showEmptyListMessage(paymentList: ArrayList<Payment>) {
        if (paymentList.isEmpty()) {
            binding.tvBalancePaymentListEmpty.visibility = View.VISIBLE
            binding.srlBalancePayments.visibility = View.GONE
        }
        else {
            binding.tvBalancePaymentListEmpty.visibility = View.GONE
            binding.srlBalancePayments.visibility = View.VISIBLE
        }
    }

    private fun loadData() {
        mainActivity.blockScreen(binding.clBalancePaymentsProgress)
        val group = mainActivity.getSelectedPaymentGroup()
        if (group != null) {
            paymentList = paymentService.getPaymentsByGroup(group,
                PaymentType.BALANCE.paymentTypeId)
        }
        else {
            paymentList = ArrayList()
        }
        mainActivity.unblockScreen(binding.clBalancePaymentsProgress)
        showEmptyListMessage(paymentList)
    }

    private fun deleteBalance(viewHolder: RecyclerView.ViewHolder, direction: Int) {
        val builder = AlertDialog.Builder(mainActivity)
        builder.setMessage(getString(R.string.message_sure_delete))
            .setCancelable(false)
            .setPositiveButton(getString(R.string.label_yes)) { dialog, id ->
                val itemToDelete = paymentList[viewHolder.adapterPosition]
                paymentService.deletePayment(itemToDelete.id!!)
                balancePaymentsAdapter.deleteItem(viewHolder.adapterPosition)
                paymentList.remove(itemToDelete)
            }
            .setNegativeButton(getString(R.string.label_no)) { dialog, id ->
                balancePaymentsAdapter.refreshAdapter()
                // Dismiss the dialog
                dialog.dismiss()
            }
        val alert = builder.create()
        alert.show()
    }
}