package com.psm.lifebillorganizer.android.ui

import android.app.Activity
import android.content.Context
import android.content.res.Configuration
import android.view.View
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import com.psm.lifebillorganizer.R
import com.psm.lifebillorganizer.consts.AppConsts

open class ParentActivity: AppCompatActivity() {

    fun setCurrentTheme(activity: Activity) {
        val sharedPref = activity.getPreferences(Context.MODE_PRIVATE) ?: return
        val themeSelected = sharedPref.getString(
            AppConsts.PREFERENCE_THEME,
            AppConsts.PREFERENCE_THEME_SYSTEM)

        when (themeSelected) {
            AppConsts.PREFERENCE_THEME_DARK -> {
                activity.setTheme(R.style.darkTheme)
            }
            AppConsts.PREFERENCE_THEME_LIGHT -> {
                activity.setTheme(R.style.AppTheme)
            }
            AppConsts.PREFERENCE_THEME_SYSTEM -> {
                when (AppCompatDelegate.getDefaultNightMode()) {
                    AppCompatDelegate.MODE_NIGHT_YES -> activity.setTheme(R.style.darkTheme)
                    AppCompatDelegate.MODE_NIGHT_NO -> activity.setTheme(R.style.AppTheme)
                    AppCompatDelegate.MODE_NIGHT_UNSPECIFIED -> {
                        val nightModeFlags: Int = activity.applicationContext.resources.configuration.uiMode and
                                Configuration.UI_MODE_NIGHT_MASK
                        when (nightModeFlags) {
                            Configuration.UI_MODE_NIGHT_YES -> activity.setTheme(R.style.darkTheme)
                            Configuration.UI_MODE_NIGHT_NO -> activity.setTheme(R.style.AppTheme)
                            Configuration.UI_MODE_NIGHT_UNDEFINED -> activity.setTheme(R.style.AppTheme)
                        }
                    }
                }
            }
        }
    }

    fun blockScreen(progressBarContainer: View) {
        runOnUiThread {
            progressBarContainer.visibility = View.VISIBLE
            window.setFlags(
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
        }
    }

    fun unblockScreen(progressBarContainer: View) {
        runOnUiThread {
            progressBarContainer.visibility = View.GONE
            window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
        }
    }
}