package com.psm.lifebillorganizer.android.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.psm.lifebillorganizer.R
import com.psm.lifebillorganizer.databinding.ItemPayerPaymentBinding
import com.psm.lifebillorganizer.models.PaymentPayerDetail
import com.psm.lifebillorganizer.android.ui.MainActivity
import com.psm.lifebillorganizer.utils.FormatUtil

class PayerPaymentAdapter(private val mainActivity: MainActivity,
                          private val dataSet: ArrayList<PaymentPayerDetail>) : RecyclerView.Adapter<PayerPaymentAdapter.ViewHolder>() {

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val binding = ItemPayerPaymentBinding.bind(view)
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.item_payer_payment, viewGroup, false)

        return ViewHolder(view)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {

        viewHolder.binding.tvItemPayerPaymentName.text = dataSet[position].payer.name
        viewHolder.binding.tvItemPayerPaymentValue.text = FormatUtil
            .formatDouble2DigitsMoney(dataSet[position].totalPaid)
    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount() = dataSet.size

    fun deleteItem(i: Int) {
        dataSet.removeAt(i)
        refreshAdapter()
    }

    fun addItem(i: Int, paymentPayerDetail: PaymentPayerDetail) {
        dataSet.add(i, paymentPayerDetail)
        refreshAdapter()
    }

    fun refreshAdapter() {
        notifyDataSetChanged()
    }
}