package com.psm.lifebillorganizer.android.adapters

import android.app.AlertDialog
import android.content.Context
import android.os.Bundle
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import androidx.annotation.AttrRes
import androidx.annotation.ColorInt
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.psm.lifebillorganizer.MyApplication
import com.psm.lifebillorganizer.R
import com.psm.lifebillorganizer.android.receivers.PaymentRecurrentAlarm
import com.psm.lifebillorganizer.databinding.ItemPaymentGroupBinding
import com.psm.lifebillorganizer.models.PaymentGroup
import com.psm.lifebillorganizer.services.PaymentGroupService
import com.psm.lifebillorganizer.android.ui.MainActivity
import com.psm.lifebillorganizer.services.PaymentGroupOnlineService
import com.psm.lifebillorganizer.utils.InternetUtil
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.text.DecimalFormat
import javax.inject.Inject

class PaymentGroupAdapter(
    val mainActivity: MainActivity,
    private val dataSet: ArrayList<PaymentGroup>,
    private val destinationSelection: Int):
    RecyclerView.Adapter<PaymentGroupAdapter.ViewHolder>() {

    @Inject
    lateinit var paymentGroupService: PaymentGroupService
    @Inject
    lateinit var paymentGroupOnlineService: PaymentGroupOnlineService

    private val dec = DecimalFormat("#.##")

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val binding = ItemPaymentGroupBinding.bind(view)
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        // Create a new view, which defines the UI of the list item
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.item_payment_group, viewGroup, false)

        (mainActivity.applicationContext as MyApplication).appComponent.inject(this)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {

        viewHolder.binding.tvItemPaymentGroupName.text = dataSet[position].name
        viewHolder.binding.tvItemPaymentGroupPeriod.text = mainActivity.getString(R.string.label_payment_group_period) + " " + dataSet[position].period.name
        var payersList = ""
        for (payer in dataSet[position].payers) {
            payersList += payer.name + ", "
        }

        viewHolder.binding.tvItemPaymentGroupPayers.text = mainActivity.getString(R.string.label_payer_list) + " " + payersList.substring(0, payersList.length-2)
        if (destinationSelection != 0) {
            if (dataSet[position].selected) {
                viewHolder.itemView.setBackgroundColor(
                    mainActivity.getColorFromAttr(R.attr.selected_item))
            }
            else {
                viewHolder.itemView.setBackgroundColor(
                    mainActivity.getColorFromAttr(R.attr.unselected_item))
            }
            viewHolder.binding.tbGroupOnline.isEnabled = false
        }

        viewHolder.itemView.setOnClickListener(View.OnClickListener {
            if (destinationSelection != 0) {
                paymentGroupService.unselectPaymentGroups()
                paymentGroupService.selectPaymentGroup(dataSet[position].id!!)
                mainActivity.setSelectedPaymentGroup(dataSet[position])
                viewHolder.itemView.setBackgroundColor(
                    mainActivity.getColorFromAttr(R.attr.selected_item))
                val bundle = Bundle()
                mainActivity.navigate(R.id.navigation_payment_group_selection,
                    destinationSelection, bundle, true)

            }
        })
        if (dataSet[position].externalId==null) {
            viewHolder.binding.tbGroupOnline.setBackgroundColor(mainActivity.getColorFromAttr(R.attr.item_offline_background))
        }
        else {
            viewHolder.binding.tbGroupOnline.setBackgroundColor(mainActivity.getColorFromAttr(R.attr.item_online_background))
            viewHolder.binding.tbGroupOnline.isChecked = true
            viewHolder.binding.tbGroupOnline.isEnabled = false
        }
        viewHolder.binding.tbGroupOnline
            .setOnCheckedChangeListener { toggleButton, isChecked ->
                if (isChecked) {

                    if (InternetUtil.isOnline(mainActivity)) {
                        val builder = AlertDialog.Builder(mainActivity)
                        builder.setMessage(mainActivity.getString(R.string.message_make_online_group_title))
                            .setCancelable(false)
                            .setPositiveButton(mainActivity.getString(R.string.label_yes)) { dialog, _ ->
                                toggleButton.setBackgroundColor(mainActivity.getColorFromAttr(R.attr.item_online_background))
                                viewHolder.binding.tbGroupOnline.isEnabled = false
                                CoroutineScope(Dispatchers.IO).launch {
                                    paymentGroupOnlineService.insertOnlineGroup(dataSet[position])
                                    val groupOnline = paymentGroupService.getGroup(dataSet[position].id!!)
                                    mainActivity.runOnUiThread {
                                        if (groupOnline != null && groupOnline.selected) {
                                            mainActivity.setSelectedPaymentGroup(groupOnline)
                                        }
                                    }
                                }
                            }
                            .setNegativeButton(mainActivity.getString(R.string.label_no)) { dialog, _ ->
                                viewHolder.binding.tbGroupOnline.isChecked = false
                                // Dismiss the dialog
                                dialog.dismiss()
                            }
                        val alert = builder.create()
                        alert.show()
                    }
                    else {
                        Snackbar.make(
                            viewHolder.binding.tbGroupOnline,
                            mainActivity.getString(R.string.message_error_payment_group_online_no_internet),
                            Snackbar.LENGTH_LONG
                        ).show()
                    }


                } else {
                    toggleButton.setBackgroundColor(mainActivity.getColorFromAttr(R.attr.item_offline_background))
                }
            }

    }

    @ColorInt
    fun Context.getColorFromAttr(
        @AttrRes attrColor: Int,
        typedValue: TypedValue = TypedValue(),
        resolveRefs: Boolean = true
    ): Int {
        theme.resolveAttribute(attrColor, typedValue, resolveRefs)
        return typedValue.data
    }

    override fun getItemCount() = dataSet.size

    fun deleteItem(i: Int) {
        dataSet.removeAt(i)
        refreshAdapter()
    }

    fun addItem(i: Int, paymentGroup: PaymentGroup) {
        dataSet.add(i, paymentGroup)
        refreshAdapter()
    }

    fun refreshAdapter() {
        notifyDataSetChanged()
    }

}