package com.psm.lifebillorganizer.android.ui.paymentperiods

import android.os.Bundle
import android.view.*
import androidx.appcompat.content.res.AppCompatResources
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout.OnRefreshListener
import com.psm.lifebillorganizer.MyApplication
import com.psm.lifebillorganizer.R
import com.psm.lifebillorganizer.android.adapters.PaymentPeriodAdapter
import com.psm.lifebillorganizer.consts.AppConsts
import com.psm.lifebillorganizer.databinding.FragmentPaymentPeriodsBinding
import com.psm.lifebillorganizer.models.Payment
import com.psm.lifebillorganizer.models.PaymentPeriod
import com.psm.lifebillorganizer.services.PaymentPeriodService
import com.psm.lifebillorganizer.services.PaymentService
import com.psm.lifebillorganizer.android.ui.MainActivity
import javax.inject.Inject

class PaymentPeriodsFragment : Fragment() {

    private lateinit var paymentPeriodsViewModel: PaymentPeriodsViewModel

    private lateinit var mainActivity: MainActivity
    private lateinit var paymentPeriodAdapter: PaymentPeriodAdapter

    private lateinit var paymentPeriods: ArrayList<PaymentPeriod>

    @Inject
    lateinit var paymentPeriodService: PaymentPeriodService
    @Inject
    lateinit var paymentService: PaymentService

    private var paymentsDetailsShown: Boolean = false

    private var _binding: FragmentPaymentPeriodsBinding? = null
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        this.mainActivity = activity as MainActivity
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentPaymentPeriodsBinding.inflate(inflater, container, false)

        /*val textView: TextView = root.findViewById(R.id.text_dashboard)
        paymentPeriodsViewModel.text.observe(viewLifecycleOwner, Observer {
            textView.text = it
        })*/


        //Inject
        (mainActivity.applicationContext as MyApplication).appComponent.inject(this)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setHasOptionsMenu(true)

        val title = StringBuilder()
        if (mainActivity.getSelectedPaymentGroup()!=null) {
            title.append(mainActivity.getSelectedPaymentGroup()?.name)
        }
        else {
            title.append(getString(R.string.title_no_group_created))
        }

        mainActivity.supportActionBar?.title = title.toString()

        getPaymentPeriods()
        setPaymentPeriodDetailFragment()

        paymentPeriodAdapter = PaymentPeriodAdapter(mainActivity, paymentPeriods)
        val layoutManager = LinearLayoutManager(activity)
        binding.rvPaymentPeriods.layoutManager = layoutManager
        binding.rvPaymentPeriods.itemAnimator = DefaultItemAnimator()
        binding.rvPaymentPeriods.adapter = paymentPeriodAdapter
        binding.srlPaymentPeriods.setOnRefreshListener(OnRefreshListener {
            binding.srlPaymentPeriods.isRefreshing = false
            getPaymentPeriods()
            paymentPeriodAdapter.notifyDataSetChanged()
        })
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.payments_header_menu, menu)

        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        if (item.itemId == R.id.select_group) {
            val bundle = Bundle()
            bundle.putInt(AppConsts.BUNDLE_ORIGINAL_PAGE, R.id.navigation_payment_periods)
            mainActivity.navigate(R.id.navigation_payment_periods, R.id.navigation_payment_group_selection, bundle, false)

            return true
        }

        if (item.itemId == R.id.show_detail) {
            if (paymentsDetailsShown) {
                binding.fmPaymentPeriodDetail.visibility = View.GONE
                binding.srlPaymentPeriods.visibility = View.VISIBLE
                item.icon = AppCompatResources.getDrawable(mainActivity,
                    R.drawable.outline_receipt_white_24)
            }
            else {
                binding.fmPaymentPeriodDetail.visibility = View.VISIBLE
                binding.srlPaymentPeriods.visibility = View.GONE
                item.icon = AppCompatResources.getDrawable(mainActivity,
                    R.drawable.outline_payments_white_24)
            }
            paymentsDetailsShown = !paymentsDetailsShown
        }

        return false
    }

    private fun showEmptyListMessage(paymentList: ArrayList<PaymentPeriod>) {
        if (paymentList.isEmpty()) {
            binding.tvPaymentPeriodListEmpty.visibility = View.VISIBLE
            binding.srlPaymentPeriods.visibility = View.GONE
        }
        else {
            binding.tvPaymentPeriodListEmpty.visibility = View.GONE
            binding.srlPaymentPeriods.visibility = View.VISIBLE
        }
    }

    private fun getPaymentPeriods() {
        mainActivity.blockScreen(binding.clPaymentPeriodsProgress)
        if (mainActivity.getSelectedPaymentGroup() != null) {
            paymentPeriods = paymentPeriodService
                .getPaymentPeriodsByGroup(mainActivity, mainActivity.getSelectedPaymentGroup()!!)
        }
        else {
            paymentPeriods = ArrayList()
        }
        mainActivity.unblockScreen(binding.clPaymentPeriodsProgress)
        showEmptyListMessage(paymentPeriods)

    }

    private fun setPaymentPeriodDetailFragment() {
        val paymentList = ArrayList<Payment>()
        for (period in paymentPeriods) {
            paymentList.addAll(period.payments)
        }
        val childFragment: Fragment = PaymentPeriodDetailFragment(paymentList, false)
        val transaction: FragmentTransaction = childFragmentManager.beginTransaction()
        transaction.replace(R.id.fm_payment_period_detail, childFragment).commit()
    }
}