package com.psm.lifebillorganizer.android.ui.options

import android.app.AlertDialog
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.res.Configuration
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatDelegate
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.psm.lifebillorganizer.MyApplication
import com.psm.lifebillorganizer.R
import com.psm.lifebillorganizer.android.adapters.PayersAdapter
import com.psm.lifebillorganizer.android.adapters.PaymentGroupAdapter
import com.psm.lifebillorganizer.android.adapters.SwipeGesture
import com.psm.lifebillorganizer.android.receivers.PaymentRecurrentAlarm
import com.psm.lifebillorganizer.consts.AppConsts
import com.psm.lifebillorganizer.databinding.FragmentOptionsBinding
import com.psm.lifebillorganizer.models.Payer
import com.psm.lifebillorganizer.models.PaymentGroup
import com.psm.lifebillorganizer.models.Period
import com.psm.lifebillorganizer.services.PayerService
import com.psm.lifebillorganizer.services.PaymentGroupService
import com.psm.lifebillorganizer.android.ui.MainActivity
import com.psm.lifebillorganizer.services.PaymentGroupOnlineService
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

class OptionsFragment : Fragment() {

    private lateinit var mainActivity: MainActivity
    private lateinit var payers: ArrayList<Payer>
    private lateinit var paymentGroups: ArrayList<PaymentGroup>
    private lateinit var periods: ArrayList<Period>

    private var payersHidden: Boolean = false
    private var paymentGroupsHidden: Boolean = false
    private lateinit var payersAdapter: PayersAdapter
    private lateinit var paymentGroupsAdapter: PaymentGroupAdapter

    @Inject
    lateinit var payerService: PayerService
    @Inject
    lateinit var paymentGroupService: PaymentGroupService
    @Inject
    lateinit var paymentGroupOnlineService: PaymentGroupOnlineService
    @Inject
    lateinit var optionsViewModel: OptionsViewModel

    private var _binding: FragmentOptionsBinding? = null
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        this.mainActivity = activity as MainActivity
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentOptionsBinding.inflate(inflater, container, false)

        //Inject
        (mainActivity.applicationContext as MyApplication).appComponent.inject(this)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //TODO: BUG: Al crear un grupo de 3 payers, subirlo a Mongo. Cuando el segundo lo baja No se ve el tercer integrante en Opciones

        setHasOptionsMenu(true)
        mainActivity.supportActionBar?.title = getString(R.string.title_options)

        payersHidden = true
        paymentGroupsHidden = true

        //Payers
        loadPayers()
        binding.btPayers.setOnClickListener(View.OnClickListener {
            payersHidden = !payersHidden
            var img: Drawable? = null
            img = if (payersHidden) {
                binding.rvOptionsPayers.visibility = View.GONE
                binding.root.resources.getDrawable(R.drawable.outline_expand_more_white_24, mainActivity.theme)
            } else {
                binding.rvOptionsPayers.visibility = View.VISIBLE
                binding.root.resources.getDrawable(R.drawable.outline_expand_less_white_24, mainActivity.theme)
            }
            binding.btPayers.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null)
        })

        binding.ibOptionsAddPayer.setOnClickListener(View.OnClickListener {

            if (payers.size == AppConsts.MAX_PAYERS) {
                Snackbar.make(
                    binding.rvOptionsPayers, getString(R.string.message_error_max_payers_exceeded),
                    Snackbar.LENGTH_LONG
                ).show()
            }
            else {
                val bundle = Bundle()
                bundle.putBoolean(AppConsts.BUNDLE_FROM_OPTIONS, true)
                mainActivity.navigate(R.id.navigation_options, R.id.navigation_payer, bundle, false)
            }

        })

        //Payment Groups
        loadPaymentGroups()
        binding.btPaymentGroups.setOnClickListener(View.OnClickListener {
            paymentGroupsHidden = !paymentGroupsHidden
            var img: Drawable? = null
            img = if (paymentGroupsHidden) {
                binding.rvOptionsPaymentGroups.setVisibility(View.GONE)
                binding.root.resources.getDrawable(R.drawable.outline_expand_more_white_24, mainActivity.theme)
            } else {
                binding.rvOptionsPaymentGroups.setVisibility(View.VISIBLE)
                binding.root.resources.getDrawable(R.drawable.outline_expand_less_white_24, mainActivity.theme)
            }
            binding.btPaymentGroups.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null)

        })

        binding.ibOptionsAddPaymentGroup.setOnClickListener(View.OnClickListener {

            if (paymentGroups.size == AppConsts.MAX_GROUPS) {
                Snackbar.make(
                    binding.rvOptionsPaymentGroups, getString(R.string.message_error_max_payment_groups_exceeded),
                    Snackbar.LENGTH_LONG
                ).show()
            }
            else {
                val bundle = Bundle()
                bundle.putBoolean(AppConsts.BUNDLE_FROM_OPTIONS, true)
                mainActivity.navigate(R.id.navigation_options, R.id.navigation_payment_group, bundle, false)
            }
        })

        binding.ibOptionsJoinOnlinePaymentGroup.setOnClickListener {
            if (paymentGroups.size == AppConsts.MAX_GROUPS) {
                Snackbar.make(
                    binding.rvOptionsPaymentGroups,
                    getString(R.string.message_error_max_payment_groups_exceeded),
                    Snackbar.LENGTH_LONG
                ).show()
            } else {
                val bundle = Bundle()
                mainActivity.navigate(
                    R.id.navigation_options, R.id.navigation_join_payment_group,
                    bundle, false
                )
            }
        }



        // Change Theme
        val sharedPref = activity?.getPreferences(Context.MODE_PRIVATE) ?: return
        val themeSelected = sharedPref.getString(AppConsts.PREFERENCE_THEME,
            AppConsts.PREFERENCE_THEME_SYSTEM)
        when (themeSelected) {
            AppConsts.PREFERENCE_THEME_DARK -> {
                binding.rbDarkTheme.isChecked = true
                binding.rbLightTheme.isChecked = false
                binding.rbSystemTheme.isChecked = false
            }
            AppConsts.PREFERENCE_THEME_LIGHT -> {
                binding.rbDarkTheme.isChecked = false
                binding.rbLightTheme.isChecked = true
                binding.rbSystemTheme.isChecked = false
            }
            AppConsts.PREFERENCE_THEME_SYSTEM -> {
                binding.rbDarkTheme.isChecked = false
                binding.rbLightTheme.isChecked = false
                binding.rbSystemTheme.isChecked = true
            }
        }
        binding.rgChangeTheme.setOnCheckedChangeListener { radioGroup, checkedId ->
            when (checkedId) {
                binding.rbDarkTheme.id ->
                    with (sharedPref.edit()) {
                        putString(AppConsts.PREFERENCE_THEME, AppConsts.PREFERENCE_THEME_DARK)
                        apply()
                        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
                    }
                binding.rbLightTheme.id ->
                    with (sharedPref.edit()) {
                        putString(AppConsts.PREFERENCE_THEME, AppConsts.PREFERENCE_THEME_LIGHT)
                        apply()
                        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
                    }
                binding.rbSystemTheme.id ->
                    with (sharedPref.edit()) {
                        putString(AppConsts.PREFERENCE_THEME, AppConsts.PREFERENCE_THEME_SYSTEM)
                        apply()
                        //TODO: Ver si conviene moverlo a un lugar generico para no repetirlo
                        val nightModeFlags: Int = mainActivity.applicationContext.resources
                            .configuration.uiMode and Configuration.UI_MODE_NIGHT_MASK
                        when (nightModeFlags) {
                            Configuration.UI_MODE_NIGHT_YES -> AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
                            Configuration.UI_MODE_NIGHT_NO -> AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
                            Configuration.UI_MODE_NIGHT_UNDEFINED -> AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
                        }
                    }
            }
        }

    }

    private fun loadPayers() {
        payers = payerService.getPayers(null)
        payersAdapter = PayersAdapter(
            mainActivity, payers,
            itemSelectable = false,
            oneSelection = false
        )

        val swipeGesture = object : SwipeGesture(mainActivity) {
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                when (direction) {
                    ItemTouchHelper.LEFT -> {
                        deletePayer(viewHolder, direction)
                    }
                    ItemTouchHelper.RIGHT -> {
                        deletePayer(viewHolder, direction)
                    }
                }
            }
        }

        val touchHelper = ItemTouchHelper(swipeGesture)
        touchHelper.attachToRecyclerView(binding.rvOptionsPayers)

        val layoutManager = LinearLayoutManager(activity)
        binding.rvOptionsPayers.layoutManager = layoutManager
        binding.rvOptionsPayers.itemAnimator = DefaultItemAnimator()
        binding.rvOptionsPayers.adapter = payersAdapter
    }

    private fun loadPaymentGroups() {
        paymentGroups = paymentGroupService.getGroups()
        paymentGroupsAdapter = PaymentGroupAdapter(mainActivity, paymentGroups, 0)

        val swipeGesturePG = object : SwipeGesture(mainActivity) {
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                when (direction) {
                    ItemTouchHelper.LEFT -> {
                        deleteGroup(viewHolder, direction)
                    }
                    ItemTouchHelper.RIGHT -> {
                        deleteGroup(viewHolder, direction)
                    }
                }
            }
        }

        val touchHelperPG = ItemTouchHelper(swipeGesturePG)
        touchHelperPG.attachToRecyclerView(binding.rvOptionsPaymentGroups)

        val layoutManagerPG = LinearLayoutManager(activity)
        binding.rvOptionsPaymentGroups.layoutManager = layoutManagerPG
        binding.rvOptionsPaymentGroups.itemAnimator = DefaultItemAnimator()
        binding.rvOptionsPaymentGroups.adapter = paymentGroupsAdapter
        this.paymentGroups = paymentGroups

    }

    private fun deletePayer(viewHolder: RecyclerView.ViewHolder, direction: Int) {

        val itemToDelete = payers[viewHolder.adapterPosition]
        if (itemToDelete.isMainPayer) {
            Snackbar.make(
                binding.btPayers, getString(R.string.message_error_main_payer_cant_be_deleted),
                Snackbar.LENGTH_LONG
            ).show()
            payersAdapter.refreshAdapter()
        }
        else {
            val builder = AlertDialog.Builder(mainActivity)
            builder.setMessage(getString(R.string.message_sure_delete_payer))
                .setCancelable(false)
                .setPositiveButton(getString(R.string.label_yes)) { dialog, id ->


                    val groups = paymentGroupService.getGroupsByPayer(itemToDelete.id!!)
                    for (group in groups) {
                        var pos: Int? = null
                        for (groupShowed in paymentGroups) {
                            if (group.id == groupShowed.id) {
                                pos = paymentGroups.indexOf(group)
                                break
                            }
                        }
                        if (pos != null) {
                            paymentGroupsAdapter.deleteItem(pos)
                        }

                        //Cancel alarm for recurrent payments
                        val alarm = PaymentRecurrentAlarm()
                        alarm.cancelAlarm(mainActivity, group.id!!)
                    }

                    val newGroupSelected = payerService.deletePayer(itemToDelete.id!!)
                    payersAdapter.deleteItem(viewHolder.adapterPosition)
                    if (newGroupSelected!=null) {
                        mainActivity.setSelectedPaymentGroup(newGroupSelected)
                    }
                }
                .setNegativeButton(getString(R.string.label_no)) { dialog, id ->
                    payersAdapter.refreshAdapter()
                    // Dismiss the dialog
                    dialog.dismiss()
                }
            val alert = builder.create()
            alert.show()
        }


    }

    private fun deleteGroup(viewHolder: RecyclerView.ViewHolder, direction: Int) {
        val builder = AlertDialog.Builder(mainActivity)
        builder.setMessage(getString(R.string.message_sure_delete_group))
            .setCancelable(false)
            .setPositiveButton(getString(R.string.label_yes)) { dialog, id ->
                val itemToDelete = paymentGroups[viewHolder.adapterPosition]
                val groupToDelete = paymentGroupService.getGroup(itemToDelete.id!!)
                val newGroupSelected = paymentGroupService.deleteGroup(itemToDelete.id!!)
                //Delete group remotely
                if (groupToDelete?.externalId != null) {
                    CoroutineScope(Dispatchers.IO).launch {
                        paymentGroupOnlineService.deleteOnlinePaymentGroup(groupToDelete!!)
                    }
                }

                paymentGroupsAdapter.deleteItem(viewHolder.adapterPosition)
                if (newGroupSelected!=null) {
                    mainActivity.setSelectedPaymentGroup(newGroupSelected)
                }
                else {
                    mainActivity.setSelectedPaymentGroup(null)
                }

                //Cancel alarm for recurrent payments
                val alarm = PaymentRecurrentAlarm()
                alarm.cancelAlarm(mainActivity, itemToDelete.id!!)
            }
            .setNegativeButton(getString(R.string.label_no)) { dialog, id ->
                paymentGroupsAdapter.refreshAdapter()
                // Dismiss the dialog
                dialog.dismiss()
            }
        val alert = builder.create()
        alert.show()
    }

}