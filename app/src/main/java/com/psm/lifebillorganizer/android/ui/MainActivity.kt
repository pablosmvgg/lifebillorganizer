package com.psm.lifebillorganizer.android.ui

import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavOptions
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.psm.lifebillorganizer.MyApplication
import com.psm.lifebillorganizer.R
import com.psm.lifebillorganizer.dao.DatabaseDao
import com.psm.lifebillorganizer.databinding.ActivityMainBinding
import com.psm.lifebillorganizer.models.PaymentGroup
import com.psm.lifebillorganizer.services.FirstTimeExecService
import com.psm.lifebillorganizer.services.PaymentGroupService
import com.psm.lifebillorganizer.services.PaymentService
import javax.inject.Inject


class MainActivity : ParentActivity() {

    private var originIds: ArrayList<Int>? = ArrayList<Int>()
    private var destinationIds: ArrayList<Int>? = ArrayList<Int>()
    private var bundles: ArrayList<Bundle>? = ArrayList<Bundle>()

    @Inject
    lateinit var databaseDao: DatabaseDao
    @Inject
    lateinit var paymentGroupService: PaymentGroupService
    @Inject
    lateinit var firstTimeExecService: FirstTimeExecService
    @Inject
    lateinit var paymentService: PaymentService

    private var selectedPaymentGroup: PaymentGroup? = null

    private lateinit var binding: ActivityMainBinding

    private var lastOriginNavigationId: Int? = null
    private var lastDestNavigationId: Int? = null

    private var pressedTime: Long = 0

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        when (newConfig.uiMode and Configuration.UI_MODE_NIGHT_MASK) {
            Configuration.UI_MODE_NIGHT_YES -> setTheme(R.style.darkTheme)
            Configuration.UI_MODE_NIGHT_NO -> setTheme(R.style.AppTheme)
            Configuration.UI_MODE_NIGHT_UNDEFINED -> setTheme(R.style.AppTheme)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        setCurrentTheme(this)

        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        //Inject
        (applicationContext as MyApplication).appComponent.inject(this)

        databaseDao.initDatabase(applicationContext)

        //Redirect if it's the first time
        val firstTimeExec = firstTimeExecService.getFirstTimeExec()
        if (!firstTimeExec.executed) {
            val intent = Intent(this, FirstTimeActivity::class.java).apply {
                //putExtra(EXTRA_MESSAGE, message)
            }
            startActivity(intent)
        }

        //Toolbar
        binding.toolbar.title = ""
        binding.toolbar.setTitleTextColor(getColor(R.color.lt_toolbar_text_default))
        setSupportActionBar(binding.toolbar)

        val navController = findNavController(R.id.nav_host_fragment)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        val appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.navigation_payments, R.id.navigation_payment_periods, R.id.navigation_balance_payments,
                R.id.navigation_options
            )
        )

        setupActionBarWithNavController(navController, appBarConfiguration)
        binding.navView.setupWithNavController(navController)

        //Get the selected group
        selectedPaymentGroup = paymentGroupService.getSelectedGroup()

        if (savedInstanceState == null) {
            navController.navigate(R.id.navigation_payments)
        }

    }

    override fun onSupportNavigateUp(): Boolean {
        navigate(
            destinationIds!![destinationIds!!.size - 1], originIds!![originIds!!.size - 1],
            bundles!![bundles!!.size - 1], true
        )

        return true
    }

    fun navigate(originId: Int, destinationId: Int, bundle: Bundle, isBack: Boolean) {
        lastOriginNavigationId = originId
        lastDestNavigationId = originId
        if (originIds == null && destinationIds == null && bundles == null) {
            originIds = ArrayList()
            destinationIds = ArrayList()
            bundles = ArrayList()
        }

        if (!isBack) {
            originIds?.add(originId)
            destinationIds?.add(destinationId)
            bundles?.add(bundle)
        } else {
            originIds?.removeAt(originIds!!.size - 1)
            destinationIds?.removeAt(destinationIds!!.size - 1)
            bundles?.removeAt(bundles!!.size - 1)
        }

        val navController = Navigation.findNavController(this, R.id.nav_host_fragment)
        val navOptions = NavOptions.Builder()
            .setPopUpTo(originId, true)
            .build()
        navController.navigate(destinationId, bundle, navOptions)
    }

    fun getSelectedPaymentGroup(): PaymentGroup? {
        return selectedPaymentGroup
    }

    fun setSelectedPaymentGroup(paymentGroup: PaymentGroup?) {
        this.selectedPaymentGroup = paymentGroup
    }

    override fun onBackPressed() {
        if (pressedTime + 2000 > System.currentTimeMillis()) {
            super.onBackPressed()
            finish()
        } else {
            Toast.makeText(baseContext, getString(R.string.message_exit), Toast.LENGTH_SHORT).show()
        }
        pressedTime = System.currentTimeMillis()
    }

/*
SQlite:
https://cashapp.github.io/sqldelight/android_sqlite/

Icons:
    https://fonts.google.com/icons

View Binding example:
https://cursokotlin.com/capitulo-29-view-binding-en-kotlin/

RetroFit 2 example:
https://cursokotlin.com/tutorial-retrofit-2-en-kotlin-con-corrutinas-consumiendo-api-capitulo-20-v2/

Alarms:
https://techvidvan.com/tutorials/android-alarm-manager/

Dark Mode:
https://www.section.io/engineering-education/how-to-implement-dark-mode-in-android-studio/

Drive:
https://developers.google.com/drive/api/v3/about-sdk?hl=es_419

Mongo library:
mongodb+srv://lboclusteruser:fG.6527TK6.ty@lifebillorganizercluste.tnkyd.mongodb.net/LifeBillOrganizer?retryWrites=true&w=majority
val uri = MongoClientURI("mongodb://lboclusteruser:fG.6527TK6.ty@lifebillorganizercluste.tnkyd.mongodb.net/LifeBillOrganizer?retryWrites=true&w=majority")
val mongoClient = MongoClient(uri)
val db = mongoClient.getDatabase(uri.database)
val response = db.getCollection("GroupPayments").find().first()
val algo = 1;

Mongo API:
OkHttpClient client = new OkHttpClient().newBuilder().build();
MediaType mediaType = MediaType.parse("application/json");
RequestBody body = RequestBody.create(mediaType, "{\n    \"collection\":\"GroupPayments\",\n    \"database\":\"LifeBillOrganizer\",\n    \"dataSource\":\"lifebillorganizercluste\",\n    \"projection\": {\"_id\": 1}\n\n}");
Request request = new Request.Builder()
    .url("https://data.mongodb-api.com/app/data-rkubr/endpoint/data/beta/action/findOne")
    .method("POST", body)
    .addHeader("Content-Type", "application/json")
    .addHeader("Access-Control-Request-Headers", "*")
    .addHeader("api-key", "UwypHYi5IqdP1rSE5IJyw9W9amJvBJIghNTzysVYtN7jbs2FNC7Oy4XXHrxqcxLv")
    .build();
Response response = client.newCall(request).execute();


Ejemplo de coroutine:
CoroutineScope(Dispatchers.IO).launch {
    val response = paymentGroupService.getOnlineGroup("algo")
}

Para ver como mantener actualizados los online groups:
https://perihanmirkelam.medium.com/socket-programming-on-android-tcp-server-example-e4552a957c08
https://docs.mongodb.com/manual/changeStreams/

 */
}