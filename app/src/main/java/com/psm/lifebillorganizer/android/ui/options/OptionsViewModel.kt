package com.psm.lifebillorganizer.android.ui.options

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.psm.lifebillorganizer.models.Payer
import com.psm.lifebillorganizer.models.PaymentGroup
import com.psm.lifebillorganizer.services.PayerService
import com.psm.lifebillorganizer.services.PaymentGroupService
import javax.inject.Inject

class OptionsViewModel @Inject constructor(val paymentGroupService: PaymentGroupService,
                                           val payerService: PayerService) : ViewModel() {

    //TODO: ver si vale la pena implementarlo
    //Example
    private val _text = MutableLiveData<String>().apply {
        value = "This is notifications Fragment"
    }
    val text: LiveData<String> = _text

    private val payers: MutableLiveData<ArrayList<Payer>> by lazy {
        MutableLiveData<ArrayList<Payer>>().also {
            it.value = loadPayers()
        }
    }
    fun getPayers(): LiveData<ArrayList<Payer>> {
        return payers
    }
    private fun loadPayers(): ArrayList<Payer> {
        return payerService.getPayers(null)
    }


    private val paymentGroups: MutableLiveData<ArrayList<PaymentGroup>> by lazy {
        MutableLiveData<ArrayList<PaymentGroup>>().also {
            it.value = loadPaymentGroups()
        }
    }
    fun getPaymentGroups(): LiveData<ArrayList<PaymentGroup>> {
        return paymentGroups
    }
    private fun loadPaymentGroups(): ArrayList<PaymentGroup> {
        return paymentGroupService.getGroups()
    }

}